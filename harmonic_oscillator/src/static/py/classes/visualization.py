from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

class Visualization:
    
    @classmethod
    def plot_result(cls, i, loss_value, x, y, x_data, y_data, yh, xp=None):
        "Pretty plot training results"
        plt.figure(figsize=(8, 4))
        plt.plot(x, y, color="grey", linewidth=2, linestyle = '--',
                alpha=0.8, label="Exact solution")
        plt.plot(x, yh, color="tab:blue", linewidth=4,
                alpha=0.8, label="Neural network prediction")
        plt.scatter(x_data, y_data, s=50, color="tab:orange",
                    alpha=0.8, label='Training data')
        if xp is not None:
            plt.scatter(xp, -0*np.ones_like(xp), s=60, color="tab:green", alpha=0.4,
                        label='Physics loss training locations')
        l = plt.legend(loc=(1.01, 0.34), frameon=False, fontsize="large")
        plt.setp(l.get_texts(), color="k")
        plt.xlim(-0.05, 1.05)
        plt.ylim(-1.1, 1.1)
        plt.text(1.065, 0.7, "Training step: %i" %
                (i), fontsize="xx-large", color="k")
        plt.text(1.065, 0.4, "Value Loss Function: %.8f" %
                (loss_value), fontsize="xx-large", color="k")
        plt.axis("off")

    @classmethod
    def save_gif(cls,outfile, files, fps=5, loop=0):
        "Helper function for saving GIFs"
        imgs = [Image.open(file) for file in files]
        imgs[0].save(fp=outfile, format='GIF', append_images=imgs[1:], save_all=True, duration=int(1000/fps), loop=loop)