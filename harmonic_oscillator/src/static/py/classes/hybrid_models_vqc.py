import pennylane as qml
import torch
import torch.nn as nn
import torch.optim as optim
from quantum_models import QuantumModelsTwoQubits

n_qubits = 2  # Número de qubits
#dev = qml.device("default.qubit", wires=n_qubits)  # Simulador cuántico
# Quantum layer
#quantum_model = QuantumModelsTwoQubits().vqc_three

class QuantumLayer(nn.Module):
    def __init__(self):
        super().__init__()
        weight_shapes = {"weights": 10}
        self.q_weights = nn.Parameter(torch.randn(weight_shapes["weights"]))  # Pesos cuánticos

    def forward(self, x):
        quantum_model = QuantumModelsTwoQubits().vqc_three_tensor
        print('*****************')
        print(self.q_weights)
        print(self.q_weights[0].item())
        print(self.q_weights[1].item())

        return torch.tensor(quantum_model(x, self.q_weights), dtype=torch.float32)

class HybridQNN(nn.Module):
    def __init__(self,N_INPUT, N_OUTPUT, N_HIDDEN, N_LAYERS):
        super().__init__()
        activation = nn.Tanh
        self.fcs = nn.Sequential(*[
                        nn.Linear(N_INPUT, N_HIDDEN),
                        activation()])
        self.fch = nn.Sequential(*[
                        nn.Sequential(*[
                            nn.Linear(N_HIDDEN, N_HIDDEN),
                            activation()]) for _ in range(N_LAYERS-1)])
        self.fce = nn.Linear(N_HIDDEN, N_OUTPUT)

        #self.fce = nn.Linear(N_HIDDEN, N_OUTPUT)
        #self.fc1 = nn.Linear(N_INPUT, N_HIDDEN)  # Capa clásica
        self.q_layer = QuantumLayer()  # Capa cuántica
        #self.fc2 = nn.Linear(N_HIDDEN, N_LAYERS)  # Capa de salida
    
    def forward(self, x):
     #   x = torch.tanh(self.fc1(x))  # Activación clásica
        x = self.fcs(x)
        x = self.fch(x)
        x = self.fce(x)
        print('entro al forward')
        print(x)
        x = x.detach().numpy()[0]
        print(x)
        x = self.q_layer(x)  # Pasar por la capa cuántica
       # x = torch.sigmoid(self.fc2(x))  # Salida
        return x
    