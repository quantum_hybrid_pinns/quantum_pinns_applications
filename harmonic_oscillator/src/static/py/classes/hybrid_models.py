import pennylane as qml
import torch
import torch.nn as nn
import torch.optim as optim

n_qubits = 2  # Número de qubits
dev = qml.device("default.qubit", wires=n_qubits)  # Simulador cuántico

@qml.qnode(dev, interface="torch")
def quantum_circuit(inputs, weights):
    qml.AngleEmbedding(inputs, wires=range(n_qubits))
    qml.StronglyEntanglingLayers(weights, wires=range(n_qubits))
    return [qml.expval(qml.PauliZ(i)) for i in range(n_qubits)]

class QuantumLayer(nn.Module):
    def __init__(self):
        super().__init__()
        weight_shapes = {"weights": (3, n_qubits, 3)}
        self.q_weights = nn.Parameter(torch.randn(weight_shapes["weights"]))  # Pesos cuánticos

    def forward(self, x):
        batch_outputs = [quantum_circuit(sample, self.q_weights) for sample in x]  # Ejecutar por cada muestra
        batch_outputs = torch.tensor(batch_outputs, dtype=torch.float32)  # Convertir correctamente
        return batch_outputs
    
class HybridQNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(2, 2)  # Capa clásica
        self.q_layer = QuantumLayer()  # Capa cuántica
        self.fc2 = nn.Linear(2, 1)  # Capa de salida
    
    def forward(self, x):
        x = torch.tanh(self.fc1(x))  # Activación clásica
        x = self.q_layer(x)  # Pasar por la capa cuántica
        x = torch.sigmoid(self.fc2(x))  # Salida
        return x
# Crear datos de ejemplo
X = torch.rand(100, 2)  # Datos de entrada (100 muestras, 2 características)
y = (X[:, 0] + X[:, 1] > 1).float().unsqueeze(1)  # Etiquetas binarias

# Crear el modelo
model = HybridQNN()
optimizer = optim.Adam(model.parameters(), lr=0.1)
loss_fn = nn.BCELoss()

# Entrenamiento
for epoch in range(100):
    optimizer.zero_grad()
    y_pred = model(X)
    loss = loss_fn(y_pred, y)
    loss.backward()
    optimizer.step()
    if epoch % 10 == 0:
        print(f"Epoch {epoch}: Loss = {loss.item():.4f}")

with torch.no_grad():
    test_sample = torch.tensor([[0.8, 0.9]])
    prediction = model(test_sample)
    print(f"Predicción para {test_sample.numpy()}: {prediction.item():.4f}")