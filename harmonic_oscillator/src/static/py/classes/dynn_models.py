# All imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors

# Set a random seed
np.random.seed(seed=1)

class DumpedHarmonicOscillator():
    "Defines a connected network"
    
    def __init__(self, natural_frequency, dumped_frequency):
        super().__init__()
        self.order_one_coeff = 2 * dumped_frequency
        self.order_zero_coeff = natural_frequency ** 2

    def get_ssm(self):
        """
        Returns an LTI system matrices (A, B, C, D) corresponding to 2-D heat equation
        
        Args:
            ssm_dim (List of matrices): Input, state and output dimensions of the LTI system
            natural_freq: Natural frequency of system for squared w0**2
            dumped_frew: Dumped frequency for 2, 2*d

        Returns:
            List of matrices: A, B, C, D
        """

        # Fetch dimensions
        
        # State Matrix
        A = np.array([[0, 1],[-self.order_zero_coeff, -self.order_one_coeff] ])
        
        # Other matrices of the state-space model
        B = np.array([[0], [1]])        # Corregir cuando sea movimiento forzado
        C = np.array([[1, 0]])       # Output matrix
        D = np.zeros((1, 1))
        #D = np.zeros((dimension, dimension))     # Feedthrough (or feedforward) matrix
        return A, B, C, D


    def get_input(self,timesteps, d_in, show_input = True):
        """
        Returns the input which is the heat injected into the system via the 
        source term.

        Args:
            timesteps (array): Time points
            d_in (int): input dimension
            show_input (bool, optional): Plots the input at time t = 0.02. Defaults to True.

        Returns:
            list: Returns [u, du_dt] (Ignore: dt_dt for this example as it is never used)
        """
        # Input to a dynamic neural network and the numerical solvers

        u = - np.ones((len(timesteps), d_in))*400
        du_dt = np.zeros((len(timesteps), d_in))
        #gaussian_input_pulse = 100. * np.exp(-0.8 * ((x_grid-l/2)**2 + (y_grid-l/2)**2)) #sqrt(x**2 + y**2)
        gaussian_input_pulse = 0
        #u[2, :] = gaussian_input_pulse.reshape(-1)
        # Plot the input signal at time t = 0.02
        #if show_input:
         #   plt.figure(figsize=(2,2))
          #  plt.plot()
           # fontsize = 10
            #plt.title('$u(t=0.02)$',fontsize=fontsize)
            #sm.set_array([])
            #plt.colorbar(sm, location='bottom',aspect=20)


        # du_dt: Not required for real eigenvalues 
        # (It's only used when A has complex evals)
        for t in range(0, len(timesteps) - 1):   
            du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])

        return [u, du_dt]

