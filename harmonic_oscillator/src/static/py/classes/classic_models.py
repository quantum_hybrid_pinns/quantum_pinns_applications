
import numpy as np
import torch
import torch.nn as nn
import torch.autograd as Variable

class FCN(nn.Module):
    "Defines a connected network"
    
    def __init__(self, N_INPUT, N_OUTPUT, N_HIDDEN, N_LAYERS):
        super().__init__()
        activation = nn.Tanh
        self.fcs = nn.Sequential(*[
                        nn.Linear(N_INPUT, N_HIDDEN),
                        activation()])
        self.fch = nn.Sequential(*[
                        nn.Sequential(*[
                            nn.Linear(N_HIDDEN, N_HIDDEN),
                            activation()]) for _ in range(N_LAYERS-1)])
        self.fce = nn.Linear(N_HIDDEN, N_OUTPUT)
        
    def forward(self, x):
        x = self.fcs(x)
        x = self.fch(x)
        x = self.fce(x)
        return x

class SimpleRNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(SimpleRNN, self).__init__()
        self.rnn = nn.RNN(input_size, hidden_size, batch_first=True)
        self.fc = nn.Linear(hidden_size, output_size)
        # self.hidden_size = hidden_size
    
    def forward(self, x):
        
        h0 = torch.zeros(1, x.size(0), self.hidden_size).to(x.device)
        out, _ = self.rnn(x, h0)
        out = self.fc(out)
        return out
    
# class LevelRNN(nn.Module):
#     def __init__(self,input_size, hidden_size, output_size):
#         super(LevelRNN, self).__init__()
    #     self.rnn = nn.RNN(input_size, hidden_size, num_layers=1, nonlinearity='tanh', bias=True, batch_first=False, dropout=0.0, bidirectional=False, device=None, dtype=None)

    #     self.num_layers = 1
    #     self.batch_first = False
    #     self.hidden_size = hidden_size

    # def forward(self, x, h_0=None):
    #     if self.batch_first:
    #         x = x.transpose(0, 1)
    #     seq_len, batch_size, _ = x.size()
    #     if h_0 is None:
    #         h_0 = torch.zeros(self.num_layers, batch_size, self.hidden_size)
    #     h_t_minus_1 = h_0
    #     h_t = h_0
    #     output = []
    #     for t in range(seq_len):
    #         for layer in range(self.num_layers):
    #             h_t[layer] = torch.tanh(
    #                 x[t] @ weight_ih[layer].T
    #                 + bias_ih[layer]
    #                 + h_t_minus_1[layer] @ weight_hh[layer].T
    #                 + bias_hh[layer]
    #             )
    #         output.append(h_t[-1])
    #         h_t_minus_1 = h_t
    #     output = torch.stack(output)
    #     if self.batch_first:
    #         output = output.transpose(0, 1)
    #     return output, h_t

class RNN_LSTM_Base(nn.Module):
    def training_step(self, batch):
        samples, targets = batch
        outputs = self(samples.double())
        loss = nn.functional.mse_loss(outputs, targets)
        return loss

class VanillaRNN(RNN_LSTM_Base):
    def __init__(self, in_size, hid_size, out_size, n_layers=1):
        super(VanillaRNN, self).__init__()        
        # Define dimensions for the layers
        self.input_size = in_size
        self.hidden_size = hid_size
        self.output_size = out_size
        self.n_layers = n_layers        
        # Defining the RNN layer
        self.rnn = nn.RNN(in_size, hid_size, n_layers, batch_first=True)        
        # Defining the linear layer
        self.linear = nn.Linear(hid_size, out_size)
        
    def forward(self, x):
        # x must be of shape (batch_size, seq_len, input_size)
        # xb = x.view(x.size(0), x.size(1), self.input_size).double()        
        # Initialize the hidden layer's array of shape (n_layers*n_dirs, batch_size, hidden_size_rnn)
        h0 = torch.zeros(self.n_layers, x.size(0), self.hidden_size, requires_grad=True)
        # out is of shape (batch_size, seq_len, num_dirs*hidden_size_rnn)
        out, hn = self.rnn(x, h0)        
        # out needs to be reshaped into dimensions (batch_size, hidden_size_lin)
        out = nn.functional.tanh(hn)        
        # Finally we get out in the shape (batch_size, output_size)
        out = self.linear(out)
        return out