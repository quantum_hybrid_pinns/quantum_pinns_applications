import pennylane as qml
from pennylane import numpy as np


class QuantumModelsTwoQubits:
    # Quantum model based in VQC(Variational Quantum Circuit)
    def __init__(self):
        self.dev_qubit = qml.device('default.qubit', wires=2)

    def vqc_one(self,x,w):
        @qml.qnode(self.dev_qubit)
        def _vqc_one(x, w):
            # circuit  1
            qml.RY(x*w[2], wires=0)
            qml.RX(x*w[3], wires=1)
            qml.RY(w[4], wires = 0)
            qml.RY(w[5], wires = 1)
            qml.CNOT(wires = [1,0])
            return qml.expval(qml.PauliZ(wires=0))
        return _vqc_one(x,w)
    
    def vqc_two(self,x,w):
        @qml.qnode(self.dev_qubit)
        def _vqc_two(x, w):
        # circuit 2
            qml.RY(x*w[2], wires=0)
            qml.RX(w[0], wires=0)
            qml.RY(w[1], wires=0)
            return qml.expval(qml.PauliZ(wires=0))
        return _vqc_two(x,w)
    
    def vqc_three(self,x,w):
        @qml.qnode(self.dev_qubit)
        def _vqc_three(x, w):
            # qml.Hadamard(wires=0)
            # qml.Hadamard(wires = 1)
            qml.RY(x*w[0], wires=0)
            qml.RY(x*w[1], wires=1)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[2], wires=0)
            qml.RY(w[3], wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[4], wires=0)
            qml.RY(w[5], wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[6], wires=0)
            qml.RY(w[7], wires=0)
            qml.CNOT(wires=[1, 0])


            qml.RX(w[8], wires=0)
            qml.RY(w[9], wires=1)
            qml.CNOT(wires=[1, 0])
            return qml.expval(qml.PauliZ(wires=0))
        return _vqc_three(x,w)
    
    def vqc_four(self,x,w):
        @qml.qnode(self.dev_qubit)
        def _vqc_four(x, w):
            # qml.Hadamard(wires=0)
            # qml.Hadamard(wires = 1)
            qml.RY(x*w[0], wires=0)
            qml.RY(x*w[1], wires=1)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[2], wires=0)
            qml.RY(w[3], wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[4], wires=0)
            qml.RY(w[5], wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[6], wires=0)
            qml.RY(w[7], wires=1)
            qml.CNOT(wires=[1, 0])

            return qml.expval(qml.PauliZ(wires=0))
        return _vqc_four(x,w)
    
    def vqc_three_tensor(self,x,w):
        @qml.qnode(self.dev_qubit)
        def _vqc_three_tensor(x, w):
            # qml.Hadamard(wires=0)
            # qml.Hadamard(wires = 1)
            print('entro al circuito cuantico')
            print(x)
            print(w[0].item())
            print(x*w[0].item())
            qml.RY(x*w[0].item(), wires=0)
            qml.RY(x*w[1].item(), wires=1)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[2].item(), wires=0)
            qml.RY(w[3].item(), wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[4].item(), wires=0)
            qml.RY(w[5].item(), wires=0)
            qml.CNOT(wires=[1, 0])

            qml.RX(w[6].item(), wires=0)
            qml.RY(w[7].item(), wires=0)
            qml.CNOT(wires=[1, 0])


            qml.RX(w[8].item(), wires=0)
            qml.RY(w[9].item(), wires=1)
            qml.CNOT(wires=[1, 0])
            return qml.expval(qml.PauliZ(wires=0))
        return _vqc_three_tensor(x,w)
    