# Systematic construction of continuous-time neural networks for linear dynamical systems

This repository is the official implementation of the paper __'Systematic construction of continuous-time neural networks for linear dynamical systems'__. The authors of this work are __Chinmay Datar, Adwait Datar, Felix Dietrich and Wil Schilders__. 

<p align="center" width="100%">
    <img src="dynamic_neural_networks/intro_fig.jpg"> 
<p>
Figure: Overview of our proposed workflow illustrating the systematic construction of continuous-time neural networks from Linear Time-Invariant (LTI) systems. 

## How to use the code? 
# Requirements
To install requirements:

```setup
pip install poetry
poetry install
```

# Numerical examples: Reproducing results in the paper
- __Navigate to the directory - dynamic_neural_networks/numerical_results__: It contains Jupyter notebooks with all examples in the paper: 
    - 1. __diffusion_equation_2d.ipynb__
    - 2. __sparsity_cond_tradeoff.ipynb__
    - 3. __horizontal_layers_all_layers.ipynb__
    - 4. __convection_diffusion_equation_2d.ipynb__ (example in the supplemental meterial)
    - 5.1. __continuous_time_system_idnetification.mlx__ (MATLAB live script to perform system identification)
    - 5.2. __system_idnetification.ipynb__ (DyNN to simulate the identified LTI system)
    - Example in Section SM2.1.3. The directory __adam_sys_id_comparison__ has all scripts for obtaining state-space matrices using (a) system identification methods and (b) gradient-based iterative optimization with ADAM.
        - __adam_training.ipynb__ :  Ex SM2.1.3: Script to learn state-space matrices from data using the Adam optimizer.
        - __adam_dynn_train.ipynb__: Ex SM2.1.3: Simulate the LTI system obtained with the Adam optimzer using a DyNN on train data. 
        - __adam_dynn_test.ipynb__: Ex SM2.1.3: Simulate the LTI system obtained with the Adam optimzer using a DyNN on test data. 
        - __sys_id.mlx__ : Ex SM2.1.3: Script to identify state-space matrices from data using the continuous-time system identification (CTSI) algorithm.
        - __sys_id_dynn_train.ipynb__: Ex SM2.1.3: Simulate the identified LTI system with DyNN on train data. 
        - __sys_id_dynn_test.ipynb__: Ex SM2.1.3: Simulate the identified LTI system with DyNN on test data. 


- __Run the examples__: 
    - You can run the notebook one cell a time or the whole notebook in a single step by clicking on the menu Cell -> Run All. It will generate the data used in the corresponding example and reproduce the results in the paper. 

## Structure (Description of source code and data files)
- __dynamic_neural_networks/numerical_results__: This directory contains Jupyter notebooks containing all numerical examples in the paper.  
- __dynamic_neural_networks/data__: This directory contains Python files containing helper functions that generate data (state-space models) for all examples in the paper. Functions to generate data are called from within each Jupyter notebook containing a numerical example. 
- __dynamic_neural_networks/model__: This directory contains the source code containing the implementation of dynamic neural networks.
- __dynamic_neural_networks/transformations__: This directory contains Python files with helper functions required for pre-processing the given state-space matrices of a linear time-invariant system.
- __dynamic_neural_networks/utils__: This directory contains some helper functions.
- __tests/__: This directory contains our test suite. 

