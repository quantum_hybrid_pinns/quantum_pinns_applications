from dynamic_neural_networks.model.state_space_model import *

#TODO: Write a test for state-space model that checks the compatability of dimensions

class Test_state_space_model:
    def setup_class(self):
        # Mass-spring damper
        self.m = 4.0   # mass
        self.b = 10.0  # damping coefficient
        self.k = 4.0   # spring-constant
        
        # Dimensions
        self.n = 2   # State Input dimension
        self.m = 1   # Input dimension 
        self.l = 2   # Output dimension
        
        # State-Space Model
        self.dim = np.array([self.n, self.m, self.l])
        self.A = np.array([[0.,1.], [-self.k/self.m, -self.b/self.m]])
        self.B = np.array([[0.0], [1.0/self.m]])
        self.C = np.identity(2) # Output: position and velocity
        self.D = np.array([[0.], [0.]])
    
    def test_shapes(self):
        np.testing.assert_equal(self.A.shape, (self.n, self.n))
        np.testing.assert_equal(self.B.shape, (self.n, self.m))
        np.testing.assert_equal(self.C.shape, (self.l, self.n))
        np.testing.assert_equal(self.D.shape, (self.l, self.m))