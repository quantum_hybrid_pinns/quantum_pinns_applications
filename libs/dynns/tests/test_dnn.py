from dynamic_neural_networks.model.dnn import *
from dynamic_neural_networks.model.state_space_model import *
from dynamic_neural_networks.utils.helper_functions import *
from tests.utils.helper_functions import *
import scipy.linalg as LA
import numpy as np
from scipy import signal
from scipy.integrate import solve_ivp
import pytest

def test_real_schur_decomposition():
    A_ut_repeated_evals = np.array([[5.0, 8.0], [0, 5.0]])
    R, Q = LA.schur(A_ut_repeated_evals, output='real')
    np.testing.assert_array_almost_equal(R, A_ut_repeated_evals, decimal=5)
    np.testing.assert_array_almost_equal(Q, np.identity(2), decimal=5)

def test_diagonalizability():
    # Unitarily diagonalizable matrix (Normal matrix AA* = A*A)
    A_normal= np.array([[3., 8.], [8., 5.]])
    R, Q = LA.schur(A_normal, output='real')
    block_dims = [2, 2]
    block_row_sizes = [1, 1] 
    block_col_sizes = [1, 1]
    block_sizes = [block_dims, block_row_sizes, block_col_sizes]
    Z = block_diagonalization(Q, R, block_sizes)
    assert np.allclose(np.dot(Z, Z.T), np.eye(A_normal.shape[0]))  # Z orthonormal
    
    # Non-unitarily diagonalizable matrix
    A_non_normal= np.array([[3., 8.], [0, 5.]])
    R, Q = LA.schur(A_non_normal, output='real')
    assert np.allclose(np.dot(Q, Q.T), np.eye(A_non_normal.shape[0]))  # Z orthonormal

    Z = block_diagonalization(Q, R, block_sizes)
    assert np.allclose(np.dot(Z, Z.T), np.array([[17., 4.], [4., 1.]]))  # Z NOT orthonormal
    

class Test_dynamic_neural_network:         
    def dynamic_neural_network_simulation(self, timesteps, inputs, state_space_model, clustering_alg, params):
        # Simulate the LTI system using a dynamic neural network
        dynn = dynamic_neural_network(ssm=state_space_model)
        y_dynn = dynn.simulate(params, clustering_alg, timesteps, inputs, verbose=0, show_plots=False)
        return y_dynn      
    
    ############################## Parameter Set - 1 ##############################
    # Tests: Real blocks: outer_loop over neurons
    # Tests: Input interp = Piecewise constant, diff num of clusters

    # Design different parameter settings to test the forward pass
        """
        - Errors between solvers for u_interp='sample' are not close to machine zero as 
        lsim interpolates input between the time-points. Numerical solver solve_ivp 
        takes input function handle and so does the DyNN! 

        Raises:
            ValueError: _description_
        """
    @pytest.mark.parametrize("timesteps, inputs, state_space_model, clustering_alg, \
        forward_pass_params, outer_loop, u_interp, rtol, atol, exp_tol_solve_ivp, \
        exp_tol_lsim, exp_tol_bet_solvers", 
        [    
        # Tests: Real blocks: outer_loop over timesteps
            # Tests: Input interp = Piecewise constant, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_real_blocks(n_clusters = 5, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_real_blocks(n_clusters = 3, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),   
            # Tests: Input interp = Piecewise linear, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_real_blocks(n_clusters = 5, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_real_blocks(n_clusters = 3, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
                        
            # Tests: Input interp = sample, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
            params_real_blocks(n_clusters = 5, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
            params_real_blocks(n_clusters = 3, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
                    
        # Tests: Real blocks: outer_loop over timesteps
            # Tests: Input interp = Piecewise constant, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_real_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_real_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                    
            # Tests: Input interp = Piecewise linear, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_real_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_real_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
                        
            # Tests: Input interp = sample, diff num of clusters
            params_real_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
            params_real_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
            params_real_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 5, 3], rtol=1e-12, atol=1e-12) + (1e-11, 1e-3, 1e-3),
        
        # Tests: Complex Blocks: outer_loop over neurons    
            # Tests: Input interp = Piecewise linear, diff num of clusters
            params_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_complex_blocks(n_clusters = 5, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_complex_blocks(n_clusters = 3, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
                        
            # Tests: Input interp = sample, diff num of clusters
            params_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
            params_complex_blocks(n_clusters = 5, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
            params_complex_blocks(n_clusters = 3, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
            
            # TODO: Fix these tests Tests: Input interp = Piecewise constant, diff num of clusters
            #params_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            #params_complex_blocks(n_clusters = 2, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            #params_complex_blocks(n_clusters = 6, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
        
        # Tests: Complex Blocks: outer_loop over timesteps    
            params_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_complex_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
            params_complex_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                    
            # Tests: Input interp = Piecewise linear, diff num of clusters
            params_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_complex_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
            params_complex_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-8, 1e-8, 1e-8),
                        
            # Tests: Input interp = sample, diff num of clusters
            params_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
            params_complex_blocks(n_clusters = 5, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
            params_complex_blocks(n_clusters = 3, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-3, 1e-3),
        
        # Tests: Real-Complex Blocks: outer_loop over neurons 
                # Tests: Input interp = Piecewise linear, diff num of clusters
                params_real_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
                params_real_complex_blocks(n_clusters = 2, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
                params_real_complex_blocks(n_clusters = 6, outer_loop = 'neurons', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
                
                # Tests: Input interp = sample, diff num of clusters
                params_real_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
                params_real_complex_blocks(n_clusters = 2, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
                params_real_complex_blocks(n_clusters = 6, outer_loop = 'neurons', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-7, 1e-7, 1e-7),
        
                # TODO: Fix these tests Tests: Input interp = Piecewise constant, diff num of clusters
                #params_real_complex_blocks(n_clusters = 1, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                #params_real_complex_blocks(n_clusters = 2, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                #params_real_complex_blocks(n_clusters = 6, outer_loop = 'neurons', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
        
        # Tests: Real-Complex Blocks: outer_loop over timesteps
                # Tests: Input interp = piecewise linear, diff num of clusters
                params_real_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-12, 1e-12),
                params_real_complex_blocks(n_clusters = 2, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-12, 1e-12),
                params_real_complex_blocks(n_clusters = 6, outer_loop = 'timesteps', u_interp = 'piecewise_linear', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-12, 1e-12),
                
                # Tests: Input interp = sample, diff num of clusters
                params_real_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-2, 1e-2),
                params_real_complex_blocks(n_clusters = 2, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-2, 1e-2),
                params_real_complex_blocks(n_clusters = 6, outer_loop = 'timesteps', u_interp = 'sample', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-12, 1e-2, 1e-2),
                
                # TODO: Fix these tests Tests: Input interp = Piecewise constant, diff num of clusters
                params_real_complex_blocks(n_clusters = 1, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                params_real_complex_blocks(n_clusters = 2, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
                params_real_complex_blocks(n_clusters = 6, outer_loop = 'timesteps', u_interp = 'piecewise_constant', dims = [2, 10, 3], rtol=1e-12, atol=1e-12) + (1e-10, 1e-10, 1e-10),
        ])      
    def test_forward_pass_outer_loop_neurons(self, timesteps, inputs, state_space_model, clustering_alg, forward_pass_params, outer_loop, u_interp, rtol, atol, exp_tol_solve_ivp, exp_tol_lsim, exp_tol_bet_solvers):
        u = inputs[0]
        # Simulate the LTI system using python routine lsim
        if u_interp == 'piecewise_linear':
            interp = True    
        elif u_interp == 'piecewise_constant':
            interp = False    
        elif u_interp == 'sample':
            interp = True
            t = timesteps
            i = np.arange(0, state_space_model.B.shape[1], 1)
            u = u(np.reshape(t, (len(t), 1)), np.reshape(i, (1, len(i))))
        else:
            raise ValueError('Check u_interp!')
        
        y_lsim = numerical_simulation_lsim(timesteps, u, state_space_model, interp=interp)
        
        # Simulate the LTI system using python routine solve_ivp
        y_solve_ivp = numerical_simulation_ivp_solve(state_space_model, inputs, timesteps, u_interp, outer_loop, rtol, atol)

        # Simulate using a Dynamic Neural Network
        y_dynn = self.dynamic_neural_network_simulation(timesteps, inputs, state_space_model, clustering_alg, forward_pass_params) # dims: timesteps * output
        
        # Compute absolute errors
        abs_err_solve_ivp = np.amax(abs(y_solve_ivp - y_dynn))
        abs_err_lsim = np.amax(abs(y_dynn - y_lsim))
        abs_err_between_solvers = np.amax(abs(y_lsim - y_solve_ivp)) 
    
        # Test if the solutions are equal to numerical precision
        assert(abs_err_solve_ivp < exp_tol_solve_ivp)
        assert(abs_err_between_solvers < exp_tol_bet_solvers)
        assert(abs_err_lsim < exp_tol_lsim)
        
        
class Test_hidden_block_real_repeated:
    def setup_class(self):
        # TODO: Write in the doctring that currently assuming that the entire matrix has just one block with real repeated eigenvalues
        # Initial conditions: y(0), y'(0)
        self.y_0 = 0.0
        self.y_prime_0 = 0.0
        self.y0 = [self.y_0 , self.y_prime_0]

        # Timesteps
        self.t_0 = 0.0
        self.t_end = 1. #25.0 
        self.dt = 0.005 #0.01
        self.timesteps = np.arange(self.t_0,self.t_end + self.dt,self.dt)
        self.u_hat = np.reshape(1 * np.ones(len(self.timesteps)), (len(self.timesteps), 1))
        
        ################################################################################
        ###### Test Case 1 : Single-Input Multiple-Output LTI system - 1 #######
        ################################################################################
        # Dimensions
        self.n1 = 2   # State dimension
        self.m1 = 1   # Input dimension 
        self.l1 = 2   # Output dimension
        self.dim1 = np.array([self.n1, self.m1, self.l1])
        
        # State-Space Matrices 
        self.A1 = np.array([[-5.,2.], [0., -5.]]);
        self.B1 = np.array([[0.0], [1.0]])
        self.C1 = np.identity(2) # Output: position and velocity
        self.D1 = np.array([[0.], [0.]])
        self.eps1 = 1e-8  # Tolerance for multiplicity 
        
        # Input 
        self.u_hat1 = self.u_hat #np.reshape(self.u_hat,(len(self.u_hat), 1))
        
        # State-space model 1
        self.ssm1 = state_space_model(self.A1, self.B1, self.C1, self.D1)
 
        ################################################################################
        ###### Test Case 2 : Single-Input Multi-Output LTI system - 2 #######
        ################################################################################
        # Dimensions
        self.n2 = 4   # State Input dimension
        self.m2 = 1   # Input dimension 
        self.l2 = 4   # Output dimension
        self.dim2 = np.array([self.n2, self.m2, self.l2])
        
        # State-Space Matrices 
        self.A2 = np.array([[-5.,1., 1., 1.], [0.,-5., 1., 1.], [0.,0., -5., 1.], [0.,0., 0., -5.]]);
        self.B2 = np.array([[1.0], [1.0], [1.0], [1.0]])
        self.C2 = np.identity(4) # Output: position and velocity
        self.D2 = np.array([[1.], [2.], [3.], [4.]])
        self.eps2 = 1e-8  # Tolerance for multiplicity 
        
        # Input 
        #self.u_hat2 = self.u_hat #np.reshape(self.u_hat,(len(self.u_hat), 1))
        #self.u_hat2 = np.empty((len(self.timesteps), self.m2))
        #for i in range(self.m2):
        #    self.u_hat2[:, i] = np.sin((i+1)*self.timesteps) # Each input signal with a different frequency
        
        self.u_hat2 = np.sin(self.timesteps).reshape(((len(self.timesteps), self.m2)))
        
        # State-space model 1       
        self.ssm2 = state_space_model(self.A2, self.B2, self.C2, self.D2)
                
        ################################################################################
        ###### Test Case 3 : Multi-Input Multi-Output LTI system - 1 #######
        ################################################################################
        # Dimensions
        self.n3 = 2   # State dimension
        self.m3 = 3   # Input dimension 
        self.l3 = 4   # Output dimension
        self.dim3 = np.array([self.n3, self.m3, self.l3])

        ##########
        self.A3 = np.array([[-5.,2.], [0., -5.]]); # n * n
        self.B3 = np.array([[-5.,2., 1.], [0., -5., 1.]]) # n * m
        self.C3 = np.array([[-5.,2.], [0., -5.], [0., -5.], [0., -5.]])  # l * n
        self.D3 = np.ones((self.l3, self.m3)) # l * m
        self.eps3 = 1e-8  # Tolerance for multiplicity 
        
        # Input 
        self.u_hat3 = np.empty((len(self.timesteps), self.m3))
        for i in range(self.m3):
            self.u_hat3[:, i] = np.sin(i*self.timesteps) # Each input signal with a different frequency
        
        # State-space model 1       
        self.ssm3 = state_space_model(self.A3, self.B3, self.C3, self.D3)
                
                
        
    def test_set_connection_weights_indices(self):
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm1)
        dnn_block_real_rep.set_connection_weights_indices()
        np.testing.assert_array_equal(dnn_block_real_rep.si, np.array([0]))

        
    def test_extract_diagonal(self):
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm1)
        dnn_block_real_rep.extract_diagonal()
        np.testing.assert_array_equal(dnn_block_real_rep.tau_0, np.array([-5., -5.]))


    def test_tau_1(self):
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm1)
        dnn_block_real_rep.extract_diagonal()
        dnn_block_real_rep.compute_tau_1()
        np.testing.assert_array_equal(dnn_block_real_rep.tau_1, np.array([0.2, 0.2]))

        
    def test_initialize_W_xi_tilde(self):
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm1)
        dnn_block_real_rep.set_connection_weights_indices()
        dnn_block_real_rep.extract_diagonal() 
        dnn_block_real_rep.compute_tau_1()
        dnn_block_real_rep.initialize_W_xi_tilde()
        np.testing.assert_equal(dnn_block_real_rep.W_xi_tilde, np.array([0.4]))
        np.testing.assert_equal(np.shape(dnn_block_real_rep.W_xi_tilde)[0], (int(self.n1 * (self.n1 - 1)/2)))


    def test_forward_pass_1(self):
        # Test case 1: 
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm1)
        dnn_block_real_rep.compute_weights()
        y_dnn, dnn_sol = dnn_block_real_rep.forward_pass(self.timesteps, a_1 = .5, a_2 = .5, h = self.dt, inputs = [self.u_hat])
        
        # Extract the states for all time-steps
        x_1_dnn = dnn_sol[:, 0, 0]
        x_2_dnn = dnn_sol[:, 1, 0]

        # Extract the outputs for all time-steps
        y_1_dnn = y_dnn[:, 0]
        y_2_dnn = y_dnn[:, 1]
        
        # Simulate using an in-built python routine signal.lti
        ssmodel_t = signal.lti(self.A1, self.B1, self.C1, self.D1)# control.ss(A, B, C, D)
        t_t, y_t, x_t = signal.lsim(ssmodel_t, self.u_hat, self.timesteps)
        x_1_t = x_t[:,0]
        x_2_t = x_t[:,1]
        y_1_t = y_t[:,0]
        y_2_t = y_t[:,1]
        
        # Check shape
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), self.n1, 2))

        # Compare predicted output   
        np.testing.assert_array_almost_equal(x_1_t, x_1_dnn, decimal=3)
        np.testing.assert_array_almost_equal(x_2_t, x_2_dnn, decimal=3)
        
        # Compare predicted output   
        np.testing.assert_array_almost_equal(y_1_t, y_1_dnn, decimal=3)
        np.testing.assert_array_almost_equal(y_2_t, y_2_dnn, decimal=3)
       
    def test_forward_pass_2(self):
        # Test case 2: 
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm2)
        dnn_block_real_rep.compute_weights()
        y_dnn, dnn_sol = dnn_block_real_rep.forward_pass(self.timesteps, a_1 = .5, a_2 = .5, h = self.dt, inputs = [self.u_hat2])
            
        # Extract the states for all time-steps
        x_1_dnn = dnn_sol[:, 0, 0]
        x_2_dnn = dnn_sol[:, 1, 0]
        x_3_dnn = dnn_sol[:, 2, 0]
        x_4_dnn = dnn_sol[:, 3, 0]
            
        # Add the contribution from the input term (D * u_hat)
        y_dnn += np.transpose(np.matmul(self.D2, self.u_hat2.T))
        
        # Extract the output for all time-steps 
        y_1_dnn = y_dnn[:, 0]
        y_2_dnn = y_dnn[:, 1]
        y_3_dnn = y_dnn[:, 2]
        y_4_dnn = y_dnn[:, 3]
        
        # Simulate using an in-built python routine signal.lti
        ssmodel_t = signal.lti(self.A2, self.B2, self.C2, self.D2)# control.ss(A, B, C, D)
        t_t, y_t, x_t = signal.lsim(ssmodel_t, self.u_hat2, self.timesteps)
        x_1_t = x_t[:,0]
        x_2_t = x_t[:,1]
        x_3_t = x_t[:,2]
        x_4_t = x_t[:,3]
        
        y_1_t = y_t[:,0]
        y_2_t = y_t[:,1]      
        y_3_t = y_t[:,2]
        y_4_t = y_t[:,3]       
        # Check the shape of the output 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), self.n2, 2))
            
        # Compare predicted output   
        np.testing.assert_array_almost_equal(x_1_t, x_1_dnn, decimal=3)
        np.testing.assert_array_almost_equal(x_2_t, x_2_dnn, decimal=3)
        np.testing.assert_array_almost_equal(x_3_t, x_3_dnn, decimal=3)
        np.testing.assert_array_almost_equal(x_4_t, x_4_dnn, decimal=3)

        # Compare predicted output   
        np.testing.assert_array_almost_equal(y_1_t, y_1_dnn, decimal=3)
        np.testing.assert_array_almost_equal(y_2_t, y_2_dnn, decimal=3)
        np.testing.assert_array_almost_equal(y_3_t, y_3_dnn, decimal=3)
        np.testing.assert_array_almost_equal(y_4_t, y_4_dnn, decimal=3)   
    
    def test_forward_pass_3(self):
        # Test case 2: 
        dnn_block_real_rep = hidden_block_real_repeated(self.ssm3)
        dnn_block_real_rep.compute_weights()
        y_dnn, dnn_sol = dnn_block_real_rep.forward_pass(self.timesteps, a_1 = .5, a_2 = .5, h = self.dt, inputs = [self.u_hat3])
            
        # Extract the states for all time-steps
        x_1_dnn = dnn_sol[:, 0, 0]
        x_2_dnn = dnn_sol[:, 1, 0]

        # Extract the output for all time-steps
        # Add the contribution from the input term (D * u_hat)
        y_dnn += np.transpose(np.matmul(self.D3, self.u_hat3.T))
        
        y_1_dnn = y_dnn[:,0]
        y_2_dnn = y_dnn[:,1]
        y_3_dnn = y_dnn[:,2]
        y_4_dnn = y_dnn[:,3]
            
        # Simulate using an in-built python routine signal.lti
        ssmodel_t = signal.lti(self.A3, self.B3, self.C3, self.D3)# control.ss(A, B, C, D)
        t_t, y_t, x_t = signal.lsim(ssmodel_t, self.u_hat3, self.timesteps)
        x_1_t = x_t[:,0]
        x_2_t = x_t[:,1]
        
        y_1_t = y_t[:,0]
        y_2_t = y_t[:,1]
        y_3_t = y_t[:,2]
        y_4_t = y_t[:,3]
        
        # Check the shape of the output 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), self.n3, 2))
            
        # Compare states    
        np.testing.assert_array_almost_equal(x_1_t, x_1_dnn, decimal=2)
        np.testing.assert_array_almost_equal(x_2_t, x_2_dnn, decimal=2)
        
        # Compare output
        np.testing.assert_array_almost_equal(y_1_t, y_1_dnn, decimal=2)
        np.testing.assert_array_almost_equal(y_2_t, y_2_dnn, decimal=2)
        np.testing.assert_array_almost_equal(y_3_t, y_3_dnn, decimal=2)
        np.testing.assert_array_almost_equal(y_4_t, y_4_dnn, decimal=2)



class Test_hidden_block_complex_repeated:
    @classmethod
    def setup_class(self):
        # TODO: Write in the doctring that currently assuming that the entire matrix has just one block with real repeated eigenvalues       
        # Initial conditions: y(0), y'(0)
        self.y_0 = 0.0
        self.y_prime_0 = 0.0
        self.y0 = [self.y_0 , self.y_prime_0]

        # Timesteps
        self.t_0 = 0.0
        self.t_end = 5.0 #25.0 
        self.dt = 0.005 #0.01
        self.timesteps = np.arange(self.t_0,self.t_end + self.dt,self.dt)

        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################
        # Dimensions
        self.n1 = 4   # State Input dimension
        self.m1 = 4   # Input dimension 
        self.l1 = 4   # Output dimension
        self.dim1 = np.array([self.n1, self.m1, self.l1])

        # Toy example of matrix A
        # TODO: Define a complex no. object /use existing object
        self.ev1_r1 = -2.
        self.ev1_c1 = -3.
        self.ev2_r1 = -2.
        self.ev2_c1 = -3.
        self.A1 = np.array([[self.ev1_r1,-self.ev1_c1, -1., -2.],
                    [self.ev1_c1, self.ev1_r1, -3., -4.],
                    [0., 0., self.ev2_r1,-self.ev2_c1,],
                    [0., 0., self.ev2_c1, self.ev2_r1]]); 

        self.B1 = np.ones((self.n1,self.m1)) 
        self.C1 = np.ones((self.l1,self.n1)) 
        self.D1 = np.zeros((self.l1,self.m1))
        self.eps1 = 1e-8  # Tolerance for multiplicity 
        
        # State-space model 1
        self.ssm1 = state_space_model(self.A1, self.B1, self.C1, self.D1)  
        
        # Input u(t) and u'(t)
        # Input 1.1
        self.u_hat1 = np.empty((len(self.timesteps), self.m1))
        self.u_hat_prime1 = np.empty((len(self.timesteps), self.m1))
        for i in range(self.m1):
            self.u_hat1[:, i] = np.ones(len(self.timesteps))
            self.u_hat_prime1[:, i] = np.zeros(len(self.timesteps))

        self.inputs = [self.u_hat1, self.u_hat_prime1]

        # Input 1.2
        self.u_hat12 = np.empty((len(self.timesteps), self.m1))
        self.u_hat_prime12 = np.empty((len(self.timesteps), self.m1))
        for i in range(self.m1):
            self.u_hat12[:, i] = np.sin(i * self.timesteps)/(2.0 * self.m1) # Each input signal with a different frequency
            self.u_hat_prime12[:, i] = (i) * np.cos(i*self.timesteps)/(2.0 * self.m1)
        
        self.inputs12 = [self.u_hat12, self.u_hat_prime12]


    
        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Dimensions
        self.n2 = 8   # State Input dimension
        self.m2 = 10   # Input dimension 
        self.l2 = 12   # Output dimension
        self.dim2 = np.array([self.n2, self.m2, self.l2])

        # Toy example of matrix A
        # TODO: Define a complex no. object /use existing object
        self.ev1_r2 = -2.
        self.ev1_c2 = -3.
        self.ev2_r2 = -2.
        self.ev2_c2 = -3.
        self.A2 = np.array([
                    [self.ev1_r2,-self.ev1_c2, -1., -2., -1., -2., -1., -2.],
                    [self.ev1_c2, self.ev1_r2, -3., -4.,-1., -2.,-1., -2.],
                    [0., 0., self.ev2_r2,-self.ev2_c2,-1., -2., -1., -2],
                    [0., 0., self.ev2_c2, self.ev2_r2, -1., -2., -1., -2],
                    [0., 0., 0., 0., self.ev2_r2,-self.ev2_c2,-1., -2.],
                    [0., 0., 0., 0., self.ev2_c2, self.ev2_r2, -1., -2.], 
                    [0., 0., 0., 0.,0., 0.,self.ev2_r2,-self.ev2_c2],
                    [0., 0., 0., 0., 0., 0., self.ev2_c2, self.ev2_r2,],                  
                    ]); 

        self.B2 = np.ones((self.n2,self.m2)) 
        self.C2 = np.ones((self.l2,self.n2)) 
        self.D2 = np.zeros((self.l2,self.m2))
        self.eps2 = 1e-8  # Tolerance for multiplicity 
        
        # State-space model 2
        self.ssm2 = state_space_model(self.A2, self.B2, self.C2, self.D2)  
        
        # Input u(t) and u'(t)
        self.u_hat2 = np.empty((len(self.timesteps), self.m2))
        self.u_hat_prime2 = np.empty((len(self.timesteps), self.m2))
        for i in range(self.m2):
            self.u_hat2[:, i] = np.sin(i * self.timesteps)/(2.0 * self.m2) # Each input signal with a different frequency
            self.u_hat_prime2[:, i] = (i) * np.cos(i*self.timesteps)/(2.0 * self.m2)
        
        self.inputs2 = [self.u_hat2, self.u_hat_prime2]
        
        ################################################################################
        ###### Test Case 3 : 2 * 2 system (complex conjugate eigenvalues with AM = 1) #######
        ################################################################################
        # Dimensions
        self.n3 = 2   # State Input dimension
        self.m3 = 2   # Input dimension 
        self.l3 = 2   # Output dimension
        self.dim3 = np.array([self.n3, self.m3, self.l3])

        ev1_r1 = -2.
        ev1_c1 = -3.
        ev2_r1 = -2.
        ev2_c1 = -3.
        self.A3 = np.array([[ev1_r1,-ev1_c1],
                    [ev1_c1, ev1_r1]]); 
        self.B3 = np.ones((self.n3, self.m3)) 
        self.C3 = np.ones((self.l3, self.n3)) 
        self.D3 = np.zeros((self.l3, self.m3)) 
        self.eps3 = 1e-8  # Tolerance for multiplicity 

        # Input 2
        """
        self.u_hat2 = np.empty((len(self.timesteps), self.m2))
        for i in range(self.m2):
            self.u_hat2[:, i] = np.ones(len(self.timesteps))
        """
        # State-space model 2
        self.ssm3 = state_space_model(self.A3, self.B3, self.C3, self.D3)  
        
        # Input u(t) and u'(t)
        self.u_hat3 = np.empty((len(self.timesteps), self.m3))
        self.u_hat_prime3 = np.empty((len(self.timesteps), self.m3))
        for i in range(self.m3):
            self.u_hat3[:, i] = np.sin(i * self.timesteps)/(2.0 * self.m3) # Each input signal with a different frequency
            self.u_hat_prime3[:, i] = (i) * np.cos(i*self.timesteps)/(2.0 * self.m3)
        
        self.inputs3 = [self.u_hat3, self.u_hat_prime3]

    
    def test_compute_c_xi(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################        
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi() # = hidden_block_complex_repeated(ssm_new, u_hat)
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.c_xi[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.c_xi[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.c_xi[0], np.reshape(np.array([0.66666667, 0.77777778]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.c_xi[1], np.reshape(np.array([0.66666667]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi()
        
        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.c_xi[p-1]), (1, k2 + 1 - p))
        
    def test_compute_c_xi_prime(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi_prime()
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.c_xi_prime[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.c_xi_prime[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.c_xi_prime[0], np.reshape(np.array([1./3, 2./9]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.c_xi_prime[1], np.reshape(np.array([1./3]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi_prime()
        
        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.c_xi_prime[p-1]), (1, k2 + 1 - p))
    
    def test_compute_c_u_hat(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_u_hat()
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.c_u_hat[0]), (1, self.m1))
        np.testing.assert_equal(np.shape(DNN_k1.c_u_hat[1]), (1, self.m1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.c_u_hat[0], np.reshape(np.array([-5./9, -5./9, -5./9, -5./9]), (1,self.m1)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.c_u_hat[1], np.reshape(np.array([-1./3, -1./3, -1./3, -1./3]), (1,self.m1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_u_hat()
        
        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.c_u_hat[p-1]), (1, self.m2))

    def test_compute_alpha(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.alpha[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.alpha[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.alpha[0], np.reshape(np.array([-13./3, -65./9]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.alpha[1], np.reshape(np.array([-13./3]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi() 
        DNN_k2.compute_alpha() 

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.alpha[p-1]), (1, k2 + 1 - p))

    def test_compute_beta(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi_prime()
        DNN_k1.compute_beta() 
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.beta[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.beta[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.beta[0], np.reshape(np.array([-2./3, -16./9]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.beta[1], np.reshape(np.array([-2./3]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi_prime() 
        DNN_k2.compute_beta() 

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.beta[p-1]), (1, k2 + 1 - p))

    def test_compute_gamma(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_u_hat()
        DNN_k1.compute_gamma()
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.gamma[0]), (1, self.m1))
        np.testing.assert_equal(np.shape(DNN_k1.gamma[1]), (1, self.m1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.gamma[0], np.reshape(np.array([31./9, 31./9, 31./9, 31./9]), (1,self.m1)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.gamma[1], np.reshape(np.array([5./3, 5./3, 5./3, 5./3]), (1,self.m1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_u_hat()
        DNN_k2.compute_gamma()

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.gamma[p-1]), (1, self.m2))

 
    def test_compute_kappa(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_u_hat() 
        DNN_k1.compute_gamma() 
        DNN_k1.compute_kappa() 


        # Test c_u_hat, gamma
        np.testing.assert_array_almost_equal(DNN_k1.c_u_hat[0], np.reshape(np.array([-5./9, -5./9, -5./9, -5./9]), (1,self.m1)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.c_u_hat[1], np.reshape(np.array([-1./3, -1./3, -1./3, -1./3]), (1,self.m1)),decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.gamma[0], np.reshape(np.array([31./9, 31./9, 31./9, 31./9]), (1,self.m1)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.gamma[1], np.reshape(np.array([5./3, 5./3, 5./3, 5./3]), (1,self.m1)),decimal=5)

        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.kappa[0], np.reshape(np.array([7., 7., 7., 7.]), (1,self.m1)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.kappa[1], np.reshape(np.array([5., 5., 5., 5.]), (1,self.m1)),decimal=5)
        
        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_u_hat()
        DNN_k2.compute_gamma()
        DNN_k2.compute_kappa()

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.kappa[p-1]), (1, self.m2))
        
        
    def test_compute_mu(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.mu[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.mu[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.mu[0], np.reshape(np.array([-13., -13.]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.mu[1], np.reshape(np.array([-13.]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi() 
        DNN_k2.compute_alpha() 
        DNN_k2.compute_mu() 

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.mu[p-1]), (1, k2 + 1 - p))

    def test_compute_nu(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi_prime()
        DNN_k1.compute_beta() 
        DNN_k1.compute_nu() 
        
        # Test shapes
        np.testing.assert_equal(np.shape(DNN_k1.nu[0]), (1, 2))
        np.testing.assert_equal(np.shape(DNN_k1.nu[1]), (1, 1))
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.nu[0], np.reshape(np.array([-2., -4.]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.nu[1], np.reshape(np.array([-2.]), (1,1)),decimal=5)

        ################################################################################
        ###### Test Case 2 : Validation of shapes and conformal operations #######
        ################################################################################
        # Test shapes
        DNN_k2 = hidden_block_complex_repeated(self.ssm2)
        DNN_k2.compute_c_xi_prime()
        DNN_k2.compute_beta() 
        DNN_k2.compute_nu() 

        k2 = int(self.n2/2)
        for p in reversed(range(1, k2 + 1)):
            np.testing.assert_equal(np.shape(DNN_k2.nu[p-1]), (1, k2 + 1 - p))


        
    def test_compute_tau2(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.compute_tau_2() 
                
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.tau_2, np.array([1./13, 1./13]), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.tau_2[0], DNN_k1.tau_2[1], decimal=5)
        
        
    def test_compute_tau1(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.compute_c_xi_prime()
        DNN_k1.compute_beta() 
        DNN_k1.compute_nu() 
        DNN_k1.compute_tau_1() 
                
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.tau_1, np.array([4./13, 4./13]), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.tau_1[0], DNN_k1.tau_1[1], decimal=5)
    

    def test_initialize_W_xi_tilde(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.initialize_W_xi_tilde() 
        
        k1 = int(self.n1/2)
        np.testing.assert_array_almost_equal(DNN_k1.W_xi_tilde, np.array([-1.]))
        np.testing.assert_equal(np.shape(DNN_k1.W_xi_tilde)[0], (int(k1 * (k1 - 1)/2)))
    

    def test_initialize_V_xi_tilde(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.compute_c_xi_prime()
        DNN_k1.compute_beta() 
        DNN_k1.compute_nu() 
        DNN_k1.initialize_V_xi_tilde() 

        k1 = int(self.n1/2)
        np.testing.assert_array_almost_equal(DNN_k1.V_xi_tilde, np.array([-5./13]))
        np.testing.assert_equal(np.shape(DNN_k1.V_xi_tilde)[0], (int(k1 * (k1 - 1)/2)))
    
        
    def test_compute_W_u_hat(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)  
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.compute_c_u_hat() 
        DNN_k1.compute_gamma() 
        DNN_k1.compute_kappa()   
        DNN_k1.compute_W_u_hat()
        
        k1 = int(self.n1/2)
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[0], np.array([7./13, 7./13, 7./13, 7./13]))
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[1], np.array([5./13, 5./13, 5./13, 5./13]))
        np.testing.assert_equal(np.shape(DNN_k1.W_u_hat), (k1, self.m1))


    def test_compute_V_u_hat(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)  
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi()
        DNN_k1.compute_alpha() 
        DNN_k1.compute_mu() 
        DNN_k1.compute_V_u_hat()
        
        k1 = int(self.n1/2)
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[0], np.array([1./13, 1./13, 1./13, 1./13]))
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[1], np.array([1./13, 1./13, 1./13, 1./13]))
        np.testing.assert_equal(np.shape(DNN_k1.V_u_hat), (k1, self.m1))

    def test_compute_Out_W_xi_tilde(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)  
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi()
        DNN_k1.compute_Out_W_xi_tilde()
        
        np.testing.assert_array_almost_equal(DNN_k1.Out_W_xi_tilde, np.array([[5./3, 22./9],[5./3, 22./9], [5./3, 22./9], [5./3, 22./9]]))

    def test_compute_Out_V_xi_tilde(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)  
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_xi_prime()
        DNN_k1.compute_Out_V_xi_tilde()
        
        np.testing.assert_array_almost_equal(DNN_k1.Out_V_xi_tilde, np.array([[1./3, 5./9],[1./3, 5./9], [1./3, 5./9], [1./3, 5./9]]))


    def test_compute_Out_W_u_hat(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)  
        DNN_k1.set_connection_weights_indices()
        DNN_k1.compute_c_u_hat()
        DNN_k1.compute_Out_W_u_hat()
        
        np.testing.assert_array_almost_equal(DNN_k1.Out_W_u_hat, (-8./9) * np.ones((self.ssm1.dim[2], self.ssm1.dim[1])))


    def test_forward_pass_test1(self):
        ################################################################################
        ###### Test Case 1 : 4 * 4 system #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm1)
        
        # Compute all the coefficients required for the mapping from the SSM --> Weights of DNN
        DNN_k1.compute_weights()

        out_dnn, dnn_sol = DNN_k1.forward_pass_scipy(self.timesteps, h = self.dt, inputs = self.inputs12)
            
        # Extract states at all time-steps 
        x_dnn = np.transpose(dnn_sol[:, :, 0])

        # Extract all derivatives of states
        x_prime_dnn = np.transpose(dnn_sol[:, :, 1])

        n_neurons = int(self.n1/2)

        for k in range(n_neurons):
            exec(f'x_{k+1}_dnn = dnn_sol[:, k, 0]')
        
        for k in range(self.l1):
            exec(f'y_{k+1}_dnn = out_dnn[:, k]')
            
        # Simulate using an in-built python routine signal.lti
        ssm_val = signal.lti(self.A1, self.B1, self.C1, self.D1)
        t_t, y_t, x_t = signal.lsim(ssm_val, self.u_hat12, self.timesteps)

        # Extract output at all time-steps
        for k in range(self.l1):
            exec(f'y_{k+1}_t = y_t[:,k]')
       
        # Check the shape of the stored states 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), n_neurons, 2))

        # Compare predicted output   
        for k in range(self.l1):
            np.testing.assert_equal(np.shape(eval(f'y_{k+1}_t')), np.shape(eval(f'y_{k+1}_dnn')))
            assert(np.amax(np.abs(eval(f'y_{k+1}_t') - eval(f'y_{k+1}_dnn'))) < 0.5)
    
    def test_forward_pass_test2(self):
        ################################################################################
        ###### Test Case 2 : n = 8, m = 10, l = 12 #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm2)
        
        # Compute all the coefficients required for the mapping from the SSM --> Weights of DNN
        DNN_k1.compute_weights()

        out_dnn, dnn_sol = DNN_k1.forward_pass_scipy(self.timesteps, h = self.dt, inputs = self.inputs2)
            
        # Extract states at all time-steps 
        x_dnn = np.transpose(dnn_sol[:, :, 0])

        # Extract all derivatives of states
        x_prime_dnn = np.transpose(dnn_sol[:, :, 1])

        n_neurons = int(self.n2/2)

        for k in range(n_neurons):
            exec(f'x_{k+1}_dnn = dnn_sol[:, k, 0]')
        
        for k in range(self.l2):
            exec(f'y_{k+1}_dnn = out_dnn[:, k]')
            
        # Simulate using an in-built python routine signal.lti
        ssm_val = signal.lti(self.A2, self.B2, self.C2, self.D2)
        t_t, y_t, x_t = signal.lsim(ssm_val, self.u_hat2, self.timesteps)

        # Extract output at all time-steps
        for k in range(self.l2):
            exec(f'y_{k+1}_t = y_t[:,k]')
       
        # Check the shape of the stored states 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), n_neurons, 2))

        # Compare predicted output   
        for k in range(self.l2):
            np.testing.assert_equal(np.shape(eval(f'y_{k+1}_t')), np.shape(eval(f'y_{k+1}_dnn')))
            assert(np.amax(np.abs(eval(f'y_{k+1}_t') - eval(f'y_{k+1}_dnn'))) < 0.99)


    def test_forward_pass_test3(self):
        ################################################################################
        ###### Test Case 3 : 2 * 2 system #######
        ################################################################################    
        DNN_k1 = hidden_block_complex_repeated(self.ssm3)
        
        # Compute all the coefficients required for the mapping from the SSM --> Weights of DNN
        DNN_k1.compute_weights()
        
        out_dnn, dnn_sol = DNN_k1.forward_pass_scipy(self.timesteps, h = self.dt, inputs = self.inputs3)
            
        # Extract states at all time-steps 
        x_dnn = np.transpose(dnn_sol[:, :, 0])

        # Extract all derivatives of states
        x_prime_dnn = np.transpose(dnn_sol[:, :, 1])

        n_neurons = int(self.n3/2)

        for k in range(n_neurons):
            exec(f'x_{k+1}_dnn = dnn_sol[:, k, 0]')
        
        for k in range(self.l3):
            exec(f'y_{k+1}_dnn = out_dnn[:, k]')
            
        # Simulate using an in-built python routine signal.lti
        ssm_val = signal.lti(self.A3, self.B3, self.C3, self.D3)
        t_t, y_t, x_t = signal.lsim(ssm_val, self.u_hat3, self.timesteps)

        # Extract output at all time-steps
        for k in range(self.l3):
            exec(f'y_{k+1}_t = y_t[:,k]')
       
        # Check the shape of the stored states 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), n_neurons, 2))

        # Compare predicted output   
        for k in range(self.l3):
            np.testing.assert_equal(np.shape(eval(f'y_{k+1}_t')), np.shape(eval(f'y_{k+1}_dnn')))
            assert(np.amax(np.abs(eval(f'y_{k+1}_t') - eval(f'y_{k+1}_dnn'))) < 0.5)



class Test_hidden_block_real_complex:
    @classmethod
    def setup_class(self):
        # TODO: Write in the doctring that currently assuming that the entire matrix has just one block with real repeated eigenvalues       
        # Dimensions
        self.n1 = 6   # State dimension
        self.m1 = 6   # Input dimension 
        self.l1 = 6  # Output dimension
        self.dim1 = [self.n1, self.m1, self.l1]
        
        # Timesteps
        self.t_0 = 0.0   # Initial time
        self.t_end = 5.0 # Final time 
        self.dt = 0.0005 # Time-step size
        self.timesteps = np.arange(self.t_0, self.t_end + self.dt, self.dt)  # All time-steps

        ############## State space matrices ############## 
        ev1_r1 = -2.
        ev1_c1 = -3.
        ev2_r1 = -2.
        ev2_c1 = -3.
        self.A1 = np.array(
                    [[-2, -1, 1., 2., 3., 4.],
                    [0., -2., 4., 3., 2., 1.],
                    [0., 0., ev1_r1,-ev1_c1, -1., -2.],
                    [0., 0., ev1_c1, ev1_r1, -3., -4.],
                    [0., 0., 0., 0., ev2_r1,-ev2_c1,],
                    [0., 0., 0., 0., ev2_c1, ev2_r1]]); 
        self.B1 = np.ones((self.n1, self.m1)) 
        self.C1 = np.ones((self.l1, self.n1)) 
        self.D1 = np.zeros((self.l1, self.m1))   
        self.eps1 = 1e-8  # Tolerance for multiplicity 

        # State-space model
        self.ssm1 = state_space_model(self.A1, self.B1, self.C1, self.D1)  
        self.n_rc1 = [2, 2]
        # Input u(t) and u'(t)
        # Input 1.1
        self.u_hat1 = np.empty((len(self.timesteps), self.m1))
        self.u_hat_prime1 = np.empty((len(self.timesteps), self.m1))
        for i in range(self.m1):
            self.u_hat1[:, i] = np.ones(len(self.timesteps))
            self.u_hat_prime1[:, i] = np.zeros(len(self.timesteps))

        self.inputs = [self.u_hat1, self.u_hat_prime1]

        # Input 1.2
        self.u_hat2 = np.empty((len(self.timesteps), self.m1))
        self.u_hat_prime2 = np.empty((len(self.timesteps), self.m1))
        for i in range(self.m1):
            self.u_hat2[:, i] = np.sin(i * self.timesteps)/(self.m1) # Each input signal with a different frequency
            self.u_hat_prime2[:, i] = (i) * np.cos(i*self.timesteps)/(self.m1)
        
        self.inputs2 = [self.u_hat2, self.u_hat_prime2]
        
        # Initial conditions: y(0), y'(0)
        self.y_0 = 0.0
        self.y_prime_0 = 0.0
        self.y0 = [self.y_0 , self.y_prime_0]
        
    def test_tau_1(self):
        ################################################################################
        ###### Test Case 1 : Validation with pen-paper computation #######
        ################################################################################    
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()                
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.tau_1, np.array([1./2, 1./2, 4./13, 4./13]), decimal=5)
    
    def test_tau_2(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()    
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.tau_2, np.array([0., 0., 1./13, 1./13]), decimal=5)

    def test_mu_c(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()         
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.mu_c[0], np.reshape(np.array([7./3, 7.22222]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.mu_c[1], np.reshape(np.array([6., 5.]), (1,2)), decimal=5)
    
    def test_nu_c(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()         
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.nu_c[0], np.reshape(np.array([2./3, 16./9]), (1,2)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.nu_c[1], np.reshape(np.array([1., 1.]), (1,2)), decimal=5)

   
    def test_kappa_c(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()         
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.kappa_c[0], np.reshape(np.array([-13./9,-13./9,-13./9,-13./9,-13./9,-13./9]), (1, 6)), decimal=5)
        np.testing.assert_array_almost_equal(DNN_k1.kappa_c[1], np.reshape(np.array([-1.,-1.,-1.,-1.,-1.,-1.]), (1, 6)), decimal=5)

    def test_W_xi_tilde(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()         
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.W_xi_tilde, np.array([-1./2, 7./6, 7.2222222/2, 3., 2.5, -1.]), decimal=5)
            
       
    def test_V_xi_tilde(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights()         
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.V_xi_tilde, np.array([0, 1./3, 8./9, 1./2, 1./2, -0.384615]), decimal=5)
            
    def test_W_u_hat(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights() 
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[0], -0.72222222 * np.ones((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[1], -0.5 * np.ones((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[2], 0.53846154 * np.ones((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.W_u_hat[3], 0.38461538 * np.ones((6,)))
        np.testing.assert_equal(np.shape(DNN_k1.W_u_hat), (4, self.m1))

    def test_V_u_hat(self):
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights() 
        
        # Test values
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[0],np.zeros((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[1], np.zeros((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[2], 0.07692308 * np.ones((6,)))
        np.testing.assert_array_almost_equal(DNN_k1.V_u_hat[3], 0.07692308 * np.ones((6,)))
        np.testing.assert_equal(np.shape(DNN_k1.V_u_hat), (4, self.m1))
    
    def test_forward_pass_test1(self):
        ################################################################################
        ###### Test Case 1 : 4 * 4 system #######
        ################################################################################    
        DNN_k1 = hidden_block_real_complex(self.ssm1, self.n_rc1)
        DNN_k1.compute_weights() 
               
        # out_dnn, dnn_sol = DNN_k1.forward_pass(self.timesteps, a_1 = 0.5, a_2 = 0.5, h = self.dt)

        out_dnn, dnn_sol = DNN_k1.forward_pass_scipy(self.timesteps, h = self.dt, inputs = self.inputs2)
            
        # Extract states at all time-steps 
        x_dnn = np.transpose(dnn_sol[:, :, 0])

        # Extract all derivatives of states
        x_prime_dnn = np.transpose(dnn_sol[:, :, 1])

        
        for k in range(self.l1):
            exec(f'y_{k+1}_dnn = out_dnn[:, k]')
            
        # Simulate using an in-built python routine signal.lti
        ssm_val = signal.lti(self.A1, self.B1, self.C1, self.D1)
        t_t, y_t, x_t = signal.lsim(ssm_val, self.u_hat2, self.timesteps)

        n_neurons = 4
        # Extract output at all time-steps
        for k in range(self.l1):
            exec(f'y_{k+1}_t = y_t[:,k]')
       
        # Check the shape of the stored states 
        np.testing.assert_equal(np.shape(dnn_sol), (len(self.timesteps), n_neurons, 2))

        # Compare predicted output   
        for k in range(self.l1):
            np.testing.assert_equal(np.shape(eval(f'y_{k+1}_t')), np.shape(eval(f'y_{k+1}_dnn')))
            assert(np.amax(np.abs(eval(f'y_{k+1}_t') - eval(f'y_{k+1}_dnn'))) < 0.9)
    


        
        
"""  
    def numerical_simulation_solve_ivp(self):
        # Initialize the state and state-derivative set to zero
        x_gt = np.zeros((self.n)) # 1 state per timestep
        y_solve_ivp = np.zeros((len(self.timesteps), self.l)) # timesteps, neurons in the output layer

        def dx_dt(t, x, A, B, u):
            dx_dt = A @ x + B @ u
            return dx_dt
            
        for t in range(1, len(self.timesteps)):                    
            # Set initial conditions of the ODE
            x_old = x_gt
            u_old = self.input[t-1]
                            
            sol = solve_ivp(dx_dt, [self.timesteps[t-1], self.timesteps[t]], x_old,
                            args= (self.A, self.B, u_old.T), 
                            t_eval=np.array([self.timesteps[t-1], self.timesteps[t]])
                            , method='DOP853')#, method='LSODA'
            
            # Update the state (Required for computing the forcing term)
            x_gt = sol.y.T[-1] #, -1            
            y_solve_ivp[t, :] = np.dot(self.C, x_gt) + np.dot(self.D, self.input[t].T)   
        
        return y_solve_ivp
    
    
    def numerical_simulation_lsim(self):
        # Simulate the transformed LTI system
        ssm_val = signal.lti(self.state_space_model.A, self.state_space_model.B, 
                            self.state_space_model.C, self.state_space_model.D)
        t_t, y_lsim, x_t = signal.lsim(ssm_val, self.input, self.timesteps, interp=False)  
        return y_lsim
""" 