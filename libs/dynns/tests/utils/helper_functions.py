import numpy as np
from dynamic_neural_networks.model.dnn import *
from dynamic_neural_networks.model.state_space_model import *


def params_real_blocks(n_clusters, outer_loop, u_interp, dims, rtol=1e-10, atol=1e-10):
    ###########    Time   ###########   
    t_0 = 0.   # Initial time
    t_end = 2. # Final time 
    dt = 1e-1  # time-step size
    timesteps = np.arange(t_0, t_end + dt, dt)  # All time-steps
        
    ###########    Dimensions   ###########   
    n = dims[1]  # State dimension
    m = dims[0]  # Input dimension 
    l = dims[2]  # Output dimension
    ssm_dim = [n, m, l]  # Dimensions of the state-space model

    np.random.seed(4)
    A = -np.triu((np.random.rand(n, n)))
    np.fill_diagonal(A, -np.arange(1, n + 1))
    B = np.random.rand(n, m)
    C = np.random.rand(l, n)
    D = np.random.rand(l, m) 
    ssm = state_space_model(A, B, C, D)

    # Inputs: u(t) and u'(t)
    # Compute u'(t) based on the approximation of u: piecewise const, piecewise linear, or sampling the input function (if known)
    if u_interp == 'piecewise_constant': 
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency

    elif u_interp == 'piecewise_linear':
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency
        for t in range(0, len(timesteps) - 1):   
            du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])
            
    elif u_interp == 'sample':
        u = lambda t, i: np.sin(t @ i)
        du_dt = lambda t, i: i * np.cos(t @ i)
        
    else:
        raise ValueError('Check u_interp! ')
    
    inputs = [u, du_dt]
    
    clustering_alg = SpectralClustering(n_clusters=n_clusters,  
                    n_neighbors=5, 
                    affinity='nearest_neighbors',
                    gamma=1.0 )
    forward_pass_params = forward_pass_parameters(outer_loop=outer_loop, 
                    ssm_input_interpolation=u_interp,
                    solver_routine ='solve_ivp',  
                    ode_solver_params = ode_solver_parameters(atol=atol, rtol=rtol, \
                    ode_solver='DOP853'))
    return (timesteps, inputs, ssm, \
        clustering_alg, forward_pass_params, outer_loop, u_interp, \
            rtol, atol)


def params_complex_blocks(n_clusters, outer_loop, u_interp, dims, rtol=1e-10, atol=1e-10):
    ###########    Time   ###########   
    t_0 = 0.   # Initial time
    t_end = 2. # Final time 
    dt = 1e-1  # time-step size
    timesteps = np.arange(t_0, t_end + dt, dt)  # All time-steps
        
    ###########    Dimensions   ###########   
    n = dims[1]  # State dimension
    m = dims[0]  # Input dimension 
    l = dims[2]  # Output dimension
    ssm_dim = [n, m, l]  # Dimensions of the state-space model

    np.random.seed(4)
    A = -np.triu((np.random.rand(n, n)))
    n_c = int(n / 2) # Number of complex eval pairs
    for i in range(n_c):
        exec(f'r = -4.0 * np.random.rand(1)')
        exec(f'c = 3 + np.random.rand(1)')
        exec(f'eval_block = np.reshape(np.array([[r, c], [-c, r]]), (2, 2))')
        exec(f'A[2*i:2*i + 2, 2*i:2*i + 2] = eval_block')

    B = np.random.rand(n, m)
    C = np.random.rand(l, n)
    D = np.random.rand(l, m) 
    ssm = state_space_model(A, B, C, D)

    # Inputs: u(t) and u'(t)
    # Compute u'(t) based on the approximation of u: piecewise const, piecewise linear, or sampling the input function (if known)
    if u_interp == 'piecewise_constant': 
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency

    elif u_interp == 'piecewise_linear':
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency
        for t in range(0, len(timesteps) - 1):   
            du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])
            
    elif u_interp == 'sample':
        u = lambda t, i: np.sin(t @ i)
        du_dt = lambda t, i: np.multiply(np.cos(t @ i), i)
        
    else:
        raise ValueError('Check u_interp! ')
    
    inputs = [u, du_dt]
    
    clustering_alg = SpectralClustering(n_clusters=n_clusters,  
                    n_neighbors=5, 
                    affinity='nearest_neighbors',
                    gamma=1.0 )
    forward_pass_params = forward_pass_parameters(outer_loop=outer_loop, 
                    ssm_input_interpolation=u_interp,
                    solver_routine ='solve_ivp',  
                    ode_solver_params = ode_solver_parameters(atol=atol, rtol=rtol, \
                    ode_solver='DOP853'))
    return (timesteps, inputs, ssm, \
        clustering_alg, forward_pass_params, outer_loop, u_interp, \
            rtol, atol)


def params_real_complex_blocks(n_clusters, outer_loop, u_interp, dims, rtol=1e-10, atol=1e-10):
    ###########    Time   ###########   
    t_0 = 0.   # Initial time
    t_end = 2. # Final time 
    dt = 1e-1  # time-step size
    timesteps = np.arange(t_0, t_end + dt, dt)  # All time-steps
        
    ###########    Dimensions   ###########   
    n = dims[1]  # State dimension
    m = dims[0]  # Input dimension 
    l = dims[2]  # Output dimension
    ssm_dim = [n, m, l]  # Dimensions of the state-space model

    np.random.seed(4)
    s = [2.0] # Radii of the two circles

    # Consruct real and imaginary parts of the eigenvalues in arbitrary order  
    for i in range(len(s)): 
        c1_ev1_r = -1./np.sqrt(2) * s[i]
        c1_ev1_c = 1./np.sqrt(2) * s[i]
        
        c1_ev2_r = 1./np.sqrt(2) * s[i]
        c1_ev2_c = 1./np.sqrt(2) * s[i]
        
        c1_ev3_r = 0.2 * s[i]
        c1_ev3_c = 1. * s[i]
        
        c1_ev6_r = -0.2 * s[i]
        c1_ev6_c = 1. * s[i]
        
        c1_ev4 = -1. * s[i]
        c1_ev5 = 1. * s[i]
        
        
    A_11 = np.array([
            [c1_ev1_r,-c1_ev1_c, -0.8, -0.9, 0.5, 0.6,0.7, -0.3,0.7, -0.3],
            [c1_ev1_c, c1_ev1_r, -0.4, 0.3, -0.2, 0.1, 0.3, -0.24,0.7, -0.3],
            [0.   ,0.      , c1_ev4, 0.1, 0.2, -0.3, -0.2, -0.1,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_r,-c1_ev2_c, -0.5, -0.3, -0.9,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_c, c1_ev2_r, -0.9, -0.9, 0.9,0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     ,  c1_ev5, 0.7, 0.6,0.7, -0.3], 
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_r,-c1_ev3_c,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_c, c1_ev3_r,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0.,    0.,   0.,   c1_ev6_r, -c1_ev6_c],
            [0.   ,0.      , 0., 0.    ,0.     , 0., 0.,       0.,   c1_ev6_c,  c1_ev6_r],
            ]); 

    A = A_11

    B = np.random.rand(n, m)
    C = np.random.rand(l, n)
    D = np.random.rand(l, m) 
    ssm = state_space_model(A, B, C, D)

    # Inputs: u(t) and u'(t)
    # Compute u'(t) based on the approximation of u: piecewise const, piecewise linear, or sampling the input function (if known)
    if u_interp == 'piecewise_constant': 
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency

    elif u_interp == 'piecewise_linear':
        u = np.zeros((len(timesteps), m))
        du_dt = np.zeros((len(timesteps), m))
        for i in range(m):
            u[:, i] = np.sin(i * timesteps) # Each input signal with a different frequency
        for t in range(0, len(timesteps) - 1):   
            du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])
            
    elif u_interp == 'sample':
        u = lambda t, i: np.sin(t @ i)
        du_dt = lambda t, i: np.multiply(np.cos(t @ i), i)
        
    else:
        raise ValueError('Check u_interp! ')
    
    inputs = [u, du_dt]
    
    clustering_alg = SpectralClustering(n_clusters=n_clusters,  
                    n_neighbors=5, 
                    affinity='nearest_neighbors',
                    gamma=1.0 )
    forward_pass_params = forward_pass_parameters(outer_loop=outer_loop, 
                    ssm_input_interpolation=u_interp,
                    solver_routine ='solve_ivp',  
                    ode_solver_params = ode_solver_parameters(atol=atol, rtol=rtol, \
                    ode_solver='DOP853'))
    return (timesteps, inputs, ssm, \
        clustering_alg, forward_pass_params, outer_loop, u_interp, \
            rtol, atol)
    
  