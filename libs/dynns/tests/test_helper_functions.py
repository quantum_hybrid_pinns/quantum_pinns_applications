import numpy as np
import scipy.linalg as LA
from dynamic_neural_networks.transformations.helper_functions import *


def test_block_diagonalization_pipeline():
    ######################################################################################
    ### Example 1: State matrix with real, distinct (well-seperated) eigenvalues ###
    ######################################################################################
    n = 3
    A = np.zeros((n, n))
    np.fill_diagonal(A, - (np.arange(n) + 1.)) #
    
    # Regroup a into blocks of appropriate dimensions
    block_dims = [3, 3]
    block_row_sizes = [1, 1, 1]
    block_col_sizes = [1, 1, 1]
    
    block_sizes_1 = [block_dims, block_row_sizes, block_col_sizes]
    # QR decomposition
    R, Q = LA.schur(A, output='real')
    
    # Compute eigenvalues of R
    evals, evecs = LA.eig(R)
    
    # Check dimensions before block-diagonalization using Bartels-Stewart algorithm
    dim_check_block_diagonalization(Q, R, block_dims, block_row_sizes, block_col_sizes)

    # Perform block-diagonalization using Bartels-Stewart algorithm
    Z = block_diagonalization(Q, R, block_sizes_1)
    
    # Compute the resulting block diagonal matrix
    T = np.matmul(np.matmul(np.linalg.inv(Z), A), Z)
    
    np.testing.assert_array_equal(R, np.array(([-1., 0., 0.], [0., -2., 0.], [0., 0., -3.] )))
    np.testing.assert_array_equal(Z, np.identity(n))
    np.testing.assert_array_equal(T, np.array(([-1., 0., 0.], [0., -2., 0.], [0., 0., -3.] )))
    
    ######################################################################################
    ### Example 2: 4*4 State matrix with 2 well-seperated complex eigenvalues pairs ###
    ######################################################################################
    A = np.array([[-5., 2., 2., 3.], [-2., -5, 4., 5.], [0., 0., -3., 4.], [0., 0., -4., -3]]); 
    block_dims = [2, 2]
    block_row_sizes = [2, 2]
    block_col_sizes = [2, 2]
    
    block_sizes_2 = [block_dims, block_row_sizes, block_col_sizes]
    # QR decomposition
    R, Q = LA.schur(A, output='real')
    
    # Compute eigenvalues of R
    evals, evecs = LA.eig(R)
    
    # Check dimensions before block-diagonalization using Bartels-Stewart algorithm
    dim_check_block_diagonalization(Q, R, block_dims, block_row_sizes, block_col_sizes)

    # Perform block-diagonalization using Bartels-Stewart algorithm
    Z = block_diagonalization(Q, R, block_sizes_2)
    
    # Compute the resulting block diagonal matrix
    T = np.matmul(np.matmul(np.linalg.inv(Z), A), Z)
    
    #np.testing.assert_array_equal(R, np.array([[-5., 2., 0., 0.], [-2., -5, 0., 0.], [0., 0., -3., 4.], [0., 0., -4., -3]]))
    np.testing.assert_array_almost_equal(T, np.array([[-5., 2., 0., 0.], [-2., -5, 0., 0.], [0., 0., -3., 4.], [0., 0., -4., -3]]))

    ######################################################################################
    ### Example 3: 3*3 State matrix with complex and real well-seperated eigenvalues ###
    ######################################################################################
    A = np.array([[-5.,2., 3.], [0., -3., 3.], [0., -3., -3.]]); 

    # Regroup a into blocks of appropriate dimensions
    block_dims = [2, 2]
    block_row_sizes = [1, 2]
    block_col_sizes = [1, 2]
    block_sizes_3 = [block_dims, block_row_sizes, block_col_sizes]

    # QR decomposition
    R, Q = LA.schur(A, output='real')
    
    # Compute eigenvalues of R
    evals, evecs = LA.eig(R)
    
    # Check dimensions before block-diagonalization using Bartels-Stewart algorithm
    dim_check_block_diagonalization(Q, R, block_dims, block_row_sizes, block_col_sizes)

    # Perform block-diagonalization using Bartels-Stewart algorithm
    Z = block_diagonalization(Q, R, block_sizes_3)
    
    # Compute the resulting block diagonal matrix
    T = np.matmul(np.matmul(np.linalg.inv(Z), A), Z)
    
    np.testing.assert_array_equal(R, np.array(([-5., 2., 3.], [0., -3., 3.], [0., -3., -3.] )))
    np.testing.assert_array_equal(T, np.array(([-5., 0., 0.], [0., -3., 3.], [0., -3., -3.] )))
