import scipy.linalg as LA
import numpy as np
from dynamic_neural_networks.transformations.sorted_real_schur_forms import *
import pytest


def test_sorted_real_achur_forms():
    # Case of real-repeated eigenvalues
    s = 2.0 # Radius of the circle on which the eigenvalues lie

    # Set up a matrix A with real and complex eigenvalues ordered randomly on the diagonal
    c1_ev1_r = -1./np.sqrt(2) * s  
    c1_ev1_c = 1./np.sqrt(2) * s  
    
    c1_ev2_r = 1./np.sqrt(2) * s 
    c1_ev2_c = 1./np.sqrt(2) * s

    c1_ev3_r = 0.2 * s 
    c1_ev3_c = 1. * s

    c1_ev6_r = -0.2 * s 
    c1_ev6_c = 1. * s

    c1_ev4 = -1. * s
    c1_ev5 = 1. * s
    
    A = np.array([
            [c1_ev1_r,-c1_ev1_c, -0.8, -0.9, 0.5, 0.6,0.7, -0.3,0.7, -0.3],
            [c1_ev1_c, c1_ev1_r, -0.4, 0.3, -0.2, 0.1, 0.3, -0.24,0.7, -0.3],
            [0.   ,0.      , c1_ev4, 0.1, 0.2, -0.3, -0.2, -0.1,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_r,-c1_ev2_c, -0.5, -0.3, -0.9,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_c, c1_ev2_r, -0.9, -0.9, 0.9,0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     ,  c1_ev5, 0.7, 0.6,0.7, -0.3], 
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_r,-c1_ev3_c,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_c, c1_ev3_r,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0.,    0.,   0.,   c1_ev6_r, -c1_ev6_c],
            [0.   ,0.      , 0., 0.    ,0.     , 0., 0.,       0.,   c1_ev6_c,  c1_ev6_r],
            ]); 

    
    R, Q = LA.schur(A, output='real')
    spec_cl = SpectralClustering(
                        n_clusters=2,  
                        n_neighbors=5, 
                        affinity='rbf',
                        gamma=1.0)
    
    eps = np.finfo(R.dtype).eps
    r = np.count_nonzero(np.abs(np.diag(R, -1)) > 100*eps)
    Q, R, block_sizes, n_r, n_c, ap = \
        sort_real_schur(Q, R, spec_cl,show_clustering=False,
                        b=0, inplace=True)  # Q orthonormal
    assert np.allclose(np.dot(A, Q), np.dot(Q, R))  # check that still a decomposition of the original matrix
    assert np.allclose(np.dot(Q, Q.T), np.eye(A.shape[0]))  # Q orthonormal
    assert np.all(np.tril(R, -2) == 0)  # R triangular
    assert r == np.count_nonzero(np.abs(np.diag(R, -1)) > 100 * eps)  # number of blocks in R is preserved

    
    # check that eigenvalues are sorted
    T, Z = LA.rsf2csf(R, Q)
    ev_sorted = np.diag(T)
    
    # The sorting depends on which cluster is counted first
    expected_sorting_1 = [-2. + 0.j, -1.41421356 + 1.41421356j, -1.41421356 - 1.41421356j,\
                -0.4 + 2.j,-0.4 - 2.j, 2. + 0.j, 1.41421356 + 1.41421356j, \
                1.41421356-1.41421356j, 0.4 + 2.j, 0.4 - 2.j]
    
    np.testing.assert_array_almost_equal(ev_sorted, expected_sorting_1, decimal=3)


"""
test_cases = [
    (e_c1, e1),
    (e_c2, e2),
    (e_c3, e3)
]
@pytest.mark.parametrize("evals_c,evals", test_cases)
def test_perform_sorting(evals_c,evals):
    pass
"""