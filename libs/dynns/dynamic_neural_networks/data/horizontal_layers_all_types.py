# All imports
import numpy as np
from scipy.stats import special_ortho_group
import scipy.linalg as LA
import math

# Set a random seed
np.random.seed(seed=1)

def get_ssm(d_in, d_state, d_out):
    # Total number of real and complex eigenvalues
    n_r = 44
    n_c = 45
    
    # Get eigenvalues
    x_coords, y_coords = set_eigenvalues()

    # Set real and complex pairs of eigenvalues as 1 * 1 and 2 * 2 diagonal blocks of the state matrix
    diag_blocks = []
    for i in range(n_r + n_c):
        if i < n_c:
            #exec(f'a = np.array([(x_coords[{i}],y_coords[{i}]),(-y_coords[{i}],x_coords[{i}])])')
            a = np.array([(x_coords[i],y_coords[i]),(-y_coords[i],x_coords[i])])
        elif (i >= n_c and i < n_r + n_c):
            #exec(f'a = np.array(x_coords[{i}])')
            a = np.array(x_coords[i])
        diag_blocks.append(a)

    # Initialize state matrix A
    k = -0.5
    A = np.triu(k * np.random.rand(d_state, d_state))

    # Construct state matrix A with appropriate eigenvalues
    j = 0
    for i in range(n_r + n_c):
        if i < n_c:
            exec(f'A[{2*i}:{2*(i+1)}, {2*i}:{2*(i+1)}] = diag_blocks[i]')
            j += 2
        else:
            exec(f'A[{j}, {j}] = diag_blocks[i]')
            j += 1
    
    # Apply a similarity transfrom which simply rotates the eigenvalues
    # This transforms the block-upper traingular state matrix to dense 
    R = special_ortho_group.rvs(d_state)
    A = R @ A @ R.T
    
    # Set up the input matrix B
    B = R @ np.random.rand(d_state, d_in)
    
    # Set up the output matrix C
    C = np.random.rand(d_out, d_state) @ R.T
    
    # Set up the feedforward matrix D
    D = -np.random.rand(d_out, d_in)
    
    return A, B, C, D

# A function to sample eigenvalues in a cluster
def sample_points_in_circle(r, c, num_points):
    points = []
    for _ in range(num_points):
        # Generate random angle in radians
        theta = np.random.uniform(0, 2 * math.pi)
        radius = np.random.uniform(0, r)
        
        # Convert polar coordinates to Cartesian coordinates
        x = c[0] + radius * math.cos(theta)
        y = c[1] + radius * math.sin(theta)
        
        # Add to the list of points
        points.append((x, y))
    return points

def set_eigenvalues():
    """
    Generate eigenvalues for example 3

    Returns:
        Arrays: _description_
    """
    # Create eigenvalues 
    # Blob - 1 : Complex Eigenvalues
    r_1 = 0.8  # adjust as needed
    n_points_1 = 10  # adjust as needed
    c_1 = [-20,5]
    random_points_1 = sample_points_in_circle(r_1, c_1, n_points_1)
    x_coords_1, y_coords_1 = zip(*random_points_1)

    # Blob - 1.1 : Complex Eigenvalues
    r_11 = 0.9  # adjust as needed
    n_points_11 = 15  # adjust as needed
    c_11 = [-15,3]
    random_points_11 = sample_points_in_circle(r_11, c_11, n_points_11)
    x_coords_11, y_coords_11 = zip(*random_points_11)

    # Blob - 2 : Real and complex eigenvalues
    r_2 = 1.1  # adjust as needed
    n_points_2_c = 4  # adjust as needed
    n_points_2_r = 14  # adjust as needed
    c_2 = [-30,0]
    random_points_2 = sample_points_in_circle(r_2, c_2, n_points_2_c)
    x_coords_complex_2, y_coords_complex_2 = zip(*random_points_2)
    x_coords_real_2, y_coords_real_2 = (c_2[0] + np.random.uniform(-r_2, r_2, n_points_2_r), np.zeros(n_points_2_r))
    x_coords_2 = np.concatenate((x_coords_complex_2, x_coords_real_2))
    y_coords_2 = np.concatenate((y_coords_complex_2, y_coords_real_2))

    # Blob - 2.1 : Real and complex eigenvalues
    r_21 = 0.9  # adjust as needed
    n_points_2_c1 = 16  # adjust as needed
    n_points_2_r1 = 8  # adjust as needed
    c_21 = [-25,0]
    random_points_21 = sample_points_in_circle(r_21, c_21, n_points_2_c1)
    x_coords_complex_21, y_coords_complex_21 = zip(*random_points_21)
    x_coords_real_21, y_coords_real_21 = (c_21[0] + np.random.uniform(-r_21, r_21, n_points_2_r1), np.zeros(n_points_2_r1))

    # Blob - 3 : Only Real eigenvalues
    c_3 = [-10]
    r_3 = 1.  # adjust as needed
    n_points_3 = 6  # adjust as needed
    x_coords_3 = c_3[0] + np.random.uniform(-r_3, r_3, n_points_3)

    # Blob - 3.1 : Only Real eigenvalues
    c_31 = [-5] # Center of the cluster
    r_31 = 1.2  # Radius of the cluster
    n_points_31 = 16  # Number of points
    
    # Sample eigenvalues
    x_coords_31 = c_31[0] + np.random.uniform(-r_31, r_31, n_points_31) 
    
    # Combine all real and imaginary parts of the eigenvalues as x_coords and y_coords respectively
    x_coords = np.concatenate((x_coords_1, x_coords_11, x_coords_complex_2, x_coords_complex_21, x_coords_real_2, x_coords_real_21, x_coords_3, x_coords_31))
    y_coords = np.concatenate((y_coords_1, y_coords_11, y_coords_complex_2, y_coords_complex_21, y_coords_real_2, y_coords_real_21))
    return x_coords, y_coords

