# All imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors

# Set a random seed
np.random.seed(seed=1)

def get_ssm(ssm_dim, nx, ny, dx, diffusivity, drift_vel):
    """
    Returns an LTI system matrices (A, B, C, D) corresponding to a 2-D 
    convection-diffusion equation discretized in space on a uniform grid. 

    Args:
        ssm_dim (List of matrices): Input, state and output dimensions of the LTI system
        nx (int): Number of grid points in the first dimension
        ny (int): Number of grid points in the second dimension
        dx (float): Spacing between consecutive grid points on the uniform grid
        diffusivity (float): Diffusivity constant
        drift_vel (list): drift velocities in each of the two dimensions

    Returns:
        List of matrices: A, B, C, D
    """

    # Fetch dimensions
    d_in, d_state, d_out = ssm_dim
    v_x, v_y = drift_vel
    
    # State matrix 
    diag_mat = -4 * np.identity(nx) + np.eye(nx, k=1) + np.eye(nx, k=-1)\
                + np.eye(nx, k=nx-1) + np.eye(nx, k=-(nx-1))
    a = np.eye(nx, k=1)
    a[0][1] = 0
    b = np.eye(nx, k=-1)
    b[nx-1][nx-2] = 0
    non_dig_mat =  a + b
    diag_convection =  v_x * (np.eye(nx, k=1) + np.eye(nx, k=nx-1)- np.eye(nx, k=-1) - np.eye(nx, k=-(nx-1)))/(2.*dx)
    p_diag_blocks = np.identity(ny) 
    p_diag_blocks[0][0] = 0
    p_diag_blocks[ny-1][ny-1] = 0
    A = np.kron(p_diag_blocks, diag_mat) + np.kron(non_dig_mat, np.identity(ny))
    A = diffusivity/(dx * dx) * A
    A += np.kron(p_diag_blocks,diag_convection)
    
    # Other matrices of the state-space model
    B = np.identity(d_state)              # Input Matrix
    C = np.identity(d_state)              # Output matrix
    D = np.zeros((d_out, d_in))           # Feedthrough (or feedforward) matrix
    return A, B, C, D

   
def get_input(l_x, l_y, nx, ny, timesteps, d_in, show_input=True):
    """
    Returns the input which is the heat injected into the system via the 
    source term.

    Args:
        l_x (float): Domain length in the first dimension
        l_y (float): Domain length in the second dimension
        nx (int): Number of grid points in the first dimension
        ny (int): Number of grid points in the second dimension
        timesteps (array): Time points
        d_in (int): input dimension
        show_input (bool, optional): Plots the input at time t = 0.02. Defaults to True.

    Returns:
        list: Returns [u, du_dt] (Ignore: dt_dt for this example as it is never used)
    """
    # Input to a dynamic neural network and the numerical solvers
    x = np.linspace(0, l_x, nx) 
    y = np.linspace(0, l_y, ny)
    x_grid, y_grid = np.meshgrid(x, y)
    cmap = cm.jet
    np.random.seed(4) # Set randome seed
    u = np.zeros((len(timesteps), d_in))
    du_dt = np.zeros((len(timesteps), d_in))
    gaussian_input_pulse = 100. * np.exp(-0.8 * ((x_grid-l_x/2)**2 + (y_grid-l_y/2)**2)) #sqrt(x**2 + y**2)
    u[2, :] = gaussian_input_pulse.reshape(-1)

    # du_dt: Not required for real eigenvalues (It's only needed for complex evals)
    for t in range(0, len(timesteps) - 1):   
        du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])


    if show_input:
        plt.figure(figsize=(2,2))
        plt.plot()
        # Set title, labels for each subplot
        fontsize = 10
        plt.title('$u(t=0.02)$',fontsize=fontsize)
        contour1 = plt.contourf(x_grid, y_grid, u[2].reshape(nx, ny), cmap=cmap, levels=200)#,linewidth=0, antialiased=False, 
        plt.savefig('input.pdf')
        norm= colors.Normalize(vmin=contour1.cvalues.min(), vmax=contour1.cvalues.max())
        sm = plt.cm.ScalarMappable(norm=norm, cmap=contour1.cmap)
        sm.set_array([])
        plt.colorbar(sm, location='bottom',aspect=20)

    return [u, du_dt]

