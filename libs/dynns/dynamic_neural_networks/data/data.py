import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from abc import ABC, abstractmethod
import control


# TODO: Modularize the data generation process
# Second-order ODE data generator
def generate_data_ODE(c_2, c_1, c_0):
    # Make coefficient of y term ine c_0 = 1
    c_2 /= c_0
    c_1 /= c_0


def set_initial_conditions(order:int, initial_cond: np.array):
    assert np.size(initial_cond) == order - 1, "Order or # initial conditions not consistent"


# Example: my'' + by' + ky = u , ICs: y'(0) = 0, y(0) = 0, m = 1
m = 1.0 # mass
b = 4.0 # damping coefficient
k = 4.0 # spring-constant

# Divide by k --> same form as the ODE for the DNN
m /= k
b /= k

# Initial conditions: y(0), y'(0)
y_0 = 0.0
y_prime_0 = 0.0
y0 = [y_0 , y_prime_0]


t_0 = 0.0
t_end = 10. #25.0 
dt = 0.005 #0.01

timesteps = np.arange(t_0,t_end + dt,dt)

#u_hat = np.ones(len(timesteps))
u_hat = np.cos(2*timesteps)
u_hat /= k

#u = t # Linear response
#u_hat = np.cos(2*timesteps)/k # Cosine 

def y_derivatives(y,t):
    u_hat = np.cos(2*t)/k    #u = 1.0/k
    #u_hat = 1. / k
    return (y[1], (-b * y[1] - y[0] + u_hat)/m) # y', y''

y_sol = odeint(y_derivatives, y0, timesteps, mxordn = 2, mxords = 2)
y1 = y_sol[:,0]
z1 = y_sol[:,1]
 
# Explicit numerical solution

class integration_schemes(ABC):
    @abstractmethod
    def set_scheme():
        pass


class backward_Euler(integration_schemes):
    def __init__(self) -> None:
        super().__init__()
        
    def set_scheme():
        i_1 = 1.0
        i_2 = 0.0
        return i_1, i_2


class trapezoidal(integration_schemes):
    # Implicit Scheme: Second order Adams-Moulton (AM2) method
    def __init__(self) -> None:
        super().__init__()
        
    def set_scheme():
        i_1 = 0.5
        i_2 = 0.5
        return i_1, i_2


class Adams_Bashforth_2(integration_schemes):
    # Explicit method, conditionally stable
    def __init__(self) -> None:
        super().__init__()
        
    def set_scheme():
        i_1 = 1.5
        i_2 = -0.5
        return i_1, i_2


# Choose numerical integration scheme
a_1, a_2 = Adams_Bashforth_2.set_scheme()
#a_1, a_2 = trapezoidal.set_scheme()
#a_1, a_2 = backward_Euler.set_scheme()

    
tau_2 = m # Already normalized
tau_1 = b # Already normalized
y_old, z_old =  y0 
y_explicit = []
z_explicit = []
h = dt
h_inv = 1. / h
f_old = u_hat[0]

print(range(len(timesteps)))
for i in range(len(timesteps)):
    # Set the forcing term 
    f_new = u_hat[i]
    
    # Store solution and it's first order time-derivative
    y_explicit.append(y_old)
    z_explicit.append(z_old)
    
    # Compute an intermediate term to re-use
    term_1 = a_1 * tau_1 * h_inv + tau_2 * h_inv * h_inv
    
    # Compute new solution: y_new, first order time-derivative: z_new
    y_new = (a_1 * a_1 * f_new + a_1 * a_2 * f_old + (-a_1 * a_2 + term_1) * y_old 
             + (a_1 + a_2) * h_inv * tau_2 * z_old)/(a_1 * a_1 + term_1)
    z_new = (y_new - y_old) / (h * a_1) - (a_2 / a_1) * z_old
    
    # Swap values for next iteration
    y_old = y_new
    z_old = z_new
    f_old = f_new



A = np.array([[0.,1.], [-1./m, -b/(m)]]); # A_p = np.array([[0,1], [-5, -4.0001]]) # Mass-spring-damper case
B = np.array([[0.0], [1.0/m]])
C = np.identity(2)
D = np.array([[0.0], [0.0]])

ssmodel = signal.lti(A, B, C, D)# control.ss(A, B, C, D)

# Step response for the system
t, y_signal, x_signal = signal.lsim(ssmodel, u_hat, timesteps)
y_signal_1 = y_signal[:,0]
y_signal_2 = y_signal[:,1]


m_new = 1.0 # mass
b_new = 4.0 # damping coefficient
k_new = 4.0 # spring-constant
u_hat = np.cos(2*timesteps)
A_1 = np.array([[0.,1.], [-k_new/m_new, -b_new/(m_new)]]); # A_p = np.array([[0,1], [-5, -4.0001]]) # Mass-spring-damper case
B_1 = np.array([[0.0], [1.0/m_new]])
C_1 = np.identity(2)
D_1 = np.array([[0.0], [0.0]])

ssmodel = signal.lti(A_1, B_1, C_1, D_1)# control.ss(A, B, C, D)

# Step response for the system
t_new, y_signal_new, x_signal_new = signal.lsim(ssmodel, u_hat, timesteps)
y_signal_new_1 = y_signal_new[:,0]
y_signal_new_2 = y_signal_new[:,1]


plt.plot(t,y_signal_1,'b', linestyle='--', dashes=(5, 3), linewidth=2,label='Scipy signal', 
         markersize=12)
plt.plot(timesteps,y1,'g', linestyle='--', dashes=(5, 3), linewidth=2,label='Scipy odeint', 
         markersize=12)
plt.plot(timesteps,y_explicit,'r', linestyle='--', dashes=(5, 3), linewidth=2,label='Manual implementation', 
         markersize=12)
plt.plot(t_new,y_signal_new_1,'m', linestyle='-.', dashes=(5, 3), linewidth=2,label='New state space model', 
         markersize=12)

'''

R, Q = unitary_triangulation(A).apply_transformation()
Z = block_diagonalization_22(R, Q, eps).apply_transformation()
Z_inv = inverse(Z).apply_transformation()
T = transformed_state_matrix(Z_inv, A, Z).apply_transformation() 
A_new = T
B_new = np.matmul(Z_inv, B)
C_new = np.matmul(C, Z)
D_new = D

'''
#plt.plot(timesteps,z1,'g:',linewidth=2,label='y1: ODE Integrator', markersize=12)
#plt.plot(timesteps,z_explicit,'r-',linewidth=2,label='y1: Implicit Euler manual solution', markersize=12)
#plt.plot(t,y2,'r:',linewidth=2,label='y2: ODE Integrator')
plt.xlabel('Time')
plt.ylabel('Response (y)')
plt.legend(loc='best')
plt.show()


'''
# Important stuff here
# First-order ODE data generator 
# Simulate tau_1 * y'+ y = K*u
K = 1.0
tau_1 = 2.0

# State Space : (1) x'(t) = A_c x(t) + B_c u(t) , (2) y(t) = C_c x(t) + D_c u(t)
A_c = -1.0/tau_1
B_c = K/tau_1
C_c = 1.0
D_c = 0.0
sys2 = signal.StateSpace(A_c,B_c,C_c,D_c)
#print(sys2_c)
t_0 = 0.0
t_end = 5.0
dt = 0.05
t2 = np.arange(t_0,t_end + dt,dt) # Specify time-steps
t2,y2 = signal.step(sys2, T = t2)

# ODE Integrator
def model3(y,t):
    u = 1   # Step-input
    return (-y + K * u)/tau_1
t3 = np.arange(t_0,t_end + dt,dt)
y3 = odeint(model3,0,t3)
#print(t3, y3)
plt.figure(1)
plt.plot(t2,y2,'g:',linewidth=2,label='State Space')
plt.plot(t3,y3,'r-',linewidth=1,label='ODE Integrator')
plt.xlabel('Time')
plt.ylabel('Response (y)')
plt.legend(loc='best')
plt.show()

'''



'''
# tau_2 * dy2/dt2 + 2*zeta*tau_2*dy/dt + y = Kp*u
Kp = 2.0    # gain
tau_2 = 1.0   # time constant
zeta = 0.25 # damping factor
theta = 0.0 # no time delay
du = 1.0    # change in u

tau_1 = 2 * np.sqrt(tau_2) *

# (1) Transfer Function
num = [Kp]
den = [tau_2**2,2*zeta*tau_2,1]
sys1 = signal.TransferFunction(num,den)
t1,y1 = signal.step(sys1)

# (2) State Space
A = [[0.0,1.0],[-1.0/tau_2**2,-2.0*zeta/tau_2]]
B = [[0.0],[Kp/tau_2**2]]
C = [1.0,0.0]
D = 0.0
sys2 = signal.StateSpace(A,B,C,D)
t2,y2 = signal.step(sys2)

# (3) ODE Integrator
def model3(x,t):
    y = x[0]
    dydt = x[1]
    dy2dt2 = (-2.0*zeta*tau_2*dydt - y + Kp*du)/tau_2**2
    return [dydt,dy2dt2]
t3 = np.linspace(0,25,100)
x3 = odeint(model3,[0,0],t3)
y3 = x3[:,0]

plt.figure(2)
plt.plot(t1,y1*du,'b--',linewidth=3,label='Transfer Fcn')
plt.plot(t2,y2*du,'g:',linewidth=2,label='State Space')
plt.plot(t3,y3,'r-',linewidth=1,label='ODE Integrator')
y_ss = Kp * du
plt.plot([0,max(t1)],[y_ss,y_ss],'k:')
plt.xlim([0,max(t1)])
plt.xlabel('Time')
plt.ylabel('Response (y)')
plt.legend(loc='best')
plt.show()
'''