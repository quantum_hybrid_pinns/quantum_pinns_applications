# All imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors

# Set a random seed
np.random.seed(seed=1)

def get_ssm(ssm_dim, nx, ny, dx, diffusivity):
    """
    Returns an LTI system matrices (A, B, C, D) corresponding to 2-D heat equation
    discretized in space on uniform grid

    Args:
        ssm_dim (List of matrices): Input, state and output dimensions of the LTI system
        nx (int): Number of grid points in the first dimension
        ny (int): Number of grid points in the second dimension
        dx (float): Spacing between consecutive grid points on the uniform grid
        diffusivity (float): Diffusivity constant

    Returns:
        List of matrices: A, B, C, D
    """

    # Fetch dimensions
    d_in, d_state, d_out = ssm_dim

    # State Matrix
    diag_mat = -4 * np.identity(nx) + np.eye(nx, k=1) + np.eye(nx, k=-1) \
                + np.eye(nx, k=nx-1) + np.eye(nx, k=-(nx-1))
    non_dig_mat =  np.eye(nx, k=1) + np.eye(nx, k=-1) \
                + np.eye(nx, k=nx-1) + np.eye(nx, k=-(nx-1))
    A = np.kron(np.identity(ny), diag_mat) + np.kron(non_dig_mat, np.identity(ny))
    A = diffusivity/(dx * dx) * A
    
    # Other matrices of the state-space model
    B = np.identity(d_state)        # Input Matrix
    C = np.identity(d_state)        # Output matrix
    D = np.zeros((d_out, d_in))     # Feedthrough (or feedforward) matrix
    return A, B, C, D


def get_input(l, nx, ny, timesteps, d_in, show_input=True):
    """
    Returns the input which is the heat injected into the system via the 
    source term.

    Args:
        l (float): Domain length in each of the two dimensions
        nx (int): Number of grid points in the first dimension
        ny (int): Number of grid points in the second dimension
        timesteps (array): Time points
        d_in (int): input dimension
        show_input (bool, optional): Plots the input at time t = 0.02. Defaults to True.

    Returns:
        list: Returns [u, du_dt] (Ignore: dt_dt for this example as it is never used)
    """
    # Input to a dynamic neural network and the numerical solvers
    x = np.linspace(0, l, nx) 
    y = np.linspace(0, l, ny)
    x_grid, y_grid = np.meshgrid(x, y)

    u = np.zeros((len(timesteps), d_in))
    du_dt = np.zeros((len(timesteps), d_in))
    gaussian_input_pulse = 100. * np.exp(-0.8 * ((x_grid-l/2)**2 + (y_grid-l/2)**2)) #sqrt(x**2 + y**2)
    u[2, :] = gaussian_input_pulse.reshape(-1)
    # Plot the input signal at time t = 0.02
    if show_input:
        plt.figure(figsize=(2,2))
        plt.plot()
        fontsize = 10
        plt.title('$u(t=0.02)$',fontsize=fontsize)
        contour1 = plt.contourf(x_grid, y_grid, u[2].reshape(nx, ny), cmap=cm.jet, levels=200)#,linewidth=0, antialiased=False, 
        norm= colors.Normalize(vmin=contour1.cvalues.min(), vmax=contour1.cvalues.max())
        sm = plt.cm.ScalarMappable(norm=norm, cmap=contour1.cmap)
        sm.set_array([])
        #plt.colorbar(sm, location='bottom',aspect=20)


    # du_dt: Not required for real eigenvalues 
    # (It's only used when A has complex evals)
    for t in range(0, len(timesteps) - 1):   
        du_dt[t, :] = (u[t+1, :] - u[t, :])/(timesteps[t+1] - timesteps[t])

    return [u, du_dt]

