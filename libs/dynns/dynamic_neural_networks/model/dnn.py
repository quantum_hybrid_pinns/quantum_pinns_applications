import numpy as np
import scipy.linalg as LA
import matplotlib.ticker as ticker
from scipy.integrate import odeint, solve_ivp
import matplotlib.pyplot as plt
from scipy import signal
from libs.dynns.dynamic_neural_networks.model.state_space_model import *
from libs.dynns.dynamic_neural_networks.transformations.sorted_real_schur_forms import *
from libs.dynns.dynamic_neural_networks.transformations.helper_functions import *


class ode_solver_parameters:
    def __init__(self, atol=1e-10, rtol=1e-10, ode_solver='DOP853'):
        # Initialize the member variables 
        self.atol = atol
        self.rtol = rtol
        self.ode_solver = ode_solver
        
        # Sanity check: Ensure that the chosen ode solver is supported
        if ode_solver not in ['RK23', 'RK45', 'DOP853', 'Radau', 'BDF', 'LSODA']:
            raise ValueError("Choose appropriate solver!")


class forward_pass_parameters:
    def __init__(self, outer_loop='neurons', 
                 ssm_input_interpolation='piecewise_linear',
                 solver_routine ='solve_ivp',  
                 ode_solver_params = ode_solver_parameters(atol=1e-10, rtol=1e-10, \
                 ode_solver='DOP853')):
        # Initialize the member variables 
        self.outer_loop = outer_loop
        self.ode_solver_params = ode_solver_params
        self.input_interpolation = ssm_input_interpolation
        self.solver_routine = solver_routine
        
        # Sanity check: Ensure that the chosen interpolation method is supported
        if ssm_input_interpolation not in ['piecewise_constant', 'piecewise_linear', \
            'sample']:
            raise ValueError("Choose appropriate input function interpolation method!")
        
        # Sanity check: Ensure that the chosen solver routine is supported
        if solver_routine not in ['solve_ivp', 'manual_implementation']:
            raise ValueError("Choose appropriate solver routine!")
        
        # Sanity check: Ensure that the chosen solver routine is supported
        if outer_loop not in ['neurons', 'timesteps']:
            raise ValueError("Choose appropriate outer loop!")

        
class dynamic_neural_network:
    def __init__(self, ssm: state_space_model):
        self.ssm = ssm
        self.dynn_blocks = []
        self.eigenvalues = None
        self.cond_num = None
        

    def simulate(self, forward_pass_params: forward_pass_parameters, \
        clustering_alg, timesteps, inputs, verbose, show_plots, init_cond=None): 
        
        # Transform state space model, construct dynn blocks, compute weights
        self.fit(clustering_alg, verbose, show_plots, init_cond)
        
        # Perform forward pass and compute output
        y_dynn = self.predict(inputs, timesteps, forward_pass_params)
        return y_dynn
    
    
    def plot_architectures(self, figname='example', 
        fig_size=(7.,2.5), counter=1, fontsize = 10, show_title=False):
        n_evals = 0
        n_neurons = 0 
        cmap='jet'
        #my_cmap = plt.cm.get_cmap('jet')
        #colors = my_cmap(y_pos)
        
        n_neurons_block = np.zeros(len(self.dynn_blocks))
        cluster_of_each_neuron = []
        cluster_of_each_eval = []
        block_types = []
        fig, axes = plt.subplots(1, 2, figsize=fig_size)
        fontsize = fontsize #10
        if show_title:
            fig.suptitle('Number of eigenvalue clusters = ' + str(counter))
        for i in range(len(self.dynn_blocks)):
            if self.dynn_blocks[i].block_type == 'complex': 
                n_eval_i = 2 * self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real':
                n_eval_i = self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real_complex':
                n_eval_i = self.dynn_blocks[i].k_r + \
                            2 * self.dynn_blocks[i].k_c
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            else:
                raise ValueError('Check the block type!')
            n_neurons_block[i] = self.dynn_blocks[i].d_A
            n_neurons += n_neurons_block[i]

            cluster_of_each_neuron.extend(i * np.ones(self.dynn_blocks[i].d_A, dtype=int))
            block_types.append(self.dynn_blocks[i].block_type)
                
        heat_map_A = axes[0].scatter(self.eigenvalues.real,\
            self.eigenvalues.imag, c=cluster_of_each_eval, cmap=cmap)#,s=marker_size
        axes[0].tick_params(axis='x', labelsize=fontsize)
        axes[0].tick_params(axis='y', labelsize=fontsize)
        axes[0].set_xlabel('Real', fontsize=fontsize)
        axes[0].set_ylabel('Imaginary', fontsize=fontsize)
        #axes[0].set_title.set_text('Eigenvalues',fontsize=fontsize)
        axes[0].set_title('Eigenvalues',fontsize=fontsize)
        #axes[0].set_xticks(Fontsize=fontsize)
        #axes[0].set_yticks(Fontsize=fontsize)
        #ax = plt.gca()
        #cb0 = fig1.colorbar(heat_map_A, ax=ax, location='right')
        #fig1.tight_layout()
        #plt.savefig(figname + '_evals.pdf')

        #fig2 = plt.figure(figsize=fig_size_2)
        y_pos = np.arange(len(self.dynn_blocks)) + 1
        my_cmap = plt.cm.get_cmap('jet')
        colors = my_cmap(y_pos)

        # Normalize values to 0-1
        norm = plt.Normalize(y_pos.min(), y_pos.max())

        # Apply the color map
        colors = my_cmap(norm(y_pos))
        axes[1].set_xlim(0, 11)  # Adjust the values as needed
        axes[1].set_ylim(0, 11)  # Adjust the values as needed
        if len(self.dynn_blocks) == 1:
            axes[1].barh(y_pos, n_neurons_block, align='center', color='#7AFF7D',
                        height=0.8)
        else:
            axes[1].barh(y_pos, n_neurons_block, align='center', color=colors,
                        height=0.8)
        #axes[1].barh(y_pos, n_neurons_block, align='center',
        #                     color=my_cmap(y_pos) ,height=0.8)
        #if len(y_pos) < 10:
        #    ticks = [1, 3, 5, 7, 9]
        #    plt.yticks(ticks, fontsize = fontsize)#, labels=np.arange(n_clusters)
        
        ax = plt.gca()
        axes[1].invert_yaxis()  # labels read top-to-bottom
        axes[1].set_ylabel('Horizontal layer', fontsize=fontsize)
        axes[1].set_xlabel('Depth (no. of neurons)', fontsize=fontsize)
        #axes[1].title.set_text('DyNN hidden layers',fontsize=fontsize)
        axes[1].set_title('DyNN hidden layers',fontsize=fontsize)
        #axes[1].set_xticks(Fontsize=fontsize)
        #plt.xticks(fontsize = fontsize)
        axes[1].tick_params(axis='x', labelsize=fontsize)
        axes[1].tick_params(axis='y', labelsize=fontsize)
        # Set x-axis ticks to integer values
        axes[1].yaxis.set_major_locator(plt.MaxNLocator(integer=True))
        axes[1].xaxis.set_major_locator(plt.MaxNLocator(integer=True))
        #axes[1].locator_params(axis='x', integer=True)
        
        # Set y-axis ticks to integer values
        #axes[1].locator_params(axis='y', integer=True)
        
        #plt.title('DyNN hidden layers', fontsize=fontsize)
        plt.gca()
        if len(self.dynn_blocks) == 1:
            cb1 = fig.colorbar(heat_map_A, ax=axes[1], location='right',
                    ticks=[1])
            # Set the locator for the colorbar ticks to Integer
        elif len(self.dynn_blocks) == 10:
            cb1 = fig.colorbar(heat_map_A, ax=axes[1], location='right',
                    ticks=[1, 3, 5, 7, 9])
        else:
            cb1 = fig.colorbar(heat_map_A, ax=axes[1], location='right')
            cb1.ax.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))
        
        cb1.ax.tick_params(labelsize=fontsize) 
        #fig.colorbar(sc, ax=axes[1])
        plt.subplots_adjust(top=0.5)
        fig.tight_layout()
        
        plt.savefig(figname + '_' + str(counter) +'_architecture.pdf', dpi=600)
        #plt.savefig(figname + '_architecture.pdf')
    
    def plot_architecture(self, figname='example', 
        fig_size_1=(2.5, 2), fig_size_2=(2.5,2), fontsize = 10, 
        counter=1, show_title=False, color_bar=True, sort_evals=False):
        plt.rcParams["text.usetex"] = False
        n_evals = 0
        n_neurons = 0 
        cmap='jet'
        n_neurons_block = np.zeros(len(self.dynn_blocks))
        cluster_of_each_neuron = []
        cluster_of_each_eval = []
        block_types = []
    
        for i in range(len(self.dynn_blocks)):
            if self.dynn_blocks[i].block_type == 'complex': 
                n_eval_i = 2 * self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real':
                n_eval_i = self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real_complex':
                n_eval_i = self.dynn_blocks[i].k_r + \
                            2 * self.dynn_blocks[i].k_c
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            else:
                raise ValueError('Check the block type!')
            n_neurons_block[i] = self.dynn_blocks[i].d_A
            n_neurons += n_neurons_block[i]

            cluster_of_each_neuron.extend(i * np.ones(self.dynn_blocks[i].d_A, dtype=int))
            block_types.append(self.dynn_blocks[i].block_type)
                
        fig1 = plt.figure(figsize=fig_size_1, layout='constrained')
                       
        # Set x-axis ticks to integer values
        plt.tick_params(axis='both', labelsize=fontsize)
        plt.locator_params(axis='x', integer=True)
        #axes[0].tick_params(axis='y', labelsize=fontsize)
        #axes[0].set_xlabel('Real', fontsize=fontsize)
        # Set y-axis ticks to integer values
        plt.locator_params(axis='y', integer=True)
        """
        if all(bt == 'real' for bt in block_types):
            heat_map_A = plt.scatter(np.arange(n_evals) + 1, \
                self.eigenvalues.real, c=cluster_of_each_eval, \
                    cmap=cmap)
            #axs0.set_ylim([0, 200])
            #plt.xscale('symlog')
            plt.xlabel('Neuron', fontsize=fontsize)
            plt.ylabel('Eigenvalue', fontsize=fontsize)
            #plt.title('Eigenvalues', fontsize=fontsize)
        else:
        """
        if sort_evals:
            x = self.eigenvalues.real
            y = self.eigenvalues.imag
            sorted_indices = np.argsort(abs(x))
            sorted_r = x[sorted_indices]
            sorted_c = y[sorted_indices]
            #print(sorted_indices)
            #print(sorted_r)
        
            if color_bar:
                heat_map_A = plt.scatter(sorted_r,\
                    sorted_c, c=np.arange(len(sorted_r))+1, cmap=cmap)#,s=marker_size
            else:
                heat_map_A = plt.scatter(sorted_r,\
                    sorted_c, color='r',
                    alpha=0.3, s=50, edgecolors='k')#,s=marker_size, marker = '.'
        else:  
            if color_bar:
                heat_map_A = plt.scatter(self.eigenvalues.real,\
                    self.eigenvalues.imag, c=cluster_of_each_eval, cmap=cmap)#,s=marker_size
            else:
                heat_map_A = plt.scatter(self.eigenvalues.real,\
                    self.eigenvalues.imag, color='r',
                    alpha=0.3, s=50, edgecolors='k')#,s=marker_size, marker = '.'
        plt.xlabel('Real', fontsize=fontsize)
        plt.ylabel('Imaginary', fontsize=fontsize)
            # plt.title('Eigenvalues', fontsize=fontsize)
        ax = plt.gca()
        #cb0 = fig1.colorbar(heat_map_A, ax=ax, location='right')
        #cb0.ax.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))

        plt.gca()
        if color_bar:
            if len(self.dynn_blocks) == 1:
                cb0 = fig1.colorbar(heat_map_A, ax=ax, location='right',
                        ticks=[1])
                # Set the locator for the colorbar ticks to Integer
            else:
                cb0 = fig1.colorbar(heat_map_A, ax=ax, location='right')
            #fig1.tight_layout()
            cb0.ax.tick_params(labelsize=fontsize) 
        plt.savefig(figname + '_evals.pdf')

        fig2 = plt.figure(figsize=fig_size_2, layout='constrained')
        y_pos = np.arange(len(self.dynn_blocks)) + 1
        my_cmap = plt.cm.get_cmap('jet')
        colors = my_cmap(y_pos)
        # Normalize values to 0-1
        norm = plt.Normalize(y_pos.min(), y_pos.max())

        # Apply the color map
        colors = my_cmap(norm(y_pos))
        if len(y_pos) == 1:
            plt.ylim(0, 2)  # Adjust the values as needed
            bar_chart = plt.barh(y_pos, n_neurons_block, align='center', color=colors,
                            height=1)
        else:
            bar_chart = plt.barh(y_pos, n_neurons_block, align='center', color=colors
                                     ,height=1)
            
        #plt.yticks(y_pos)#, labels=np.arange(n_clusters)
        ax = plt.gca()
        ax.invert_yaxis()  # labels read top-to-bottom
        plt.ylabel('Horizontal layer', fontsize=fontsize)
        plt.xlabel('Depth (no. of neurons)', fontsize=fontsize)
        plt.tick_params(axis='both', labelsize=fontsize)
        plt.locator_params(axis='both', integer=True)

        #plt.title('DyNN hidden layers', fontsize=fontsize)
        if color_bar:
            ax = plt.gca()
            cb1 = fig1.colorbar(heat_map_A, ax=ax, location='right')
            #fig2.tight_layout()
            cb1.ax.tick_params(labelsize=fontsize) 

        plt.savefig(figname + '_architecture.pdf')
    
    def plot_NFE(self, figname='example', fig_size=(2.5, 2), fontsize=10, 
                 x_label='Horizontal layer', y_label='Avg NFE', color_bar=True,
                 sort_evals=False):
        rhs_evals_cluster = np.zeros(len(self.dynn_blocks))
        for i in range(len(self.dynn_blocks)):
            # Gather number of neurons as n_neurons
            if i == 0:
                rhs_evals_neuron = self.dynn_blocks[i].rhs_evaluations
                
            if i > 0:
                rhs_evals_neuron = np.hstack((rhs_evals_neuron, self.dynn_blocks[i].rhs_evaluations))
            
            rhs_evals_cluster[i] = np.sum(self.dynn_blocks[i].rhs_evaluations)/self.dynn_blocks[i].d_A

        
        cluster = np.arange(len(self.dynn_blocks)) + 1
        fig3 = plt.figure(figsize=fig_size)
        if sort_evals:
            x = self.eigenvalues.real
            sorted_indices = np.argsort(abs(x))
            c_eig = x[sorted_indices]
            #print(sorted_indices)
            if color_bar:
                heat_map_C = plt.scatter(cluster, rhs_evals_cluster[sorted_indices],\
                        c=c_eig, cmap='jet_r')#len(self.dynn_blocks)
                ax = plt.gca()
                cb0 = fig3.colorbar(heat_map_C, ax=ax)#location='bottom', 
                cb0.ax.tick_params(labelsize=fontsize) 
            else:
                heat_map_C = plt.scatter(cluster, rhs_evals_cluster, color = 'g', alpha=0.6, s=30)#len(self.dynn_blocks)marker = '.',
        
        else:
            if color_bar:
                heat_map_C = plt.scatter(cluster, rhs_evals_cluster,\
                        c=cluster, cmap='jet')#len(self.dynn_blocks)
                ax = plt.gca()
                cb0 = fig3.colorbar(heat_map_C, ax=ax)#location='bottom', 
                cb0.ax.tick_params(labelsize=fontsize) 
            else:
                heat_map_C = plt.scatter(cluster, rhs_evals_cluster, color = 'g', alpha=0.6, s=30)#len(self.dynn_blocks)marker = '.',
            
        #plt.xticks(ticks=[2, 4, 6], fontsize=fontsize)
        plt.xlabel(x_label, fontsize=fontsize) #HHL, Neuron
        plt.ylabel(y_label, fontsize=fontsize)# Avg /neurons in cluster
        plt.locator_params(axis='both', integer=True)
        plt.tick_params(axis='both', labelsize=fontsize)

        #plt.title('ODE Solver Evaluations', fontsize=fontsize)

        fig3.tight_layout()
        plt.savefig(figname + '_NFE.pdf')
        
    
    def plot_rhs_evaluations_sep_figures(self, savefig=False, 
            figname='rhs_evals', figsize=(2.5, 2), cmap='jet', 
            counter = 0, marker_size = 20, figsize_2=(2.5 ,2.)):
        n_evals = 0
        n_neurons = 0 
        rhs_evals_cluster = np.zeros(len(self.dynn_blocks))
        n_neurons_block = np.zeros(len(self.dynn_blocks))
        cluster_of_each_neuron = []
        cluster_of_each_eval = []
        block_types = []
    
        for i in range(len(self.dynn_blocks)):
            # Gather number of neurons as n_neurons
            if i == 0:
                rhs_evals_neuron = self.dynn_blocks[i].rhs_evaluations
                
            if i > 0:
                rhs_evals_neuron = np.hstack((rhs_evals_neuron, self.dynn_blocks[i].rhs_evaluations))
            
            if self.dynn_blocks[i].block_type == 'complex': 
                n_eval_i = 2 * self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real':
                n_eval_i = self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real_complex':
                n_eval_i = self.dynn_blocks[i].k_r + \
                            2 * self.dynn_blocks[i].k_c
                n_evals += n_eval_i
                cluster_of_each_eval.extend((i+1) * np.ones(n_eval_i, dtype=int))

            else:
                raise ValueError('Check the block type!')
            n_neurons_block[i] = self.dynn_blocks[i].d_A
            n_neurons += n_neurons_block[i]
            rhs_evals_cluster[i] = np.sum(self.dynn_blocks[i].rhs_evaluations)/self.dynn_blocks[i].d_A

            cluster_of_each_neuron.extend(i * np.ones(self.dynn_blocks[i].d_A, dtype=int))
            block_types.append(self.dynn_blocks[i].block_type)
        
        neuron = np.arange(n_neurons) + 1
        cluster = np.arange(len(self.dynn_blocks)) + 1
        
        fig0 = plt.figure(figsize=figsize)
        fontsize = 10
        
        if all(bt == 'real' for bt in block_types):
            #axs0 = fig.add_subplot(1, 3, 1)
            heat_map_A = plt.scatter(np.arange(n_evals) + 1, \
                self.eigenvalues.real, c=cluster_of_each_eval, \
                    cmap=cmap)
            #axs0.set_ylim([0, 200])
            #plt.xscale('symlog')
            plt.xlabel('Neuron', fontsize=fontsize)
            plt.ylabel('Eigenvalue', fontsize=fontsize)
            #plt.title('Eigenvalues', fontsize=fontsize)
        else:
            heat_map_A = plt.scatter(self.eigenvalues.real,\
                self.eigenvalues.imag, c=cluster_of_each_eval, cmap=cmap)#,s=marker_size
            plt.xlabel('Real', fontsize=fontsize)
            #plt.xscale('log')
            #plt.yscale('log')
            plt.ylabel('Imaginary', fontsize=fontsize)
            #plt.title('Eigenvalues', fontsize=fontsize)
        ax = plt.gca()
        cb0 = fig0.colorbar(heat_map_A, ax=ax, location='right')
        fig0.tight_layout()
        plt.savefig(figname + '_0.pdf')
        
        #fig1 = plt.figure(figsize=(10,2.)) # figsize
        fig1 = plt.figure(figsize=figsize_2) # figsize

        #plt.rcParams['text.usetex'] = True

        fontsize = 10
        if all(bt == 'real_' for bt in block_types): # Remove the underscore
            #axs0 = fig.add_subplot(1, 3, 1)
            heat_map_A = plt.scatter(np.arange(n_evals) + 1, self.eigenvalues.real,\
                c=cluster_of_each_eval, cmap=cmap,s=marker_size)#, s=300, edgecolors='black', linewidths=2
            #axs0.set_yscale('log')
            plt.xlabel('Neuron', fontsize=fontsize)
            plt.ylabel('Eigenvalue', fontsize=fontsize)
            #plt.title('clusters', fontsize=fontsize)
            #plt.title(r'Eigenvalues of $\mathcal{A}$', fontsize=fontsize)
            plt.xticks(fontsize=fontsize)
            plt.yticks(fontsize=fontsize)

            ax = plt.gca()
            cb1 = fig1.colorbar(heat_map_A, ax=ax, location='right')
        else:
            heat_map_A = plt.scatter(self.eigenvalues.real,\
                self.eigenvalues.imag, c=cluster_of_each_eval, cmap=cmap)#s=300, edgecolors='black', linewidths=2
            plt.xlabel('Real', fontsize=fontsize)
            plt.ylabel('Imaginary', fontsize=fontsize)
            #plt.xscale('log')
            #plt.title('No. of clusters = %d' %counter, fontsize=fontsize)
            #plt.title(r'Eigenvalues of $\mathcal{A}$', fontsize=fontsize)
            ax = plt.gca()
            cb1 = fig1.colorbar(heat_map_A, ax=ax, location='right')
            #cb1.set_ticks(cluster)
            plt.yticks(fontsize=fontsize)
        #plt.xticks(fontsize=fontsize)
        fig1.tight_layout()
        plt.savefig(figname + '_1.pdf')
        
        fig2 = plt.figure(figsize=figsize_2)

        y_pos = np.arange(len(self.dynn_blocks)) + 1
        my_cmap = plt.cm.get_cmap('jet')
        colors = my_cmap(y_pos)

        # Normalize values to 0-1
        norm = plt.Normalize(y_pos.min(), y_pos.max())

        # Apply the color map
        colors = my_cmap(norm(y_pos))
        bar_chart = plt.barh(y_pos, n_neurons_block, align='center', color=colors)
        #plt.yticks(y_pos)#, labels=np.arange(n_clusters)
        ax = plt.gca()
        ax.invert_yaxis()  # labels read top-to-bottom
        plt.ylabel('Horizontal layer', fontsize=fontsize)
        plt.xlabel('Depth (no. of neurons)', fontsize=fontsize)
        #plt.title('DyNN hidden layers', fontsize=fontsize)
        ax = plt.gca()
        cb1 = fig1.colorbar(heat_map_A, ax=ax, location='right')
        fig2.tight_layout()
        plt.savefig(figname + '_2.pdf')
        
        fig3 = plt.figure(figsize=(7./3., 2.5))

        heat_map_C = plt.scatter(cluster, rhs_evals_cluster,\
                c=cluster, cmap='jet')#len(self.dynn_blocks)
        ax = plt.gca()
        fig3.colorbar(heat_map_C, ax=ax)#location='bottom', 
        #plt.xticks(ticks=[2, 4, 6], fontsize=fontsize)
        plt.xlabel('Horizontal layer', fontsize=fontsize) #HHL, Neuron
        plt.ylabel('Avg NFE', fontsize=fontsize)# Avg /neurons in cluster
        #plt.title('ODE Solver Evaluations', fontsize=fontsize)

        fig3.tight_layout()
        plt.savefig(figname + '_3.pdf')
    
    def plot_rhs_evaluations(self, savefig=False, figname='rhs_evals.pdf'):
        n_evals = 0
        n_neurons = 0 
        rhs_evals_cluster = np.zeros(len(self.dynn_blocks))
        n_neurons_block = np.zeros(len(self.dynn_blocks))
        cluster_of_each_neuron = []
        cluster_of_each_eval = []
        block_types = []
        for i in range(len(self.dynn_blocks)):
            # Gather number of neurons as n_neurons
            if i == 0:
                rhs_evals_neuron = self.dynn_blocks[i].rhs_evaluations
                
            if i > 0:
                rhs_evals_neuron = np.hstack((rhs_evals_neuron, self.dynn_blocks[i].rhs_evaluations))
            
            if self.dynn_blocks[i].block_type == 'complex': 
                n_eval_i = 2 * self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend(i * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real':
                n_eval_i = self.dynn_blocks[i].d_A
                n_evals += n_eval_i
                cluster_of_each_eval.extend(i * np.ones(n_eval_i, dtype=int))

            elif self.dynn_blocks[i].block_type == 'real_complex':
                n_eval_i = self.dynn_blocks[i].k_r + \
                            2 * self.dynn_blocks[i].k_c
                n_evals += n_eval_i
                cluster_of_each_eval.extend(i * np.ones(n_eval_i, dtype=int))

            else:
                raise ValueError('Check the block type!')
            n_neurons_block[i] = self.dynn_blocks[i].d_A
            n_neurons += n_neurons_block[i]
            rhs_evals_cluster[i] = np.sum(self.dynn_blocks[i].rhs_evaluations)/self.dynn_blocks[i].d_A

            cluster_of_each_neuron.extend(i * np.ones(self.dynn_blocks[i].d_A, dtype=int))
            block_types.append(self.dynn_blocks[i].block_type)
        
        neuron = np.arange(n_neurons) + 1
        cluster = np.arange(len(self.dynn_blocks)) + 1
        fig = plt.figure(figsize=(7, 2.5))
        fontsize = 10
        """
        fig, axes = plt.subplots(1, 2, figsize=(7.,2.5))

        # Example data
        #people = ('Tom', 'Dick', 'Harry', 'Slim', 'Jim')

        """
        
        #fig.suptitle(f'No. of RHS evaluations', fontsize=14)
        if all(bt == 'real' for bt in block_types):
            axs0 = fig.add_subplot(1, 3, 1)
            axs0.scatter(np.arange(n_evals) + 1, self.eigenvalues.real,\
                c=cluster_of_each_eval, cmap='jet')
            #axs0.set_ylim([0, 200])
            #axs0.set_yscale('log')
            #cb1 = fig.colorbar(heat_map_A, ax=axs[0],location='bottom', shrink=0.7)
            axs0.set_xlabel('Neuron', fontsize=fontsize)
            axs0.set_ylabel('Eigenvalue', fontsize=fontsize)
            axs0.set_title('Eigenvalues', fontsize=fontsize)

        else:
            axs0 = fig.add_subplot(1, 3, 1)#np.arange(n_neurons + 1) + 1
            axs0.scatter(self.eigenvalues.real,\
                self.eigenvalues.imag, c=cluster_of_each_eval, cmap='jet')
            #cb1 = fig.colorbar(heat_map_A, ax=axs[0],location='bottom', shrink=0.7)
            axs0.set_xlabel('Real', fontsize=fontsize)
            axs0.set_ylabel('Imaginary', fontsize=fontsize)
            axs0.set_title('Eigenvalues', fontsize=fontsize)

            #axs0.title.set_text('Eigenvalues')

        """  
        axs1 = fig.add_subplot(1, 3, 2)
        axs1.scatter(neuron, rhs_evals_neuron, \
            c=cluster_of_each_neuron, cmap='jet')
        #fig.colorbar(heat_map_B, ax=axs[1], location='bottom', shrink=0.7)
        axs1.set_xlabel('Neuron')
        axs1.set_ylabel('Evaluations of rhs')
        """  
        axs1 = fig.add_subplot(1, 3, 2)

        y_pos = np.arange(len(self.dynn_blocks))
        my_cmap = plt.cm.get_cmap('jet')
        colors = my_cmap(y_pos)

        # Normalize values to 0-1
        norm = plt.Normalize(y_pos.min(), y_pos.max())

        # Apply the color map
        colors = my_cmap(norm(y_pos))
        bar_chart = axs1.barh(y_pos, n_neurons_block, align='center', color=colors)
        axs1.set_yticks(y_pos)#, labels=np.arange(n_clusters)
        axs1.invert_yaxis()  # labels read top-to-bottom
        axs1.set_ylabel('Horizontal Layer', fontsize=fontsize)
        axs1.set_xlabel('Depth (# Neurons)', fontsize=fontsize)
        axs1.set_title('DyNN hidden layers', fontsize=fontsize)
        #xticks = np.arange(1, np.amax(n_neurons) + 1, 1, dtype='int')
        #axs1.set_xticks(xticks)
        
        axs2 = fig.add_subplot(1, 3, 3) # (1, 3, 3)
        heat_map_C = axs2.scatter(cluster, rhs_evals_cluster,\
                c=cluster, cmap='jet')#len(self.dynn_blocks)
        axs2.set_yscale('log', base=2)
        fig.colorbar(heat_map_C, ax=axs2)#location='bottom', 
        axs2.set_xlabel('Horizontal Layer', fontsize=fontsize)
        axs2.set_ylabel('Avg RHS Evaluations', fontsize=fontsize)#/neurons in cluster
        axs2.set_title('ODE Solver: RHS Evaluations', fontsize=fontsize)
        #axs2.set_ylim(2**12, 2**15)
        #ax2.set_xticks(x + width, species)

        fig.tight_layout()
        if savefig:
            plt.savefig(figname)

    def get_weights(self, visualize=True):
        # For real eigenvalues
        t1 = np.zeros((self.ssm.dim[0],)) # np.empty([10, ])
        w_xi_tilde = np.zeros((self.ssm.dim[0], self.ssm.dim[0]))
        w_u_hat = np.zeros((self.ssm.dim[0], self.ssm.dim[1]))
        k = 0
        for i in range(len(self.dynn_blocks)):
            # Extract no. neurons in the current block
            n_neurons = self.dynn_blocks[i].d_A
            
            # Extract tau_1
            t1[k:k+n_neurons] = self.dynn_blocks[i].tau_1
            
            # Extract w_xi_tilde
            upper_triangular_mask = np.triu(np.ones((n_neurons, n_neurons), dtype=bool), k=1)
            w_xi_tilde_new = w_xi_tilde[k:k+n_neurons, k:k+n_neurons]
            w_xi_tilde_new[upper_triangular_mask] = self.dynn_blocks[i].W_xi_tilde
            
            # Extract W_u_hat
            w_u_hat[k:k+n_neurons, :] = self.dynn_blocks[i].W_u_hat
            
            # Change the starting index
            k += n_neurons
        
        # Extract weights in different norms
        tau_1 = np.diag(t1)
        a_min = np.amin(tau_1)
        a_max = np.amax(tau_1)
                        
        b_min = np.amin(w_xi_tilde)
        b_max = np.amax(w_xi_tilde)
            
        c_min = np.amin(w_u_hat)
        c_max = np.amax(w_u_hat)
        
        
        inf_norm_weights = np.array([a_max, b_max, c_max])
        two_norm_weights = np.array([LA.norm(tau_1, ord=2), \
                            LA.norm(w_xi_tilde, ord=2), \
                            LA.norm(w_u_hat, ord=2)])

        if visualize:
            # Visualize the three matrices
            fig, axs = plt.subplots(1, 3, figsize=(9, 2.5))
            fig.suptitle(f'No. of clusters = {i+1}', fontsize=14)

            heat_map_A = axs[0].imshow(tau_1, \
                    interpolation='none', vmin=a_min, vmax=a_max)
            cb1 = fig.colorbar(heat_map_A, ax=axs[0],location='bottom', shrink=0.7)
            axs[0].set_title('$A_1$')

            heat_map_B = axs[1].imshow(w_xi_tilde, \
                    interpolation='none', vmin=b_min, vmax=b_max)
            fig.colorbar(heat_map_B, ax=axs[1], location='bottom', shrink=0.7)
            axs[1].set_title('$A_2$')     

            heat_map_C = axs[2].imshow(w_u_hat, \
                    vmin=c_min, vmax=c_max)
            fig.colorbar(heat_map_C, ax=axs[2], location='bottom', shrink=0.7)
            axs[2].set_title('B')
            fig.tight_layout()
        
        return inf_norm_weights, two_norm_weights

    
    def predict(self, inputs, timesteps, forward_pass_params: forward_pass_parameters, n_blocks=None, init_state = None):
        # Forward Propagation
        u = inputs[0]
        du_dt = inputs[1]
        y = np.zeros((len(timesteps), self.ssm.dim[2]))         
        if n_blocks is None:
            n_blocks = len(self.dynn_blocks)
        for b in range(n_blocks):
            # TODO: Have a base class with the forward class function and dynn blocks be a list of classs objects instead of None
            # TODO: Remove the return x_block part!
            # Select appropriate forward pass
            if (forward_pass_params.outer_loop == 'neurons'):
                y_block, x_block = self.dynn_blocks[b].forward_pass_outer_loop_neurons(timesteps, inputs, forward_pass_params)
            elif (forward_pass_params.outer_loop == 'timesteps'):
                y_block, x_block = self.dynn_blocks[b].forward_pass_outer_loop_timesteps(timesteps, inputs, forward_pass_params)
            else:
                raise ValueError("Select an appropriate loop!")
            #y_block, x_block = self.dynn_blocks[b].forward_pass_outer_loop_neurons(timesteps, dt, inputs, u_interp='piecewise_linear')
            y += y_block # Time-steps * output
        # End loop

        # Add the contribution from the input term
        if forward_pass_params.input_interpolation != 'sample':
            y += np.matmul(self.ssm.D, u.T).T # Dims of y: Timesteps * output
        else:
            t = timesteps
            i = np.arange(0, self.ssm.dim[1], 1)
            u_eval = u(np.reshape(t, (len(t), 1)), np.reshape(i, (1, len(i))))
            y += np.matmul(self.ssm.D, u_eval.T).T # Dims of y: Timesteps * output
        return y
    
    
    def fit(self, clustering_alg, verbose, show_plots, init_cond=None):
        #TODO: Receive xi_0, xi_0_prime too! 
        self.ssm, block_sizes, n_r, n_c, self.cond_num, xi_0 = \
            self.get_transformed_state_space_model(clustering_alg, verbose, show_plots, init_cond)
        
        # Pass xi_0, xi_0_prime to initialize the states appropriately
        self.construct_dynn_blocks(block_sizes, n_r, n_c, xi_0)
        
        self.compute_weights(verbose)
        
    
    def compute_weights(self, verbose):
        for b in range(len(self.dynn_blocks)):
            # Compute weights (using mapping from the SSM matrices --> Weights of DNN)
            self.dynn_blocks[b].compute_weights()
            if verbose > 0:
                self.dynn_blocks[b].print_trainable_parameters()
            
    
    def construct_dynn_blocks(self, block_sizes, n_r, n_c, init_cond):
        n_blocks = block_sizes[0][0]
        b_st = 0
        b_end = 0
        for i in range(n_blocks):
            # Initialize the SSM <-> 1 diagonal block
            b_end += block_sizes[1][i]
            exec(f'ssm_{i+1} = state_space_model(\
                self.ssm.A[b_st:b_end, b_st:b_end],\
                self.ssm.B[b_st:b_end, :], \
                self.ssm.C[:, b_st:b_end], \
                self.ssm.D)')
            
            
            #TODO: Compute appropriate xi_0 and xi_0_prime to appropriate blocks to initialize
            
            # Initialize the appropriate dynn block
            if (n_r[i] > 0 and n_c[i] == 0): # only real eigenvalues of A in this block
                exec(f'DyNN_block_{i+1} = hidden_block_real_repeated(ssm_{i+1}, init_cond[b_st:b_end])')
            elif (n_r[i] == 0 and n_c[i] > 0): # only complex eigenvalues of A in this block
                exec(f'DyNN_block_{i+1} = hidden_block_complex_repeated(ssm_{i+1}, init_cond[b_st:b_end])')
            elif (n_r[i] > 0 and n_c[i] > 0): # real and complex eigenvalues of A in this block
                exec(f'DyNN_block_{i+1} = hidden_block_real_complex(ssm_{i+1}, [n_r[i], n_c[i]], init_cond[b_st:b_end])')
            else:
                raise ValueError("Check eigenvalues again!")
            b_st += block_sizes[1][i]
            
            # Add the block to the dynn architecture
            exec(f'self.dynn_blocks.append(DyNN_block_{i+1})')

    
    
    def get_transformed_state_space_model(self, clustering_alg, verbose, show_plots, init_cond=None):
        R, Q = self.compute_real_schur_decomposition(self.ssm.A)
        if verbose:
            print('Transformation 1: Real Schur Decomposition')
            print('Q = \n', Q)
            print('R = \n', R)
        self.verify_real_schur_form(self.ssm.A, Q, R)
        
        # Check if the matrix is unitarily diagonalizable (for example when a_{i,j} = 0 for j >= i)
        n_ud = np.count_nonzero(np.triu(R) > 1e-10)
        if n_ud == 0:
            print('The matrix is unitarily diagonalizable!')
            print('Skipping Bartels-Stewart algorithm!')
            n_r = np.ones(np.shape(R)[0], dtype='int')
            n_c = np.zeros(np.shape(R)[0], dtype='int')
            block_sizes = compute_block_sizes(n_r, n_c)
            self.eigenvalues = self.extract_eigenvalues(R)  
            Z = Q
        
        # Perform block diagonalization
        if n_ud > 0:
            T, Z = self.get_complex_schur_form(R, Q)
            eigenvalues = self.extract_eigenvalues(T)
            sorted = False # Implies that eigenvalues are not sorted
            #self.plot_evals(eigenvalues, sorted, verbose, show_plots)
            
            # Sort the eigenvalues: clustering + ordering of clusters 
            Q, R, block_sizes, n_r, n_c, ap= sort_real_schur(Q, R, clustering_alg, 
                                    show_plots, 
                                    verbose,
                                    b=0, inplace=True)
            # The following can be commented: Just for visualiazation
            T, Z = self.get_complex_schur_form(R, Q)
            self.eigenvalues = self.extract_eigenvalues(T)  
            sorted = True      
        
            # Perform sanity checks
            self.perform_sanity_checks(self.ssm.A, Q, R, eigenvalues, self.eigenvalues)
            
            # Perform diagonalization        
            Z = block_diagonalization(Q, R, block_sizes)

        if verbose > 0:
            print('\n Number of real and complex eigenvalues per cluster: \n')
            print('n_r: \n', n_r)
            print('n_c: \n', n_c)
    
        self.cond_num = np.linalg.cond(Z, p =None)
        # Get transformed state space model (in a new coordinate system)
        if init_cond is None:
            init_cond = np.zeros((np.shape(Z)[0],)) 
        self.ssm, xi_0 = self.transform_coordinates_ssm(Z, verbose, init_cond)


        return self.ssm, block_sizes, n_r, n_c, self.cond_num, xi_0

    
    def compute_real_schur_decomposition(self, A):
        R, Q = LA.schur(A, output='real')
        return R, Q
    
    def verify_real_schur_form(self, A, Q, R):
        assert np.allclose(np.dot(A, Q), np.dot(Q, R))
        
    def get_complex_schur_form(self, R, Q):
        T, Z = LA.rsf2csf(R, Q)
        return T, Z
    
    def perform_sanity_checks(self, A, Q, R, eigenvalues, eigenvalues_sorted):
        # Set the real and imaginary parts < 1e-10 to 0
        ev_sorted_real = truncate_real(eigenvalues_sorted, threshold=1e-10)
        evals_sorted = truncate_complex(ev_sorted_real, threshold=1e-10)

        for a, b in zip(np.sort(eigenvalues), np.sort(evals_sorted)):
            assert np.allclose(a, b) or np.allclose(a, np.conj(b))  # TODO: check that both b and conj(b) are present in list

        eps = np.finfo(R.dtype).eps
        r = np.count_nonzero(np.abs(np.diag(R, -1)) > 100*eps)
        assert np.allclose(np.dot(A, Q), np.dot(Q, R))  # check that still a decomposition of the original matrix
        # test that Q and R have the correct structure
        assert np.allclose(np.dot(Q, Q.T), np.eye(A.shape[0]))  # Q orthonormal
        assert np.all(np.tril(R, -2) == 0)  # R triangular
        assert r == np.count_nonzero(np.abs(np.diag(R, -1)) > 100 * eps)  # number of blocks in R is preserved

    def extract_eigenvalues(self, T):
        ev_orig = np.diag(T)
        return ev_orig 
        
    def plot_evals(self, eigenvals_A, sorted, verbose, show_plots):
        # TODO: Get rid of the lists from below
        if verbose > 0: 
            if sorted:
                print('Eigenvalues after sorting: \n ', eigenvals_A, '\n')
            else:
                print('Eigenvalues before sorting: \n ', eigenvals_A, '\n')
      
        # Plot the eigenvalues of A
        if show_plots:
            fig = plt.figure(figsize=(7, 2.5))
            if sorted:
                fig.suptitle('Eigenvalues of the state matrix A: after sorting')
            else:
                fig.suptitle('Eigenvalues of the state matrix A: before sorting')
            re = []
            imag = []
            mag = []
            for i in range(len(eigenvals_A)):
                re.append(eigenvals_A[i].real)
                imag.append(eigenvals_A[i].imag)
                mag.append(40.0 * np.sqrt(eigenvals_A[i].real **2 + eigenvals_A[i].imag ** 2)) # For size of circle in the plot
                    
            plt.scatter(re, imag , cmap='jet', s=mag, alpha=0.5)

            for i, txt in enumerate(np.arange(len(eigenvals_A))):
                plt.annotate(txt, (re[i], imag[i]))
            plt.xlabel('Re')
            plt.ylabel('Im')
            fig.tight_layout()
            #fig.savefig('eigenvalues.pdf')
            plt.show()

            #plt.savefig('eigenvalues.pdf')


    def transform_coordinates_ssm(self, Z, verbose, init_cond):  
        if verbose > 1:
            print('\nZ = \n', Z)

        #TODO: Can we get rid of this inverse? --> Especially imp for large matrices! 
        Z_inv = np.linalg.inv(Z)
        if verbose > 1:
            print('\nZ^(-1) = \n', Z_inv)

        # Resulting block diagonal matrix 
        T = Z_inv @ self.ssm.A @ Z #np.matmul(np.matmul(Z_inv, A), Z)

        # New state-space model
        A_t = T
        B_t = Z_inv @ self.ssm.B
        C_t = self.ssm.C @ Z
        D_t = self.ssm.D
        
        # TODO: Can remove the following steps later!
        #A_t[abs(A_t) < 1e-10] = 0.
        #B_t[abs(B_t) < 1e-10] = 0.
        #C_t[abs(C_t) < 1e-10] = 0.
        #D_t[abs(D_t) < 1e-10] = 0.
        if verbose > 1:
            print("A_t: \n", A_t)
            print("Bt: \n", B_t)
            print("C_t: \n", C_t)
            print("D_t: \n", C_t)
        
        ssm_new = state_space_model(A_t, B_t, C_t, D_t)
        xi_0 = Z_inv @ init_cond
        return ssm_new, xi_0




# Do not name this as a hidden block as it also has the weights of the output layer
class DyNN_block:
    def __init__(self, ssm: state_space_model):
        pass
        
    def compute_weights(self):
        pass
    
    def forward_pass(self, timesteps, a_1, a_2, h):
        pass
           
    def forward_pass_scipy(self, timesteps, h):
        pass
    
    
    
    
#TODO: Define a parent class hidden block with child classes real_block, complex_block, real_complex_block
class hidden_block_real_repeated:
    def __init__(self, ssm: state_space_model, xi_0):
        self.block_type = 'real'
        self.ssm = ssm  # State-space model
        self.d_A = self.ssm.dim[0]  # Dimension of the state-variable in matrix A
        
        # Weights/Trainable parameters
        self.tau_0 = np.zeros(self.d_A)
        self.tau_1 = np.zeros(self.d_A)
        self.tau_2 = np.zeros(self.d_A) # For real eigenvalues    
        
        # Flag for checking whether the ode is divided by tau_0 to make the coefficient of xi = 1
        self.div_by_tau_0 = False 
        self.theta = np.zeros(self.d_A) 
        self.si = np.empty(self.d_A - 1, dtype=int)   # si: starting index for neuron 'i'
        self.W_xi_tilde = np.zeros(int((self.d_A - 1) * self.d_A / 2))
        self.W_u_hat = None #(If B =!0)
        # evaluations of rhs for each neuron ODE solver
        self.rhs_evaluations = np.zeros(self.d_A)
        #self.evals = None
        #self.evals_inv = None
        
        # State and first time-derivative of state
        self.xi = xi_0 # Initialized with zeros
        self.xi_prime = np.zeros(self.d_A)
        
    
    def set_connection_weights_indices(self):
        """Sets starting indices of the w_xi_tilde for each neuron. i.e each neuron with 
        index 'i' has d_A - (i + 1) connections, one with respect to each of the 
        previous d_A - (i + 1) neurons in the current dense block. 
        
        The weights are multiplied with the respective states of the neurons in the 
        same dense block/horizontal layer, prior to the current neuron. 
        
        Such weights corresponding to each neuron in one dense block are stacked in the 
        vector w_xi_tilde. Starting index indicates where the weights curresponding to 
        any neuron 'i' start. 
        
        Note that the last neuron 'd_A - 1' in any dense block is not connected to 
        any other neuron- Thus, len(starting_indices) = d_A - 1.
        """
        for i in range(self.d_A - 1):   # Iterate over all neurons in the block except the last
            if (i == 0):
                self.si[i] = 0
            else:
                self.si[i] = self.si[0 + i - 1] + self.d_A - i
                    
                        
    def extract_diagonal(self):  # Extract eigenvalues
        self.tau_0 = np.diag(self.ssm.A) 
        #if np.amin(abs(self.tau_0)) > 1e-2: 
        #    self.tau_0_inv = np.reciprocal(self.tau_0)

    
    def compute_tau_1(self):
        if np.amin(abs(self.tau_0)) < 1e-2:           
            # Do not divide by the eigenvalue to make the coefficient of xi = 1 
            #ind = [i for i in range(len(self.evals)) if abs(self.evals[i]) < 1e-2]
            self.tau_1 = -1. * np.ones_like(self.tau_0)
            self.div_by_tau_0 = False  
        else:
            self.tau_1 = - np.reciprocal(self.tau_0)
            self.div_by_tau_0 = True
    
    
    def initialize_W_xi_tilde(self):
        for i in range(self.d_A - 1):
            n_weights = self.d_A - (i + 1)
            self.W_xi_tilde[self.si[i] : self.si[i] + n_weights] = self.tau_1[i] * self.ssm.A[i,i+1:] 
 

    def compute_W_u_hat(self):
        self.W_u_hat =  self.ssm.B * (self.tau_1)[:,None] # Broadcasting
        assert np.shape(self.W_u_hat) == np.shape(self.ssm.B)

        
    def compute_f_zoh(self, i, u_hat):
        """
        Computes the forcing term/right hand side of the ODE to be solved for the state of neuron 'i'

        Args:
            i (int): neuron number
            u_hat (array): input (m * 1)

        Returns:
            float: forcing term for neuron 'i'
        """
        if i == self.d_A - 1:
            static_term = np.matmul(self.W_u_hat[i], u_hat) # (1, m) * (m, 1)
        else:
            n_weights = self.d_A - (i + 1)
            static_term = np.matmul(self.W_u_hat[i], u_hat)  \
                + np.matmul(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], \
                    self.xi[i+1:])
        return static_term

    def set_forcing_term_function_handle_neuron(self, u_interp):
        def xi_prime(t, xi, i, tau_0, tau_1, W_u_hat, u_hat,u_hat_prime,
                         W_xi_tilde, states_neurons, d_A, 
                         timesteps, dt):
            
            
            # Set appropriate u(t)
            if u_interp == 'piecewise_constant':  
                # Find the index of the time-step which has the value <= t 
                ind = int(t/dt)
                if t > 0 and type(t / dt) == int: ind -= 1
                
                # Compute u(t)
                u = u_hat[ind, :]
            
            elif u_interp == 'piecewise_linear': 
                # Find the index of the time-step which has the value <= t 
                ind = int(t/dt)
                if t > 0 and type(t / dt) == int: ind -= 1
                
                # Compute u(t)
                u = u_hat[ind, :] + u_hat_prime[ind, :] * (t - timesteps[ind])
            
            elif u_interp == 'sample':
                u = u_hat(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
            
            else:
                raise ValueError('Check the input_interpolation method!')
            
            # Compute f
            f = W_u_hat @ u.T # (1, m) * (m, 1)
                
            # Interpolate the states from previous neurons at time t
            if i != d_A - 1: 
                c = 0
                for j in range(i+1, d_A):
                    f += W_xi_tilde[c] * states_neurons[j].sol(t)
                    c += 1
               
            return 1.0/tau_1 * (f - tau_0 * xi) 
        return xi_prime


         
    
    def compute_output(self, xi):
        return (np.matmul(self.ssm.C, xi))      


    def forward_pass(self, timesteps, a_1, a_2, h, inputs):
        u_hat = inputs[0]
        #TODO: Implement one more forward pass with a black-box ODE solver
        h_inv = 1./h
        
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, neurons, states per neuron
        #sol_dnn = np.ones((len(timesteps),self.d_A, 2)) # timesteps, neurons, states per neuron
        y_dnn = np.zeros((len(timesteps), self.ssm.dim[2])) # timesteps, neurons, states per neuron

        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first
                # Solve the ODE and store the state for each time step and update it for next time-steps 
                #TODO: Group the following as one function: i.e y_new , z_new = odesolve(a_1, a_2, f)
                             
                # compute the forcing term 
                f = self.compute_f_zoh(i, u_hat[t-1])
                
                # Compute an intermediate term to re-use
                term_1 = a_1 * self.tau_1[i] * h_inv + self.tau_2[i] * h_inv * h_inv
                
                # Compute new solution: y_new, first order time-derivative: z_new
                sol_dnn[t, i, 0] = (a_1 * a_1 * f + a_1 * a_2 * f + (-a_1 * a_2 + term_1) * sol_dnn[t-1, i, 0]
                        + (a_1 + a_2) * h_inv * self.tau_2[i] * sol_dnn[t-1, i, 1]) / (a_1 * a_1 + term_1)
                 
                sol_dnn[t, i, 1]= (sol_dnn[t, i, 0] - sol_dnn[t-1, i, 0])/ (h * a_1) - (a_2 / a_1) * sol_dnn[t-1, i, 1]
                
            # Update state and it's derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            y_dnn[t, :] = self.compute_output(self.xi)

        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2     
         
          
    def print_trainable_parameters(self):
        print('\nAlgebraic multiplicity = ', '\n\nTau_1 = ', self.tau_1, '\n\nW_xi_tilde = \n', 
              self.W_xi_tilde, '\n\nW_u_hat = \n', self.W_u_hat)
        
    def compute_weights(self):
        self.set_connection_weights_indices()
        self.extract_diagonal()
        self.compute_tau_1()
        self.initialize_W_xi_tilde()
        self.compute_W_u_hat()

    def extract_W_xi_tilde(self, i):
        if i == self.d_A - 1:
            W_xi_tilde_i = None
        else:
            n_weights = self.d_A - (i + 1)
            W_xi_tilde_i = np.reshape(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], -1)
        return W_xi_tilde_i

    def set_forcing_term_function_handle_time_neuron(self, u_interp):
        def xi_prime(t, xi, i, tau_0, tau_1, W_u_hat, u_old, du_dt_old,
                         W_xi_tilde, states_neurons, d_A, 
                         t_s, dt):
            # Set appropriate u(t)
            if u_interp == 'piecewise_constant':  
                u = u_old
            elif u_interp == 'piecewise_linear': 
                u = u_old + du_dt_old * (t - t_s)
            elif u_interp == 'sample':
                u = u_old(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
            else:
                raise ValueError('Check the input_interpolation method!')
            
            # Compute f
            f = W_u_hat @ u.T # (1, m) * (m, 1)
                
            # Interpolate the states from previous neurons at time t
            if i != d_A - 1: 
                c = 0
                for j in range(i+1, d_A):
                    f += W_xi_tilde[c] * states_neurons[j].sol(t)
                    c += 1
                
            return 1.0/tau_1 * (f - tau_0 * xi) 
        return xi_prime
    
       
    def forward_pass_outer_loop_timesteps(self, timesteps, inputs, params: forward_pass_parameters):
        u_hat = inputs[0]
        u_hat_prime = inputs[1]     
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        sol_dnn[0, :, 0]  = self.xi 
        sol_dnn[0, :, 1]  = self.ssm.A @ self.xi + self.ssm.B @ u_hat[0].T
        
        # Initialize solution array
        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        #y_dnn[0, :] = self.ssm.C @ self.xi  + self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        y_dnn[0, :] = (self.ssm.C @ self.xi).reshape(-1, )  #+ self.ssm.D @ (u_hat[0].T) # timesteps, neurons in the output layer

        states_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
        
        u_interp = params.input_interpolation
        f_rhs = self.set_forcing_term_function_handle_time_neuron(u_interp) 
        
        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first        
                # Set initial conditions of the ODE
                y_old = self.xi[i]
                IC = [y_old]
                
                dt = timesteps[1] - timesteps[0]
                t_start = timesteps[t-1]
                
                if u_interp != 'sample':
                    u_old = u_hat[t-1]
                    du_dt_old = u_hat_prime[t-1]
                else:
                    u_old = u_hat
                    du_dt_old = u_hat_prime

                # Correctly set the coefficient of xi
                tau_0 = self.tau_0[i]
                if self.div_by_tau_0:
                    tau_0 = 1.
        
                f_rhs_args = (i, tau_0, self.tau_1[i], self.W_u_hat[i], u_old, du_dt_old,
                            self.extract_W_xi_tilde(i),states_neurons, self.d_A,
                            t_start, dt)
                states_old = solve_ivp(f_rhs, [timesteps[t-1], timesteps[t]], IC, 
                                args= f_rhs_args, 
                                t_eval=np.array([timesteps[t-1], timesteps[t]]),
                                dense_output=True,
                                method='DOP853')#, method='LSODA'
                self.rhs_evaluations[i] +=  states_old.nfev
                # Evaluate the state at some time points
                sol_dnn[t, i, 0] = states_old.y[0, -1] #, -1 #sol.y[0, -1]#, -1
                states_neurons[i] = states_old
            
                # Update the state (Required for computing the forcing term)
                #sol_dnn[t, i, 0] = sol.y[0, -1]#, -1
            
            # Update state after each time-step 
            self.xi = sol_dnn[t, :, 0] #, -1
            
            # Compute output
            y_dnn[t, :] = self.compute_output(self.xi)
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2 

    def forward_pass_outer_loop_neurons(self, timesteps, inputs, \
                            params: forward_pass_parameters): #dense
        # Set u, du_dt
        u_hat = inputs[0]
        u_hat_prime = inputs[1]    
        
        # Initialize the state and state-derivative set to zero
        self.sol_dnn = np.zeros((len(timesteps),self.d_A)) # timesteps, hidden neurons

        self.sol_dnn[0]  = self.xi 
        self.y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        #self.y_dnn[0, :] = (self.ssm.C @ self.xi  + self.ssm.D @ (u_hat[0].reshape(-1,1))).reshape(-1,) # timesteps, neurons in the output layer
        self.y_dnn[0, :] = (self.ssm.C @ self.xi).reshape(-1, )  #+ self.ssm.D @ (u_hat[0].T) # timesteps, neurons in the output layer
        
        states_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
        
        # Set function handle for computing the rhs
        u_interp = params.input_interpolation
        
        # Set number of function evaluations to zero
        f_rhs = self.set_forcing_term_function_handle_neuron(u_interp)
        
        self.rhs_evaluations = np.zeros(self.d_A)
        
        for i in reversed(range(self.d_A)): # Last row first            
            dt = timesteps[1] - timesteps[0]
            
            # Correctly set the coefficient of xi
            tau_0 = self.tau_0[i]
            if self.div_by_tau_0:
                tau_0 = 1.
            
            # Set rhs arguments
            f_rhs_args = (i, tau_0, self.tau_1[i], self.W_u_hat[i], u_hat, u_hat_prime,
                            self.extract_W_xi_tilde(i),states_neurons, self.d_A,
                            timesteps, dt)
            states_old = solve_ivp(f_rhs,
                            [timesteps[0], timesteps[-1]], # Simulation time: [start, end]
                            np.reshape(self.sol_dnn[0, i], -1), # Initial condition
                            args=f_rhs_args, 
                            dense_output=True,
                            atol = params.ode_solver_params.atol,
                            rtol = params.ode_solver_params.rtol,
                            #t_eval = timesteps,
                            method=params.ode_solver_params.ode_solver)
            self.rhs_evaluations[i] += states_old.nfev
            # Evaluate the state at some time points
            self.sol_dnn[:, i] = states_old.sol(timesteps.reshape(-1)) #sol.y[0, -1]#, -1
            #self.sol_dnn[:, i] = states_old.sol(timesteps.reshape(-1)).reshape(np.shape(self.sol_dnn[:, 0])) #sol.y[0, -1]#, -1
            states_neurons[i] = states_old
    
        # Compute output
        y_dnn = self.compute_output(self.sol_dnn.T).T
        return y_dnn, self.sol_dnn  # tensor with dimensions timesteps * n * 2
    
    
class hidden_block_complex_repeated:
    def __init__(self, ssm: state_space_model, xi_0):
        self.block_type = 'complex'
        self.ssm = ssm  # State-space model
        self.d_A = int(self.ssm.dim[0] / 2) # Size of the state vector to be stored
               
        # Weights/Trainable parameters
        self.tau_1 = np.empty(self.d_A) 
        self.tau_2 = np.zeros(self.d_A)                  
        self.W_xi_tilde = np.zeros(int((self.d_A - 1) * self.d_A / 2))
        self.W_u_hat = np.zeros((self.d_A, self.ssm.dim[1])) 
        self.V_xi_tilde = np.zeros(int((self.d_A - 1) * self.d_A / 2))
        self.V_u_hat = np.zeros((self.d_A, self.ssm.dim[1])) 
        self.theta = np.zeros(self.d_A)
       
        # Weights of output layer
        self.Out_W_xi_tilde = np.zeros((self.ssm.dim[2], self.d_A))
        self.Out_W_u_hat = np.zeros((self.ssm.dim[2],self.ssm.dim[1] ))
        self.Out_V_xi_tilde = np.zeros((self.ssm.dim[2], self.d_A))   

        # Eigenvalues of the state matrix and it's inverse
        self.evals = None
        self.evals_inv = None
        
        # evaluations of rhs for each neuron ODE solver
        self.rhs_evaluations = np.zeros(self.d_A)
        
        # State and first time-derivative of state
        self.xi = xi_0 # Initialized with zeros
        self.xi_prime = np.zeros(self.d_A)
        
        # Some variables in the mapping
        self.c_xi = []
        self.c_xi_prime = []
        self.c_u_hat = []
        self.alpha = []
        self.beta = []
        self.gamma = []
        self.mu = []
        self.nu = []
        self.kappa = []
        for i in range(self.d_A):
            self.c_xi.append([])
            self.c_xi_prime.append([])
            self.c_u_hat.append([])
            self.alpha.append([])
            self.beta.append([])
            self.gamma.append([])            
            self.mu.append([])
            self.nu.append([])
            self.kappa.append([])

        #self.c_xi = np.zeros(int(self.d_A * (self.d_A + 1) / 2))
        self.si = np.empty(self.d_A - 1, dtype=int)   # si: starting index for neuron 'i'

    
    def set_connection_weights_indices(self):
        """Sets starting indices of the w_xi_tilde for each neuron. i.e each neuron with 
        index 'i' has d_A - (i + 1) connections, one with respect to each of the 
        previous d_A - (i + 1) neurons in the current dense block. 
        
        The weights are multiplied with the respective states of the neurons in the 
        same dense block, but in the previous layers. 
        
        Such weights corresponding to each neuron in one dense block are stacked in the 
        vector w_xi_tilde. Starting index indicates where the weights curresponding to 
        any neuron 'i' start. 
        
        Note that the last neuron 'd_A - 1' in any dense block is not connected to 
        any other neuron- Thus, len(starting_indices) = d_A - 1.
        
        - Required for functions initialize_W_xi_tilde, compute_f
        """
        for i in range(self.d_A - 1):   # Iterate over all neurons in the block except the last
            if (i == 0):
                self.si[i] = 0
            else:
                self.si[i] = self.si[i - 1] + self.d_A - i
                    

    def i(self, b: int):
        """
        Returns the index of the starting entry for block 'b' of the 2D array 

        Args:
            b (int): block number of the matrix

        Returns:
            int: index of the starting entry for block 'b' of the 2D array
        """
        return 2 * (b - 1) 
    
    def compute_c_xi(self):
        #TODO: Overload the operator and get it in the form 
        # Block p of state matrix A: self.ssm.A[2 * (p - 1):2 * (p - 1) + 2, 2 * (p - 1):2 * (p - 1) + 2]
        # Iterate over blocks p: k, k-1, ..., 1

        for p in reversed(range(1, self.d_A + 1)): # Last row first
            c_xi_p = np.empty(self.d_A + 1 - p)
            alpha_p = 1./self.ssm.A[self.i(p), self.i(p) + 1]
            
            for j in range(self.d_A - p + 1):
                c_xi_p[j] = -self.ssm.A[self.i(p),self.i(p + j)]
                
                for t in range (1, j+1):
                    c_xi_p[j] = c_xi_p[j] - self.ssm.A[self.i(p),self.i(p + t) + 1] * self.c_xi[p-1+t][0][j - t] #* c_xi[p+t][j-t]
                c_xi_p[j] *= alpha_p
            self.c_xi[p-1].append(c_xi_p)  

     
    def compute_c_xi_prime(self):
        for p in reversed(range(1, self.d_A + 1)): # Last row first
            c_xi_prime_p = np.empty(self.d_A + 1 - p)
            alpha_p = 1./self.ssm.A[self.i(p), self.i(p) + 1]
            
            for j in range(self.d_A - p + 1):
                c_xi_prime_p[j] = 0.
                if j == 0:
                    c_xi_prime_p[j] = 1.
                    
                for t in range (1, j+1):
                    c_xi_prime_p[j] -= self.ssm.A[self.i(p),self.i(p + t) + 1] * self.c_xi_prime[p-1+t][0][j - t] #* c_xi[p+t][j-t]
                            
                c_xi_prime_p[j] *= alpha_p
            self.c_xi_prime[p-1].append(c_xi_prime_p)  


    def compute_c_u_hat(self):
        for p in reversed(range(1, self.d_A + 1)): # Last row first
            alpha_p = 1./self.ssm.A[self.i(p), self.i(p) + 1]
            c_u_hat_p = -1. * self.ssm.B[self.i(p),:]
            for t in range(1, self.d_A - p + 1):
                c_u_hat_p -= self.ssm.A[self.i(p),self.i(p + t) + 1] * self.c_u_hat[p+t-1][0]
            
            c_u_hat_p *= alpha_p
            self.c_u_hat[p-1].append(c_u_hat_p)


    def compute_alpha(self):
        for p in range(1,self.d_A+1):
            alpha_p = np.empty(self.d_A + 1 - p)
            for r in range (p, self.d_A + 1):
                alpha_p[r-p] = self.ssm.A[self.i(p) + 1,self.i(r)]
                for q in range(p, r + 1):
                    alpha_p[r-p] += self.ssm.A[self.i(p) + 1,self.i(q) + 1] * self.c_xi[q-1][0][r - q]
            
            self.alpha[p-1].append(alpha_p)
        

    def compute_beta(self):
        for p in range(1, self.d_A + 1):
            beta_p = np.empty(self.d_A + 1 - p)
            for r in range (p, self.d_A + 1):
                beta_p[r-p] = 0.
                for q in range(p, r + 1):
                    beta_p[r-p] += self.ssm.A[self.i(p) + 1,self.i(q) + 1] \
                        * self.c_xi_prime[q-1][0][r - q]
            
            self.beta[p-1].append(beta_p)
        
        
    def compute_gamma(self):
        for p in range(1, self.d_A + 1):
            gamma_p = self.ssm.B[2 * p - 1,:] 
            for r in range (p, self.d_A + 1):
                gamma_p += self.ssm.A[self.i(p) + 1,self.i(r) + 1] \
                    * self.c_u_hat[r-1][0]  
            
            self.gamma[p-1].append(gamma_p)

    
    def compute_mu(self):
        for p in range(1,self.d_A + 1):
            mu_p = np.empty(self.d_A + 1 - p)
            for r in range (p, self.d_A + 1):
                mu_p[r-p] = 0.
                for q in range(p, r + 1):
                    mu_p[r-p] += self.ssm.A[self.i(p),self.i(q) + 1] \
                        * self.alpha[q-1][0][r - q]
        
            self.mu[p-1].append(mu_p)

                     
    def compute_nu(self):
        for p in range(1,self.d_A + 1):
            nu_p = np.empty(self.d_A + 1 - p)
            for r in range (p, self.d_A + 1):
                nu_p[r-p] = 0.
                for q in range(p, r + 1):
                    nu_p[r-p] += self.ssm.A[self.i(p),self.i(q) + 1] \
                        * self.beta[q-1][0][r - q]
        
            self.nu[p-1].append(nu_p)

        
    def compute_kappa(self):
        for p in range(1, self.d_A + 1):
            kappa_p = np.zeros((self.ssm.dim[1]))
            for r in range (p, self.d_A + 1):
                kappa_p += self.ssm.A[self.i(p),self.i(r) + 1] * self.gamma[r-1][0]
            
            self.kappa[p-1].append(kappa_p)
        

    def compute_tau_2(self):
        for p in range(1, self.d_A + 1):
            self.tau_2[p-1] = -1. / self.mu[p-1][0][0]
            
                     
    def compute_tau_1(self):
        for p in range(1, self.d_A + 1):
            self.tau_1[p-1] = (1. / self.mu[p-1][0][0]) \
                * (self.ssm.A[self.i(p),self.i(p)] + self.nu[p-1][0][0])
    
    
    def initialize_W_xi_tilde(self):
        for i in range(self.d_A - 1):
            n_weights = self.d_A - (i + 1)
            self.W_xi_tilde[self.si[i] : self.si[i] + n_weights] \
                = (-1. / self.mu[i][0][0]) * self.mu[i][0][1:]


    def initialize_V_xi_tilde(self):
        for p in range(1, self.d_A):
            c = 0
            for j in range(p + 1, self.d_A + 1):
                self.V_xi_tilde[self.si[p-1] + c] = (-1. / self.mu[p-1][0][0]) \
                    * (self.ssm.A[self.i(p), self.i(j)] + self.nu[p-1][0][j - p] )
                c += 1
    
    def compute_W_u_hat(self):
        for p in range(1, self.d_A + 1):
            self.W_u_hat[p - 1] =  (-1. / self.mu[p - 1][0][0]) * self.kappa[p - 1][0]
        

    def compute_V_u_hat(self):
        for p in range(1, self.d_A + 1):
            self.V_u_hat[p - 1] =  (-1. / self.mu[p - 1][0][0]) * self.ssm.B[self.i(p),:]
    
    
    def compute_Out_W_xi_tilde(self):
        for p in range(1, self.d_A + 1):
            self.Out_W_xi_tilde[:, p - 1] = self.ssm.C[:, self.i(p)]
            for i in range(1, p + 1):
                self.Out_W_xi_tilde[:, p - 1] += self.ssm.C[:, 2* i - 1] * self.c_xi[i-1][0][p - i]
        

    def compute_Out_V_xi_tilde(self):
        for p in range(1, self.d_A + 1):
            for i in range(1, p + 1):
                self.Out_V_xi_tilde[:, p - 1] += self.ssm.C[:, 2* i - 1] * self.c_xi_prime[i-1][0][p - i]
    
    
    def compute_Out_W_u_hat(self):
        for p in range(1, self.d_A + 1):
            self.Out_W_u_hat += np.matmul(self.ssm.C[:, 2*p - 1].reshape(self.ssm.dim[2], 1), self.c_u_hat[p-1][0].reshape(1, self.ssm.dim[1])) 
    
    def extract_W_xi_tilde(self, i):
        if i == self.d_A - 1:
            W_xi_tilde_i = None
        else:
            n_weights = self.d_A - (i + 1)
            W_xi_tilde_i = np.reshape(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], -1)
        return W_xi_tilde_i
        
    def extract_V_xi_tilde(self, i):
        if i == self.d_A - 1:
            V_xi_tilde_i = None
        else:
            n_weights = self.d_A - (i + 1)
            V_xi_tilde_i = np.reshape(self.V_xi_tilde[self.si[i]:self.si[i] + n_weights], -1)
        return V_xi_tilde_i
    
    def compute_f(self, i, u_hat, u_hat_prime):
        if i == self.d_A - 1:
            static_term = np.matmul(self.W_u_hat[i], u_hat) # (1, m) * (m, 1)
            dynamic_term = np.matmul(self.V_u_hat[i], u_hat_prime) # (1, m) * (m, 1)
        else:
            n_weights = self.d_A - (i + 1)
            static_term = (np.matmul(self.W_u_hat[i], u_hat)  
                           + np.matmul(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], 
                                       self.xi[i+1:]))
            dynamic_term = (np.matmul(self.V_u_hat[i], u_hat_prime)  
                           + np.matmul(self.V_xi_tilde[self.si[i]:self.si[i] + n_weights], 
                                       self.xi_prime[i+1:]))
        f = static_term + dynamic_term
        return f

    
    def solve_ODE_manual(self, a_1, a_2, i,h, f, IC):
        h_inv = 1./h
        y_old = IC[0]
        z_old = IC[1]
        
        # Compute an intermediate term to re-use
        term_1 = a_1 * self.tau_1[i] * h_inv + self.tau_2[i] * h_inv * h_inv
                
        # Compute new solution: y_new 
        y_new = (a_1 * a_1 * f + a_1 * a_2 * f + (-a_1 * a_2 + term_1) * y_old
                        + (a_1 + a_2) * h_inv * self.tau_2[i] * z_old) / (a_1 * a_1 + term_1)
        
        # Compute new first order time-derivative: z_new      
        z_new = (y_new - y_old)/ (h * a_1) - (a_2 / a_1) * z_old
                
        return y_new, z_new
   
    
    def compute_output(self, u_hat):
        return (self.Out_W_xi_tilde @ self.xi + self.Out_V_xi_tilde @ self.xi_prime \
            + (self.Out_W_u_hat @ u_hat).T)
                
    def compute_output_1(self, u_hat, x1, x2):
        return (self.Out_W_xi_tilde @ x1 \
                + self.Out_V_xi_tilde @ x2 \
                + self.Out_W_u_hat @ (u_hat.T)).T # ts * out
                
    def forward_pass(self, timesteps, a_1, a_2, h, inputs):
        # Input
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        y_dnn = np.zeros((len(timesteps), self.ssm.dim[2])) # timesteps, neurons in the output layer

        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first
                # Set the forcing term of the ODE
                f = self.compute_f(i, u_hat[t-1], u_hat_prime[t-1])
                
                # Set initial conditions of the ODE
                y_old = sol_dnn[t-1, i, 0]
                z_old = sol_dnn[t-1, i, 1]
                IC = [y_old, z_old]
                
                # Solve the ODE corresponding to current neuron at current time-step
                sol_dnn[t, i, 0], sol_dnn[t, i, 1] = self.solve_ODE_manual(a_1, a_2, i, h, f, IC)               

            # Update state and it's time-derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            
            y_dnn[t, :] = self.compute_output(u_hat[t])
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2     

    def set_forcing_term_function_handle_timesteps(self, u_interp):
        def xi_prime(t, hidden_states, i, tau_1, tau_2, W_u_hat, \
            V_u_hat, u_old, du_dt_old, t_old, W_xi_tilde, V_xi_tilde, \
            interpolants_neurons, d_A):
            
            xi = hidden_states[0]
            xi_prime = hidden_states[1]
            if u_interp == 'piecewise_constant':
                u = u_old
                dynamic_term = 0.
            elif u_interp == 'piecewise_linear':
                u = u_old + du_dt_old * (t - t_old)
                dynamic_term = V_u_hat @ du_dt_old.T 
            elif u_interp == 'sample': 
                #u = u_old(np.array([t]), np.array([i]))
                u = u_old(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                du_dt_old = du_dt_old(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                dynamic_term = V_u_hat @ du_dt_old.T

            else:
                raise ValueError("Please check the input function interpolation type!")
                
            # Compute static term 
            static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                            
 
            # Jump term
            if i != (d_A - 1):
                c = 0
                for j in range(i+1, d_A):
                    static_term += W_xi_tilde[c] * interpolants_neurons[j].sol(t)[0]
                    dynamic_term += V_xi_tilde[c] * interpolants_neurons[j].sol(t)[1]
                    c += 1
                        
            f = static_term + dynamic_term
            x2 = (-1./tau_2 * xi - (tau_1/tau_2) * xi_prime + (1./(tau_2)) * f).item()
            return [xi_prime,  x2]   
        return xi_prime
    
    def forward_pass_outer_loop_timesteps(self, timesteps, inputs, \
        forward_pass_params: forward_pass_parameters):
        # This is for piecewise constant input
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        u_interp = forward_pass_params.input_interpolation
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        sol_dnn[0, :, 0]  = self.xi[0::2] 
        self.xi_prime = (self.ssm.A @ self.xi).reshape(-1,)  + self.ssm.B @ u_hat[0].T
        sol_dnn[0, :, 1]  = self.xi_prime[0::2] #[0::2] #(self.ssm.A @ self.xi).reshape(-1,)  + self.ssm.B @ u_hat[0].T

        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        y_dnn[0, :] = self.ssm.C @ self.xi  #+ self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        
        # Update self.xi
        self.xi = sol_dnn[0, :, 0]
        self.xi_prime = sol_dnn[0, :, 1]
        # List of interpolants for computing intermediate values of states and state primes
        interpolants_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
        dx_dt = self.set_forcing_term_function_handle_timesteps(u_interp)
        
        
        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first                
                # Set initial conditions of the ODE
                IC = [self.xi[i], self.xi_prime[i]]
                # IC = [sol_dnn[t, i, 0], sol_dnn[t, i, 1]]
                if u_interp != 'sample':
                    u_old = u_hat[t-1]
                    u_new = u_hat[t]
                    du_dt_old = u_hat_prime[t-1]
                else:
                    u_old = u_hat
                    du_dt_old = u_hat_prime # Not required
                    u_new = u_hat(np.reshape(np.array([timesteps[t]]),(1, 1)), np.reshape(np.arange(0, len(self.W_u_hat[i]), 1), (1, len(self.W_u_hat[i])))) 
                    #u_new = u_hat(np.array([timesteps[t]]), np.array([i]))

                f_rhs_args = (i, self.tau_1[i], self.tau_2[i], 
                                self.W_u_hat[i], self.V_u_hat[i], u_old, du_dt_old,
                                timesteps[t-1], self.extract_W_xi_tilde(i),
                                self.extract_V_xi_tilde(i),
                                interpolants_neurons,
                                self.d_A)                                      
                # Compute the output as an interpolation function    
                interpolant = solve_ivp(dx_dt, [timesteps[t-1], timesteps[t]], IC, 
                                args= f_rhs_args, 
                                dense_output=True,
                                rtol=forward_pass_params.ode_solver_params.rtol,
                                atol=forward_pass_params.ode_solver_params.atol,
                                t_eval=np.array([timesteps[t-1], timesteps[t]])
                                ,method='DOP853')#, method='LSODA'
                self.rhs_evaluations[i] += interpolant.nfev
                # Update the state (Required for computing the forcing term)
                sol_dnn[t, i, 0] = interpolant.y[0, -1] 
                sol_dnn[t, i, 1] = interpolant.y[1, -1]
                interpolants_neurons[i] = interpolant
                                
                if u_interp == 'piecewise_constant':
                    # Contribution from the jump
                    sol_jump = (1./(self.tau_2[i])) * self.V_u_hat[i] @ (u_hat[t] - u_hat[t-1] )
                    sol_dnn[t, i, 1] += sol_jump
                    

            # Update state and it's time-derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            
            y_dnn[t, :] = self.compute_output(u_new.T)
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2
 
 
    def forward_pass_outer_loop_neurons(self, timesteps, inputs, \
        forward_pass_params: forward_pass_parameters):
        # Fetch u, du_dt
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        u_interp = forward_pass_params.input_interpolation
        
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        sol_dnn[0, :, 0]  = self.xi[0::2] 
        self.xi_prime = (self.ssm.A @ self.xi).reshape(-1,)  + self.ssm.B @ u_hat[0].T
        sol_dnn[0, :, 1]  = self.xi_prime[0::2] #[0::2] #(self.ssm.A @ self.xi).reshape(-1,)  + self.ssm.B @ u_hat[0].T

        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        y_dnn[0, :] = self.ssm.C @ self.xi  #+ self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        
        # Update self.xi
        self.xi = sol_dnn[0, :, 0]
        self.xi_prime = sol_dnn[0, :, 1]

        """
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron

        sol_dnn[0, :, 0]  = self.xi[0::2]
        sol_dnn[0, :, 1]  = self.xi[1::2] #self.ssm.A @ self.xi + self.ssm.B @ u_hat[0].T
        self.xi_prime = self.xi[1::2] #self.ssm.A @ self.xi + self.ssm.B @ u_hat[0].T

        # Initialize solution array
        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        y_dnn[0, :] = self.ssm.C @ self.xi  + self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        """
        # List of interpolants for computing intermediate values of states and state primes
        interpolants_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
        
        # Function handle for computing right hand side for the ODE of each neuron: d(xi_i)/dt
        dx_dt = self.set_forcing_term_function_handle_neuron(u_interp)
                    
        # Loop over neurons   
        for i in reversed(range(self.d_A)): # Last row first                
            # Set initial conditions
            y_old = sol_dnn[0, i, 0]
            z_old = sol_dnn[0, i, 1]
            initial_conds = [y_old, z_old]
            
            # Step size   
            dt = timesteps[1] - timesteps[0]
            
            f_rhs_args = (i, self.tau_1[i], self.tau_2[i],
                            self.W_u_hat[i], self.V_u_hat[i],
                            u_hat, u_hat_prime,
                            self.extract_W_xi_tilde(i),
                            self.extract_V_xi_tilde(i),
                            interpolants_neurons, self.d_A,
                            timesteps, dt)
            
            # Solve the ODE with dense output
            interpolant = solve_ivp(dx_dt, # rhs
                        [timesteps[0], timesteps[-1]], # Initial and final time-steps
                            initial_conds, # Initial conditions
                            args=f_rhs_args, 
                            dense_output=True,
                            #max_step = dt/100,
                            #t_eval=timesteps,
                            rtol=forward_pass_params.ode_solver_params.rtol,
                            atol=forward_pass_params.ode_solver_params.atol,
                            method='DOP853')#, method='LSODA'
                
            self.rhs_evaluations[i] += interpolant.nfev
            # Print the number of points chosen for performing integration
            interpolants_neurons[i] = interpolant
            #num_time_points = len(interpolant.t)
            #print('neuron ', i, ' # of time points for integration: ', num_time_points)

            # Interpolate the solution to the required time-points
            sol_dnn[:, i, 0] = interpolant.sol(timesteps)[0, :]
            sol_dnn[:, i, 1] = interpolant.sol(timesteps)[1, :]

            if u_interp == 'piecewise_constant':
                sol_dnn = self.update_states_input_jumps(timesteps, u_hat, sol_dnn) # Integrate the delta distributions 
        # End for loop #
        
        # Compute output
        x1 = np.reshape(sol_dnn[:, :, 0], (len(timesteps), self.d_A))
        x2 = np.reshape(sol_dnn[:, :, 1], (len(timesteps), self.d_A))
        if u_interp == 'sample':
            t = timesteps
            i = np.arange(0, self.ssm.dim[1], 1)
            u_hat = u_hat(np.reshape(t, (len(t), 1)), np.reshape(i, (1, len(i))))
            print(u_hat)    
        y_dnn = self.compute_output_1(u_hat, x1.T, x2.T)
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2
    
    
    def update_states_input_jumps(self, timesteps, u_hat, sol_dnn):                
        # Define the function handle to compute the rhs of the ODE
        def xi_prime_delta(t, x, tau_1, tau_2):
            return [x[1], (-1./tau_2) * x[0] - (tau_1/tau_2) * x[1]]
            
        # Integrating the impulses caused by jumps in the input
        x_0 = 0.
        x_0_prime = 0.
                
        for t in range(1, len(timesteps)):
            init_conds = [x_0, x_0_prime]
                    
            sol = solve_ivp(xi_prime_delta, # rhs
                    [timesteps[t-1], timesteps[t]], # Initial and final time-steps
                        init_conds, # Initial conditions
                        args=(self.tau_1[i], 
                            self.tau_2[i]), 
                        #max_step = dt/100,
                        t_eval=np.array([timesteps[t-1], timesteps[t]]),
                        rtol=1e-12, atol=1e-12,
                        method='DOP853')#, method='LSODA'
                    
            sol_jump = (1./self.tau_2[i]) * self.V_u_hat[i] @ (u_hat[t] - u_hat[t-1])
            x_0 = sol.y[0, -1]
            x_0_prime = sol.y[1, -1] + sol_jump
                    
            # Add contibution of the impulse term to the state and state prime 
            sol_dnn[t, i, 0] += x_0
            sol_dnn[t, i, 1] += x_0_prime
            return sol_dnn
    
    
    def set_forcing_term_function_handle_neuron(self, u_interp):
        def xi_prime(t, hidden_states, i, tau_1, tau_2, W_u_hat, V_u_hat, u_hat, u_hat_prime,
                W_xi_tilde, V_xi_tilde, interpolants_neurons, d_A, timesteps, dt):
                           
            # Extract x and dx_dt
            x = hidden_states[0]
            dx_dt = hidden_states[1]     
        
            if u_interp == 'piecewise_constant':  
                # Find the index of the time-step which has the value <= t 
                ind = int(t/dt)
                if t > 0 and (t / dt).is_integer(): ind -= 1
                        
                # Compute u(t)
                u = u_hat[ind, :]
        
                static_term = W_u_hat @ u_hat[ind,:].T # (1, m) * (m, 1)
                dynamic_term = 0.                
                
            elif u_interp == 'piecewise_linear':
                # Find the index of the time-step which has the value <= t  
                ind = int(t/dt)
                if t > 0 and (t / dt).is_integer(): ind -= 1
                u = u_hat[ind, :] + u_hat_prime[ind, :] * (t - timesteps[ind])
                        
                # Compute f 
                static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                dynamic_term = V_u_hat @ u_hat_prime[ind, :].T              

            
            elif u_interp == 'sample': 
                u = u_hat(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                du_dt = u_hat_prime(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                #dynamic_term = 0.
                dynamic_term = V_u_hat @ du_dt.T              
                #raise ValueError("Not implemented yet!")
            else:
                raise ValueError("Please check the input function interpolation type!")
                
            # Connections from previous layers
            if i != (d_A - 1):
                c = 0
                for j in range(i+1, d_A):
                    static_term += W_xi_tilde[c] * interpolants_neurons[j].sol(t)[0]
                    dynamic_term += V_xi_tilde[c] * interpolants_neurons[j].sol(t)[1]
                    c += 1

            f = static_term + dynamic_term
            x1 = dx_dt
            x2 = (-1./tau_2 * x - (tau_1/tau_2) * dx_dt + (1./tau_2) * f).item()
            return [x1, x2]              
        return xi_prime
    
    def print_trainable_parameters(self):
        print("\nCoefficients of x''(t) and x'(t):\n", 
              '\nTau_2 = ', self.tau_2, ', shape= ', np.shape(self.tau_2),
              '\nTau_1 = ', self.tau_1,', shape= ', np.shape(self.tau_1),
              '\n\nStatic weights:',
              '\nW_xi_tilde = \n', self.W_xi_tilde, ', shape= ', np.shape(self.W_xi_tilde), 
              '\nW_u_hat = \n', self.W_u_hat, ' shape= ', np.shape(self.W_u_hat), 
              '\n\nDynamic weights:',
              '\nV_xi_tilde = \n', self.V_xi_tilde, ' shape= ', np.shape(self.V_xi_tilde),
              '\nV_u_hat = \n', self.V_u_hat, '\n', ' shape= ', np.shape(self.V_u_hat),
              )
        
        
    def compute_coefficients(self):
        self.compute_c_xi() # 
        self.compute_c_xi_prime() # 
        self.compute_c_u_hat() # 
        self.compute_alpha() # 
        self.compute_beta() # 
        self.compute_gamma() # 
        self.compute_mu() # 
        self.compute_nu() # 
        self.compute_kappa()         


    def set_hidden_layer_weights(self):
        self.compute_tau_2() # 
        self.compute_tau_1() # 
        
        self.set_connection_weights_indices()
        self.initialize_W_xi_tilde() # 
        self.initialize_V_xi_tilde() # 
        
        self.compute_W_u_hat() # 
        self.compute_V_u_hat() # 
        

    def set_output_layer_weights(self):
        self.compute_Out_W_xi_tilde() # 
        self.compute_Out_V_xi_tilde() # 
        self.compute_Out_W_u_hat() # 


    def compute_weights(self):
        # Compute all the coefficients required for the mapping from the SSM --> Weights of DNN
        self.compute_coefficients()

        # Initialize weights of the hidden layers
        self.set_hidden_layer_weights() # 

        # Initialize weights of the output layer
        self.set_output_layer_weights() #
        

class hidden_block_real_complex:
    def __init__(self, ssm: state_space_model, n_rc, xi_0):
        self.block_type = 'real_complex'
        self.ssm = ssm  # State-space model
        self.k_r = n_rc[0]
        self.k_c = n_rc[1]
        self.d_A = self.k_r + self.k_c  
        
        # State and first time-derivative of state
        self.xi = xi_0 # Initialized with zeros
        # self.xi = np.zeros(self.d_A) # Initialized with zeros
        self.xi_prime = np.zeros(self.d_A)

        # Create a block of complex repeated eigenvalues      
        A_c = self.ssm.A[self.k_r::, self.k_r::]
        B_c = self.ssm.B[self.k_r::, :]
        C_c = self.ssm.C[:, self.k_r::]
        D_c = self.ssm.D        
        self.ssm_c = state_space_model(A_c, B_c, C_c, D_c)
        if self.k_c > 0:
            self.complex_block = hidden_block_complex_repeated(self.ssm_c, xi_0[self.k_r::])
               
        # Weights/Trainable parameters
        self.tau_1 = np.zeros(self.d_A) 
        self.tau_2 = np.zeros(self.d_A)                  
        self.W_xi_tilde = np.zeros(int((self.d_A - 1) * self.d_A / 2))
        self.W_u_hat = np.zeros((self.d_A, self.ssm.dim[1])) 
        self.V_xi_tilde = np.zeros(int((self.d_A - 1) * self.d_A / 2))
        self.V_u_hat = np.zeros((self.d_A, self.ssm.dim[1])) 

        self.theta = np.zeros(self.d_A)

        # Weights of output layer
        self.Out_W_xi_tilde = np.zeros((self.ssm.dim[2], self.d_A))
        self.Out_W_u_hat = np.zeros((self.ssm.dim[2], self.ssm.dim[1] ))
        self.Out_V_xi_tilde = np.zeros((self.ssm.dim[2], self.d_A))   

        # Eigenvalues of the state matrix and it's inverse
        self.evals = None
        self.evals_inv = None
        
        # evaluations of rhs for each neuron ODE solver
        self.rhs_evaluations = np.zeros(self.d_A)
        

        # Some variables in the mapping
        #self.c_xi = []
        self.c_xi_prime = []
        self.c_u_hat = []
        self.mu_c = []
        self.nu_c = []
        self.kappa_c = []

        for i in range(self.k_r):
            #self.c_xi.append([])
            self.mu_c.append([])
            self.nu_c.append([])
            self.kappa_c.append([])


        #self.c_xi = np.zeros(int(self.d_A * (self.d_A + 1) / 2))
        self.si = np.empty(self.d_A - 1, dtype=int)   # si: starting index for neuron 'i'

    
    def set_connection_weights_indices(self):
        """Sets starting indices of the w_xi_tilde for each neuron. i.e each neuron with 
        index 'i' has d_A - (i + 1) connections, one with respect to each of the 
        previous d_A - (i + 1) neurons in the current dense block. 
        
        The weights are multiplied with the respective states of the neurons in the 
        same dense block, but in the previous layers. 
        
        Such weights corresponding to each neuron in one dense block are stacked in the 
        vector w_xi_tilde. Starting index indicates where the weights curresponding to 
        any neuron 'i' start. 
        
        Note that the last neuron 'd_A - 1' in any dense block is not connected to 
        any other neuron- Thus, len(starting_indices) = d_A - 1.
        
        - Required for functions initialize_W_xi_tilde, compute_f
        """
        for i in range(self.d_A - 1):   # Iterate over all neurons in the block except the last
            if (i == 0):
                self.si[i] = 0
            else:
                self.si[i] = self.si[i - 1] + self.d_A - i                   

    def i(self, b: int):
        """
        Returns the index of the starting entry for block 'b' of the 2D array 

        Args:
            b (int): block number of the matrix

        Returns:
            int: index of the starting entry for block 'b' of the 2D array
        """
        return 2 * (b - 1) 
       

    def compute_tau_2(self):
        for p in range(1, self.k_r + 1):
            self.tau_2[p-1] = 0.
        
        self.tau_2[self.k_r::] = self.complex_block.tau_2            
                     
    def compute_tau_1(self):
        for p in range(1, self.k_r + 1):
            self.tau_1[p-1] = -1./self.ssm.A[p-1, p-1]
        
        self.tau_1[self.k_r::] = self.complex_block.tau_1    
    
    def compute_mu_c(self):
        for p in range(1, self.k_r + 1):
            mu_c_p = np.zeros(((self.k_c)))
            for q in range (self.k_c):
                mu_c_p[q] += self.ssm.A[p-1, self.k_r + 2 * q] 
                for z in range(q + 1):
                    mu_c_p[q] += self.ssm.A[p-1, self.k_r + 2 * z + 1] * self.complex_block.c_xi[z][0][q-z]
                    
            self.mu_c[p-1].append(mu_c_p)
        
    def compute_nu_c(self):
        for p in range(1, self.k_r + 1):
            nu_c_p = np.zeros(((self.k_c)))
            for q in range (self.k_c):
                for z in range(q + 1):
                    nu_c_p[q] += self.ssm.A[p-1, self.k_r + 2 * z + 1] * self.complex_block.c_xi_prime[z][0][q-z]
                    
            self.nu_c[p-1].append(nu_c_p)
        
    def compute_kappa_c(self):
        for p in range(1, self.k_r + 1):
            kappa_c_p = self.ssm.B[p - 1,:] #np.zeros((self.ssm.dim[1])) 
            for i in range (self.k_c):
                kappa_c_p += self.ssm.A[p-1, self.k_r + 2 * i + 1] * self.complex_block.c_u_hat[i][0]
                    
            self.kappa_c[p-1].append(kappa_c_p)
        

    def initialize_W_xi_tilde(self):
        c = 0
        for p in range(self.k_r):
            for r in range(1, self.k_r - p + self.k_c):
                if (r < self.k_r - p):
                    self.W_xi_tilde[c] = \
                    (-1./self.ssm.A[p, p]) * self.ssm.A[p, p+r]
                else:
                    self.W_xi_tilde[c] = \
                    (-1./self.ssm.A[p, p]) * self.mu_c[p][0][r - (self.k_r - p)]   
                c += 1
        
        # Copy the remaining weights from the complex block
        n = len(self.complex_block.W_xi_tilde)
        if n > 0:
            self.W_xi_tilde[-n:] = self.complex_block.W_xi_tilde

    def initialize_V_xi_tilde(self):
        c = 0
        for p in range(self.k_r):
            for r in range(1, self.k_r - p + self.k_c):
                if (r < self.k_r - p):
                    self.V_xi_tilde[c] = 0.
                else:
                    self.V_xi_tilde[c] = \
                    (-1./self.ssm.A[p, p]) * self.nu_c[p][0][r - (self.k_r - p)]   
                c += 1
        
        # Copy the remaining weights from the complex block
        n = len(self.complex_block.V_xi_tilde)
        if n > 0:
            self.V_xi_tilde[-n:] = self.complex_block.V_xi_tilde
                           
        
    
    def compute_W_u_hat(self):
        for p in range(self.k_r):
            self.W_u_hat[p] = -1./self.ssm.A[p, p] * (self.kappa_c[p][0])

        self.W_u_hat[self.k_r:] = self.complex_block.W_u_hat

    def compute_V_u_hat(self):
        # Copy the non-zero values from the complex block  
        self.V_u_hat[self.k_r:] = self.complex_block.V_u_hat      
    
    def compute_Out_W_xi_tilde(self):
        for p in range(self.k_r):
            self.Out_W_xi_tilde[:, p] = self.ssm.C[:, p]
        
        self.Out_W_xi_tilde[:, self.k_r:] = self.complex_block.Out_W_xi_tilde

    def compute_Out_V_xi_tilde(self):
        self.Out_V_xi_tilde[:, self.k_r:] = self.complex_block.Out_V_xi_tilde        
    
    def compute_Out_W_u_hat(self):
        self.Out_W_u_hat = self.complex_block.Out_W_u_hat
    
    def compute_weights(self):
        # Compute weights for the complex block using the mapping SSM --> Weights of DyNN
        self.complex_block.compute_weights()
        
        # Compute weights of the real block and copy weights from the complex block
        self.compute_coefficients()
        self.set_hidden_layer_weights()
        self.set_output_layer_weights()

    
  
    def compute_coefficients(self):
        self.compute_mu_c()
        self.compute_nu_c()
        self.compute_kappa_c()
         
    def compute_f(self, i, u_hat, u_hat_prime):
        if i == self.d_A - 1:
            static_term = np.matmul(self.W_u_hat[i], u_hat) # (1, m) * (m, 1)
            dynamic_term = np.matmul(self.V_u_hat[i], u_hat_prime) # (1, m) * (m, 1)
        else:
            n_weights = self.d_A - (i + 1)
            static_term = (np.matmul(self.W_u_hat[i], u_hat)  
                           + np.matmul(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], 
                                       self.xi[i+1:]))
            dynamic_term = (np.matmul(self.V_u_hat[i], u_hat_prime)  
                           + np.matmul(self.V_xi_tilde[self.si[i]:self.si[i] + n_weights], 
                                       self.xi_prime[i+1:]))
        f = static_term + dynamic_term
        return f

    
    def solve_ODE_manual(self, a_1, a_2, i, h, f, IC):
        h_inv = 1./h
        y_old = IC[0]
        z_old = IC[1]
        
        # Compute an intermediate term to re-use
        term_1 = a_1 * self.tau_1[i] * h_inv + self.tau_2[i] * h_inv * h_inv
                
        # Compute new solution: y_new 
        y_new = (a_1 * a_1 * f + a_1 * a_2 * f + (-a_1 * a_2 + term_1) * y_old
                        + (a_1 + a_2) * h_inv * self.tau_2[i] * z_old) / (a_1 * a_1 + term_1)
        
        # Compute new first order time-derivative: z_new      
        z_new = (y_new - y_old)/ (h * a_1) - (a_2 / a_1) * z_old
                
        return y_new, z_new
    
    
    def compute_output(self, u_hat):
        output = np.matmul(self.Out_W_xi_tilde, self.xi) + \
            np.matmul(self.Out_V_xi_tilde, self.xi_prime) + \
                np.matmul(self.Out_W_u_hat, u_hat) #np.matmul(self.Out_W_u_hat, self.u_hat[t]
        return output

    def compute_output_timesteps(self, u_hat):
        return (self.Out_W_xi_tilde @ self.xi + self.Out_V_xi_tilde @ self.xi_prime \
            + (self.Out_W_u_hat @ u_hat).T)
                
    def forward_pass(self, timesteps, a_1, a_2, h, inputs):
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
            
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        y_dnn = np.zeros((len(timesteps), self.ssm.dim[2])) # timesteps, neurons in the output layer

        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first
                # Solve the ODE and store the state for each time step and update it for next time-steps 
                #TODO: Group the following as one function: i.e y_new , z_new = odesolve(a_1, a_2, f)
        
                # Set the forcing term of the ODE
                f = self.compute_f(i, u_hat[t-1], u_hat_prime[t-1])
                
                # Set initial conditions of the ODE
                y_old = sol_dnn[t-1, i, 0]
                z_old = sol_dnn[t-1, i, 1]
                IC = [y_old, z_old]
                
                # Solve the ODE corresponding to current neuron at current time-step
                sol_dnn[t, i, 0], sol_dnn[t, i, 1] = self.solve_ODE_manual(a_1, a_2, i, h, f, IC)               

            # Update state and it's time-derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            y_dnn[t, :] = self.compute_output(u_hat[t])
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2     


    def forward_pass_scipy(self, timesteps, h, inputs):
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        h_inv = 1./h
            
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer

        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first        
                # Set the forcing term of the ODE
                f = self.compute_f(i, u_hat[t-1], u_hat_prime[t-1])

                # Assuming piecewise constant f here!
                def xi_prime(t, y, tau_1, f, tau_2):
                    if (np.abs(tau_2) < 1e-14):
                        return 1./tau_1 * (-y + f)
                    else:
                        return [y[1], -1./tau_2 * y[0] - (tau_1/tau_2) * y[1] + (1./(tau_2) * f) ]   
                
                # Set initial conditions of the ODE
                y_old = self.xi[i]
                z_old = self.xi_prime[i]                
                IC = [y_old] if i < self.d_A - self.k_c else [y_old, z_old]
                
                sol = solve_ivp(xi_prime, [timesteps[t-1], timesteps[t]], IC, 
                        args= (self.tau_1[i], f, self.tau_2[i]), 
                        t_eval=np.array([timesteps[t-1], timesteps[t]])
                        , method='DOP853')#, method='LSODA'
                sol_dnn[t, i, 0] = sol.y[0, -1]  
                sol_dnn[t, i, 1] = 0. if i < self.d_A - self.k_c else sol.y[1, -1]                 
            
            # Update state and it's time-derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            y_dnn[t, :] = self.compute_output_timesteps(u_hat[t])
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2     

    
    def compute_rhs_ode(self, u_interp):
        def xi_prime(t, hidden_states, i, tau_1, tau_2, W_u_hat, V_u_hat, u_hat, u_hat_prime,
                W_xi_tilde, V_xi_tilde, interpolants_neurons, d_A, timesteps, k_r):
                           
            # Extract x and dx_dt
            x = hidden_states[0]
            
            # Compute step-size
            dt = timesteps[1] - timesteps[0]
            
            if i >= k_r:
                dx_dt = hidden_states[1]     
        
            if u_interp == 'piecewise_constant':  
                # Find the index of the time-step which has the value <= t 
                ind = int(t/dt)
                if t > 0 and (t / dt).is_integer(): ind -= 1
                        
                # Compute u(t)
                u = u_hat[ind, :]
        
                static_term = W_u_hat @ u_hat[ind,:].T # (1, m) * (m, 1)
                dynamic_term = 0.                
                
            elif u_interp == 'piecewise_linear':
                # Find the index of the time-step which has the value <= t  
                ind = int(t/dt)
                if t > 0 and (t / dt).is_integer(): ind -= 1
                u = u_hat[ind, :] + u_hat_prime[ind, :] * (t - timesteps[ind])
                        
                # Compute f 
                static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                dynamic_term = V_u_hat @ u_hat_prime[ind, :].T              

            
            elif u_interp == 'sample': 
                u = u_hat(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                du_dt = u_hat_prime(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                dynamic_term = V_u_hat @ du_dt.T              
            else:
                raise ValueError("Please check the input function interpolation type!")
                
            # Connections from previous layers
            if i != (d_A - 1):
                c = 0
                for j in range(i+1, d_A):
                    static_term += W_xi_tilde[c] * interpolants_neurons[j].sol(t)[0]
                    if j >= k_r:
                        dynamic_term += V_xi_tilde[c] * interpolants_neurons[j].sol(t)[1]
                    c += 1

            f = static_term + dynamic_term
            
            if i < k_r:
                return 1.0/tau_1 * (f - x) 
            
            elif i >= k_r:
                x1 = dx_dt
                x2 = (-1./tau_2 * x - (tau_1/tau_2) * dx_dt + (1./tau_2) * f).item()
                return [x1, x2]
            else:
                raise ValueError('Check the eigenvalue type!')
             
        return xi_prime
    
    def compute_rhs_ode_timesteps(self, u_interp):
        def xi_prime(t, hidden_states, i, tau_1, tau_2, W_u_hat, V_u_hat, u_old, du_dt_old,
                W_xi_tilde, V_xi_tilde, interpolants_neurons, d_A, t_old, k_r):
                           
            # Extract x and dx_dt
            x = hidden_states[0]
            
            # Compute step-size
            dt = t - t_old  #timesteps[1] - timesteps[0]
            
            if i >= k_r:
                dx_dt = hidden_states[1]     
        
            if u_interp == 'piecewise_constant':
                u = u_old
                dynamic_term = 0.
            elif u_interp == 'piecewise_linear':
                u = u_old + du_dt_old * (t - t_old)
                dynamic_term = V_u_hat @ du_dt_old.T 
            elif u_interp == 'sample': 
                #u = u_old(np.array([t]), np.array([i]))
                u = u_old(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                du_dt_old = du_dt_old(np.reshape(np.array([t]),(1, 1)), np.reshape(np.arange(0, len(W_u_hat), 1), (1, len(W_u_hat)))) 
                dynamic_term = V_u_hat @ du_dt_old.T

            else:
                raise ValueError("Please check the input function interpolation type!")
            
            # Compute static term 
            static_term = W_u_hat @ u.T # (1, m) * (m, 1)
                                 
            # Connections from previous layers
            if i != (d_A - 1):
                c = 0
                for j in range(i+1, d_A):
                    static_term += W_xi_tilde[c] * interpolants_neurons[j].sol(t)[0]
                    if j >= k_r:
                        dynamic_term += V_xi_tilde[c] * interpolants_neurons[j].sol(t)[1]
                    c += 1

            f = static_term + dynamic_term
            
            if i < k_r:
                return 1.0/tau_1 * (f - x) 
            
            elif i >= k_r:
                x1 = dx_dt
                x2 = (-1./tau_2 * x - (tau_1/tau_2) * dx_dt + (1./tau_2) * f).item()
                return [x1, x2]
            else:
                raise ValueError('Check the eigenvalue type!')
             
        return xi_prime
        
    def extract_W_xi_tilde(self, i):
        if i == self.d_A - 1:
            W_xi_tilde_i = None
        else:
            n_weights = self.d_A - (i + 1)
            W_xi_tilde_i = np.reshape(self.W_xi_tilde[self.si[i]:self.si[i] + n_weights], -1)
        return W_xi_tilde_i


    def extract_V_xi_tilde(self, i):
        if i == self.d_A - 1:
            V_xi_tilde_i = None
        else:
            n_weights = self.d_A - (i + 1)
            V_xi_tilde_i = np.reshape(self.V_xi_tilde[self.si[i]:self.si[i] + n_weights], -1)
        return V_xi_tilde_i
    
    def update_states_input_jumps(self, timesteps, u_hat, sol_dnn):
        pass
    
    
    def set_initial_conditions(self, sol_dnn, i):
        if i < self.k_r:
            # Set initial conditions of the ODE
            y_old = sol_dnn[0, i, 0]
            initial_conds = [y_old]
        else:                
            # Set initial conditions of the ODE
            y_old = sol_dnn[0, i, 0]
            z_old = sol_dnn[0, i, 1]                
            initial_conds = [y_old, z_old]            
        return initial_conds
    
    def forward_pass_outer_loop_timesteps(self, timesteps, inputs, \
        forward_pass_params: forward_pass_parameters):
        # Fetch u, du_dt
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        u_interp = forward_pass_params.input_interpolation
        
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        xi_new = np.hstack((self.xi[:self.k_r], self.xi[self.k_r::2] )).reshape(-1,)  
        sol_dnn[0, :, 0] = xi_new #self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        
        xi_prime_rc  = self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        sol_dnn[0, :, 1]  = np.hstack((xi_prime_rc[:self.k_r],xi_prime_rc[self.k_r::2] )).reshape(-1,) #self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        self.xi_prime = sol_dnn[0, :, 1] # self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        y_dnn[0, :] = self.ssm.C @ self.xi  #+ self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        
        # Update self.xi
        self.xi = sol_dnn[0, :, 0]

        # List of interpolants for computing intermediate values of states and state primes
        interpolants_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
        
        # Function handle for computing right hand side for the ODE of each neuron: d(xi_i)/dt
        dx_dt = self.compute_rhs_ode_timesteps(u_interp)

        for t in range(1, len(timesteps)):
            for i in reversed(range(self.d_A)): # Last row first                
                # Set initial conditions of the ODE
                IC = [self.xi[i], self.xi_prime[i]]
                if u_interp != 'sample':
                    u_old = u_hat[t-1]
                    u_new = u_hat[t]
                    du_dt_old = u_hat_prime[t-1]
                else:
                    u_old = u_hat
                    du_dt_old = u_hat_prime # Not required
                    u_new = u_hat(np.reshape(np.array([timesteps[t]]),(1, 1)), np.reshape(np.arange(0, len(self.W_u_hat[i]), 1), (1, len(self.W_u_hat[i])))) 
                    #u_new = u_hat(np.array([timesteps[t]]), np.array([i]))
                
                # Arguments to compute the RHS of the ODE solver    
                f_rhs_args = (i, self.tau_1[i], self.tau_2[i],
                self.W_u_hat[i], self.V_u_hat[i], u_old, du_dt_old,
                self.extract_W_xi_tilde(i), self.extract_V_xi_tilde(i),
                interpolants_neurons, self.d_A, timesteps[t-1], self.k_r)
            
                # Solve the ODE with dense output
                interpolant = solve_ivp(dx_dt, # rhs
                        [timesteps[t-1], timesteps[t]], # Initial and final time-steps
                            IC, # Initial conditions
                            args=f_rhs_args, 
                            dense_output=True,
                            rtol=forward_pass_params.ode_solver_params.rtol,
                            atol=forward_pass_params.ode_solver_params.atol,
                            method='DOP853')#, method='LSODA'
                self.rhs_evaluations[i] += interpolant.nfev
                # Update the state (Required for computing the forcing term)
                sol_dnn[t, i, 0] = interpolant.y[0, -1] 
                sol_dnn[t, i, 1] = interpolant.y[1, -1]
                interpolants_neurons[i] = interpolant
                
                if u_interp == 'piecewise_constant':
                    # Contribution from the jump
                    sol_jump = (1./(self.tau_2[i])) * self.V_u_hat[i] @ (u_hat[t] - u_hat[t-1] )
                    sol_dnn[t, i, 1] += sol_jump          
             
            # Update state and it's time-derivative after each time-step 
            self.xi = sol_dnn[t, :, 0]
            self.xi_prime = sol_dnn[t, :, 1]
            
            # Compute output
            y_dnn[t, :] = self.compute_output_timesteps(u_new.T)
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2

             
             
    def forward_pass_outer_loop_neurons(self, timesteps, inputs, \
        forward_pass_params: forward_pass_parameters):
        # Fetch u, du_dt
        u_hat = inputs[0]
        u_hat_prime = inputs[1]
        u_interp = forward_pass_params.input_interpolation
        
        # Initialize the state and state-derivative set to zero
        sol_dnn = np.zeros((len(timesteps),self.d_A, 2)) # timesteps, hidden neurons, states per neuron
        xi_new = np.hstack((self.xi[:self.k_r], self.xi[self.k_r::2] )).reshape(-1,)  
        sol_dnn[0, :, 0] = xi_new #self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        
        xi_prime_rc  = self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        sol_dnn[0, :, 1]  = np.hstack((xi_prime_rc[:self.k_r],xi_prime_rc[self.k_r::2] )).reshape(-1,) #self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        self.xi_prime = sol_dnn[0, :, 1] # self.ssm.A @ self.xi  + self.ssm.B @ u_hat[0].T
        y_dnn = np.zeros((len(timesteps),self.ssm.dim[2])) # timesteps, neurons in the output layer
        y_dnn[0, :] = self.ssm.C @ self.xi  #+ self.ssm.D @ u_hat[0].T # timesteps, neurons in the output layer
        
        # Update self.xi
        self.xi = sol_dnn[0, :, 0]
        # List of interpolants for computing intermediate values of states and state primes
        interpolants_neurons = [None] * self.d_A  # Interpolants for computing states of all neurons at any time 't'
                    
        # Loop over neurons   
        for i in reversed(range(self.d_A)): # Last row first                
            initial_conds = self.set_initial_conditions(sol_dnn, i)
            
            # Arguments to compute the RHS of the ODE solver    
            f_rhs_args = (i, self.tau_1[i], self.tau_2[i],
                self.W_u_hat[i], self.V_u_hat[i], u_hat, u_hat_prime,
                self.extract_W_xi_tilde(i), self.extract_V_xi_tilde(i),
                interpolants_neurons, self.d_A, timesteps, self.k_r)
            
            # Function handle for computing right hand side for the ODE of each neuron: d(xi_i)/dt
            dx_dt = self.compute_rhs_ode(u_interp)
            
            # Solve the ODE with dense output
            interpolant = solve_ivp(dx_dt, # rhs
                        [timesteps[0], timesteps[-1]], # Initial and final time-steps
                            initial_conds, # Initial conditions
                            args=f_rhs_args, 
                            dense_output=True,
                            rtol=forward_pass_params.ode_solver_params.rtol,
                            atol=forward_pass_params.ode_solver_params.atol,
                            method='DOP853')#, method='LSODA'
            self.rhs_evaluations[i] += interpolant.nfev
            # Print the number of points chosen for performing integration
            interpolants_neurons[i] = interpolant
        
            # Interpolate solution at the required time-points    
            sol_dnn = self.interpolate_solution(i, sol_dnn, timesteps, interpolant, u_hat, u_interp)  
            
        # Compute output
        y_dnn = self.compute_output_1(sol_dnn, timesteps, u_hat, u_interp)    
        return y_dnn, sol_dnn  # tensor with dimensions timesteps * n * 2
    
        
    def interpolate_solution(self, i, sol_dnn, timesteps, interpolant, u_hat, u_interp):
        # Interpolate the solution to the required time-points
        if i < self.d_A - self.k_c:
            sol_dnn[:, i, 0] = interpolant.sol(timesteps)
            sol_dnn[:, i, 1] = 0.
        else:
            sol_dnn[:, i, 0] = interpolant.sol(timesteps)[0, :]
            sol_dnn[:, i, 1] = interpolant.sol(timesteps)[1, :]

            if u_interp == 'piecewise_constant':
                sol_dnn = self.update_states_input_jumps(timesteps, u_hat, sol_dnn) # Integrate the delta distributions 

        return sol_dnn
        
    def compute_output_1(self, sol_dnn, timesteps, u_hat, u_interp):
        x1 = np.reshape(sol_dnn[:, :, 0], (len(timesteps), self.d_A))
        x2 = np.reshape(sol_dnn[:, :, 1], (len(timesteps), self.d_A))
        if u_interp == 'sample':
            input_index = np.arange(0, self.ssm.dim[1], 1)
            u_hat = u_hat(np.reshape(timesteps, (len(timesteps), 1)), np.reshape(input_index, (1, len(input_index))))

        return (self.Out_W_xi_tilde @ x1.T \
                + self.Out_V_xi_tilde @ x2.T \
                + self.Out_W_u_hat @ (u_hat.T)).T # ts * out
           
        
    def print_trainable_parameters(self):
        print("\nCoefficients of x''(t) and x'(t):\n", 
              '\nTau_2 = ', self.tau_2, ', shape= ', np.shape(self.tau_2),
              '\nTau_1 = ', self.tau_1,', shape= ', np.shape(self.tau_1),
              '\n\nStatic weights:',
              '\nW_xi_tilde = \n', self.W_xi_tilde, ', shape= ', np.shape(self.W_xi_tilde), 
              '\nW_u_hat = \n', self.W_u_hat, ' shape= ', np.shape(self.W_u_hat), 
              '\n\nDynamic weights:',
              '\nV_xi_tilde = \n', self.V_xi_tilde, ' shape= ', np.shape(self.V_xi_tilde),
              '\nV_u_hat = \n', self.V_u_hat, '\n', ' shape= ', np.shape(self.V_u_hat),
              )


    def set_hidden_layer_weights(self):
        self.compute_tau_2() # 
        self.compute_tau_1() # 
        
        self.set_connection_weights_indices()
        self.initialize_W_xi_tilde() # 
        self.initialize_V_xi_tilde() # 
        
        self.compute_W_u_hat() # 
        self.compute_V_u_hat() # 
        

    def set_output_layer_weights(self):
        self.compute_Out_W_xi_tilde() # 
        self.compute_Out_V_xi_tilde() # 
        self.compute_Out_W_u_hat() # 



#TODO: Data types of class variables 
#TODO: Documentation
#TODO: Shift to Python 3.8 (Torch not supported on Python 3.11 yet!)
#TODO: Class decorators e.g static
#TODO: List of lists with [0] could be done much better and efficiently!
#TODO: Relative and absolute imports
#TODO: Make the interpolants as efficient as you can
#TODO: Have a base class with the forward class function and dynn blocks be a list of class objects instead of None
#TODO: Real Rep Evals: Do we really need to keep the coefficient of xi to be 1 in real_rep eigenvalues?
#TODO: In complex evals, what needs to be done for nearly zero eigenvalues? 