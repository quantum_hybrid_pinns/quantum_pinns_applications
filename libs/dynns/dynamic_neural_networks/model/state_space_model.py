import numpy as np

class state_space_model:
    A: np.array
    B: np.array
    C: np.array
    D: np.array
    evals: np.array
    evals_inv : np.array
    am : int
    
    def __init__(self, A, B, C, D) -> None:
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.dim = self.compute_dimensions()
        
    
    def compute_dimensions(self):
        return [np.shape(self.A)[0], np.shape(self.B)[1], np.shape(self.C)[0]]
