import numpy as np
from scipy.integrate import solve_ivp
from scipy import signal
from libs.dynns.dynamic_neural_networks.model.dnn import *
from libs.dynns.dynamic_neural_networks.model.state_space_model import *
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as ticker
from matplotlib.animation import FFMpegWriter, PillowWriter


def compare_pde_solutions(
    timesteps, l_x, l_y, nx, ny, y_dynn, y_lsim, fig_size, save_fig=True,
    figname = "solution_err.pdf", fontsize = 10, ticks_sol = None, ticks_err = None
):
    """
    Creates a plot at 5 time-instants at t = 0.2s, 1s, 2s, 4s, 10s with:
    (a) PDE solution using a dynamic nueral network
    (b) PDE solution using a numerical solver lsim
    (c) Absolute error between the two evaluated at grid points

    Args:
        timesteps (array): Time points
        l_x (float): Domain length in the first dimension
        l_y (float): Domain length in the second dimension
        nx (int): Number of grid points in the first dimension
        ny (int): Number of grid points in the second dimension
        y_dynn (array): solution of the dynamic neural network
        y_lsim (array): solution of the numerical solver using lsim
        fig_size (tuple): figure dimensions
        save_fig (bool, optional): Defaults to True (saves the figure as a pdf).
        fontsize (int): fontsize for labels, ticks
    """
    if l_x == l_y:
        l = l_x
        # Grid-points
        x = np.linspace(0, l, nx)
        y = np.linspace(0, l, ny)

    else:
        # Grid-points
        x = np.linspace(0, l_x, nx)
        y = np.linspace(0, l_y, ny)

    # Figure parameters
    fig = plt.figure(figsize=fig_size)
    gs = fig.add_gridspec(3, 5, hspace=0, wspace=0)
    (
        (ax1, ax2, ax3, ax4, ax5),
        (ax6, ax7, ax8, ax9, ax10),
        (ax11, ax12, ax13, ax14, ax15),
    ) = gs.subplots(sharex="col", sharey="row")
    cmap = cm.jet
    for ax in fig.get_axes():
        ax.set_xticks([])
        ax.set_yticks([])
    n_plots = 5
    fontsize = 10
    cb_min_values_dynn = np.zeros(n_plots)
    cb_min_values_num_sol = np.zeros(n_plots)
    cb_min_values_error = np.zeros(n_plots)
    cb_max_values_dynn = np.zeros(n_plots)
    cb_max_values_num_sol = np.zeros(n_plots)
    cb_max_values_error = np.zeros(n_plots)
    i = 1
    jump = len(timesteps) // n_plots

    # Make plots for five selected time-instants
    timesteps_output = [2, 10, 20, 40, 100]
    for t in timesteps_output:
        # Reshape the solution
        y_dynn_t = y_dynn[t].reshape(nx, ny)
        y_lsim_t = y_lsim[t].reshape(nx, ny)
        err_t = abs(y_dynn_t - y_lsim_t)

        if i == 1:
            contour1 = ax1.contourf(x, y, y_dynn_t, cmap=cmap, levels=100)
            contour2 = ax6.contourf(x, y, y_lsim_t, cmap=cmap, levels=100)
            contour3 = ax11.contourf(x, y, err_t, cmap=cmap, levels=100)

        elif i == 2:
            contour1 = ax2.contourf(x, y, y_dynn_t, cmap=cmap, levels=100)
            contour2 = ax7.contourf(x, y, y_lsim_t, cmap=cmap, levels=100)
            contour3 = ax12.contourf(x, y, err_t, cmap=cmap, levels=100)

        elif i == 3:
            contour1 = ax3.contourf(x, y, y_dynn_t, cmap=cmap, levels=100)
            contour2 = ax8.contourf(x, y, y_lsim_t, cmap=cmap, levels=100)
            contour3 = ax13.contourf(x, y, err_t, cmap=cmap, levels=100)

        elif i == 4:
            contour1 = ax4.contourf(x, y, y_dynn_t, cmap=cmap, levels=100)
            contour2 = ax9.contourf(x, y, y_lsim_t, cmap=cmap, levels=100)
            contour3 = ax14.contourf(x, y, err_t, cmap=cmap, levels=100)

        else:
            contour1 = ax5.contourf(x, y, y_dynn_t, cmap=cmap, levels=100)
            contour2 = ax10.contourf(x, y, y_lsim_t, cmap=cmap, levels=100)
            contour3 = ax15.contourf(x, y, err_t, cmap=cmap, levels=100)

        cb_min_values_dynn[i - 1] = contour2.cvalues.min()
        cb_min_values_num_sol[i - 1] = contour2.cvalues.min()
        cb_min_values_error[i - 1] = contour3.cvalues.min()
        cb_max_values_dynn[i - 1] = contour2.cvalues.max()
        cb_max_values_num_sol[i - 1] = contour2.cvalues.max()
        cb_max_values_error[i - 1] = contour3.cvalues.max()
        i += 1

    # Set title for each column
    ax1.set_title("t = 0.2", fontsize = fontsize)
    ax2.set_title("t = 1", fontsize = fontsize)
    ax3.set_title("t = 2", fontsize = fontsize)
    ax4.set_title("t = 4", fontsize = fontsize)
    ax5.set_title("t = 10", fontsize = fontsize)
    ax1.set_ylabel("DyNN", fontsize = fontsize)
    ax6.set_ylabel("Solver", fontsize = fontsize)
    ax11.set_ylabel("Abs Error", fontsize = fontsize)
    norm_dynn = colors.Normalize(
        vmin=cb_min_values_dynn.min(), vmax=cb_min_values_dynn.max()
    )
    norm_num_sol = colors.Normalize(
        vmin=cb_min_values_num_sol.min(), vmax=cb_min_values_num_sol.max()
    )
    norm_err = colors.Normalize(
        vmin=cb_min_values_error.min(), vmax=cb_min_values_error.max()
    )
    dynn_cb = fig.colorbar(
        cm.ScalarMappable(norm=norm_dynn, cmap=cmap),
        ax=ax5,
        location="right",
        aspect=20,
    )       
    num_sol_cb = fig.colorbar(
        cm.ScalarMappable(norm=norm_num_sol, cmap=cmap),
        ax=ax10,
        location="right",
        aspect=20,
    )
    err_cb = fig.colorbar(
        cm.ScalarMappable(norm=norm_err, cmap=cmap),
        ax=ax15,
        location="right",
        aspect=20,
        format="%.0e",
    )  # , format='%.0e', format=ticker.FuncFormatter(fmt)
    if ticks_sol is not None:
        num_sol_cb.set_ticks(ticks_sol)
        dynn_cb.set_ticks(ticks_sol)

    else:
        tick_locator = ticker.MaxNLocator(nbins=2)
        num_sol_cb.locator = tick_locator
        num_sol_cb.update_ticks()        
        dynn_cb.locator = tick_locator
        dynn_cb.update_ticks()
    
    num_sol_cb.ax.tick_params(labelsize=fontsize) 
    dynn_cb.ax.tick_params(labelsize=fontsize)

    if ticks_err is not None:
        err_cb.set_ticks(ticks_err)
    else:
        tick_locator = ticker.MaxNLocator(nbins=3)
        err_cb.locator = tick_locator
        err_cb.update_ticks()
    
    err_cb.ax.tick_params(labelsize=fontsize) 
    fig.tight_layout()
    if save_fig:
        fig.savefig(figname)

def plot_output_and_erros(timesteps, y_dynn, y_lsim, fig_size = (7,2.2), save_fig = True, fig_name='sol_err.pdf', fontsize = 10, labelpad=14, d_out=4):
    # Function for plotting outputs and absolute and relative errors of a dynamic neural network
    # Figure parameters
    plt.rcParams["text.usetex"] = False
    fig = plt.figure(figsize=fig_size,layout="constrained")
    # plt.rcParams['figure.constrained_layout.use'] = True
    gs = fig.add_gridspec(2, 4, hspace=0, wspace=0)
    (
        (ax1, ax2, ax3, ax4),
        (ax5, ax6, ax7, ax8),
    ) = gs.subplots(sharex="col", sharey="row")
    gs.update(wspace=0.02) # ,hspace=0.08 set the spacing between axes. 
    ax1.set_ylabel("Solution", labelpad=labelpad, fontsize = fontsize)
    ax5.set_ylabel("Error", fontsize = fontsize)
    ax5.set_xlabel("Time", labelpad=1, fontsize = fontsize)
    ax6.set_xlabel("Time", labelpad=1, fontsize = fontsize)
    ax7.set_xlabel("Time", labelpad=1, fontsize = fontsize)
    ax8.set_xlabel("Time", labelpad=1, fontsize = fontsize)
    ax1.tick_params(axis='y', labelsize=fontsize)
    ax5.tick_params(axis='y', labelsize=fontsize)
    ax5.tick_params(axis='x', labelsize=fontsize)
    ax6.tick_params(axis='x', labelsize=fontsize)
    ax7.tick_params(axis='x', labelsize=fontsize)
    ax8.tick_params(axis='x', labelsize=fontsize)
    lw = 2
    for k in range(d_out):
        eval(f"ax{k+1}").plot(
                timesteps,
                eval(f"y_dynn[:, k]"),
                color="#0065bd",
                linestyle="-",
                linewidth=lw,
                label="DyNN",
                markersize=12,
            )
        eval(f"ax{k+1}").plot(
                timesteps,
                eval(f"y_lsim[:, k]"),
                color="#e37222",
                linestyle="-",
                dashes=(4, 4),
                linewidth=lw,
                label="Solver",
                #label=f"$y^{{solver}}_{{{k+1}}}(t)$",
                markersize=12,
            )
        eval(f"ax{k+5}").plot(
                timesteps,
                abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]")),
                color="y",
                linewidth=2,
                label=f"Abs",
                #label=f"$e^{{abs}}_{{{k+1}}}(t)$",
                markersize=12,
            )
        eval(f"ax{k+5}").plot(
                timesteps,
                abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]"))
                / (abs(eval(f"y_lsim[:, k]")) + 1e-6),
                color="c",
                linewidth=2,
                label=f"Rel",
                #label=f"$e^{{rel}}_{{{k+1}}}(t)$",
                markersize=12,
            )
        # Set log scale for errors
        eval(f"ax{k+5}").set_yscale('log')

    # lines, labels = ax4.get_legend_handles_labels()
    ax1.yaxis.set_major_locator(ticker.MaxNLocator(3))
    ax5.yaxis.set_major_locator(ticker.LogLocator(base=10, numticks=3))
    
    #lines, labels = ax4.get_legend_handles_labels()
    lgd1 = ax4.legend(loc='upper left', bbox_to_anchor=(1, 1), ncol=1, fontsize = fontsize)
    lgd2 = ax8.legend(loc='upper left', bbox_to_anchor=(1, 1), ncol=1, fontsize = fontsize)
    #plt.legend()

    # Set title for each column
    ax1.set_title("Out (dim-1)", fontsize = fontsize)
    ax2.set_title("Out (dim-2)", fontsize = fontsize)
    ax3.set_title("Out (dim-3)", fontsize = fontsize)
    ax4.set_title("Out (dim-4)", fontsize = fontsize) 
    #fig.tight_layout()    
    if save_fig:
        plt.savefig(fig_name)#, bbox_inches='tight'

def plot_ssm(ssm:state_space_model):
    fig, axs = plt.subplots(1, 3, figsize=(9, 2.5))
    fig.suptitle(f'Transformed SSM: {i+1} clusters', fontsize=14)

    heat_map_A = axs[0].imshow(ssm.A, \
            interpolation='none', vmin=np.amin(ssm.A), vmax=np.amax(ssm.A))
    cb1 = fig.colorbar(heat_map_A, ax=axs[0],location='bottom', shrink=0.7)
    axs[0].set_title('A')

    heat_map_B = axs[1].imshow(ssm.B, \
            interpolation='none', vmin=np.amin(ssm.B), vmax=np.amax(ssm.B))
    fig.colorbar(heat_map_B, ax=axs[1], location='bottom', shrink=0.7)
    axs[1].set_title('B')
    heat_map_C = axs[2].imshow(ssm.C, \
            vmin=np.amin(ssm.C), vmax=np.amax(ssm.C))
    fig.colorbar(heat_map_C, ax=axs[2], location='bottom', shrink=0.7)
    axs[2].set_title('C')

    fig.tight_layout()

def create_simulation_video(
    timesteps,
    l_x,
    l_y,
    nx,
    ny,
    y_dynn,
    y_lsim,
    abs_err_lsim,
    fig_size=(7.0, 4.0),
    file_name="conv_diff_2D.mp4",
):
    ##############
    metadata = dict(title="Movie")
    writer = FFMpegWriter(fps=3, metadata=metadata)
    cmap = cm.jet
    fig, axs = plt.subplots(1, 3, subplot_kw={"projection": "3d"}, figsize=(10, 5))
    plt.rcParams["animation.ffmpeg_path"] = (
        "C:\\Users\\chinm\\ffmpeg-master-latest-win64-gpl\\ffmpeg-master-latest-win64-gpl\\bin\\ffmpeg.exe"
    )
    plt.rcParams["figure.dpi"] = 300

    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("2-Dimensional Heat Equation")

    sol_bounds = [np.amin(y_dynn), np.amax(y_dynn)]
    err_bounds = [0, np.amax(abs_err_lsim)]

    if l_x == l_y:
        l = l_x
        # Grid-points
        x = np.linspace(0, l, nx)
        y = np.linspace(0, l, ny)

    else:
        # Grid-points
        x = np.linspace(0, l_x, nx)
        y = np.linspace(0, l_y, ny)

    with writer.saving(fig, file_name, 100):
        # Plot the first line and cursor
        for t in range(0, len(timesteps), 1):
            y_dynn_t = y_dynn[t].reshape(nx, ny)
            y_lsim_t = y_lsim[t].reshape(nx, ny)
            err_t = abs(y_dynn_t - y_lsim_t)
            # print(err_t)

            # Clear the previous plots
            axs[0].clear()  # Clear the previous plot
            axs[1].clear()  # Clear the previous plot
            axs[2].clear()  # Clear the previous plot

            # Set titles for each subplot
            axs[0].set_title("DyNN")
            axs[1].set_title("Numerical Solver")
            axs[2].set_title("Absolute Error")

            axs[0].set_xlabel("x")
            axs[1].set_xlabel("x")
            axs[2].set_xlabel("x")

            axs[0].set_ylabel("y")
            axs[1].set_ylabel("y")
            axs[2].set_ylabel("y")

            axs[0].set_zlabel("T")
            axs[1].set_zlabel("T")
            axs[2].set_zlabel("T")

            # axs[0].set_zlim(sol_bounds)
            # axs[1].set_zlim(sol_bounds)
            # axs[2].set_zlim(err_bounds)

            # Make the surface plots
            surf1 = axs[0].plot_surface(
                x, y, y_dynn_t, cmap=cmap, linewidth=0
            )  # ,linewidth=0, antialiased=False,
            surf2 = axs[1].plot_surface(
                x, y, y_lsim_t, cmap=cmap, linewidth=0
            )  # ,linewidth=0, antialiased=False
            surf3 = axs[2].plot_surface(
                x, y, err_t, cmap=cmap, linewidth=0
            )  # .linewidth=0, antialiased=False
            cb1 = fig.colorbar(
                surf1, ax=axs[0], location="bottom", shrink=0.5
            )  # , shrink=0.3, aspect=10
            cb2 = fig.colorbar(
                surf2, ax=axs[1], location="bottom", shrink=0.5
            )  # , shrink=0.3, aspect=10
            cb3 = fig.colorbar(
                surf3, ax=axs[2], location="bottom", shrink=0.5
            )  # , shrink=0.3, aspect=10

            fig.tight_layout()
            writer.grab_frame()
            cb1.remove()
            cb2.remove()
            cb3.remove()
            plt.cla()

    print("Video created and saved!")
    return



def create_simulation_video_contour(
    timesteps,
    l_x,
    l_y,
    nx,
    ny,
    y_dynn,
    y_lsim,
    abs_err_lsim,
    fig_size=(7.0, 4.0),
    file_name="conv_diff_2D_contour.mp4",
):
    metadata = dict(title='Movie')
    writer = FFMpegWriter(fps=5, metadata=metadata)
    cmap = cm.jet
    if l_x == l_y:
        l = l_x
        # Grid-points
        x = np.linspace(0, l, nx)
        y = np.linspace(0, l, ny)

    else:
        # Grid-points
        x = np.linspace(0, l_x, nx)
        y = np.linspace(0, l_y, ny)
    
    fig, axs = plt.subplots(1, 3, figsize=(5, 1.5))#subplot_kw={"projection": "3d"}, 
    plt.rcParams['animation.ffmpeg_path'] = 'C:\\Users\\chinm\\ffmpeg-master-latest-win64-gpl\\ffmpeg-master-latest-win64-gpl\\bin\\ffmpeg.exe'
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('2-Dimensional Heat Equation')

    sol_bounds = [np.amin(y_dynn), np.amax(y_dynn)]
    err_bounds = [0, np.amax(abs_err_lsim)]

    with writer.saving(fig, file_name, 100):

        # Plot the first line and cursor
        for t in range(0, len(timesteps), 1):       
            y_dynn_t = y_dynn[t].reshape(nx, ny)
            y_lsim_t = y_lsim[t].reshape(nx, ny)
            err_t = abs(y_dynn_t - y_lsim_t)
            #print(err_t)
            
            # Clear the previous plots
            axs[0].clear()  # Clear the previous plot
            axs[1].clear()  # Clear the previous plot
            axs[2].clear()  # Clear the previous plot

            # Set titles for each subplot
            axs[0].set_title('DyNN')
            axs[1].set_title('Numerical Solver')
            axs[2].set_title('Absolute Error')

            axs[0].set_xlabel('x')
            axs[1].set_xlabel('x')
            axs[2].set_xlabel('x')

            axs[0].set_ylabel('y')
            axs[1].set_ylabel('y')
            axs[2].set_ylabel('y')

            contour1 = axs[0].contourf(x, y, y_dynn_t, cmap=cmap, levels=200)#,linewidth=0, antialiased=False, 
            contour2 = axs[1].contourf(x, y, y_lsim_t, cmap=cmap, levels=200)#,linewidth=0, antialiased=False
            contour3 = axs[2].contourf(x, y, err_t, cmap=cmap, levels=200)#.linewidth=0, antialiased=False
            
            cb1 = fig.colorbar(contour1, ax=axs[0]) #, shrink=0.3, aspect=10
            cb2 = fig.colorbar(contour2, ax=axs[1])#, shrink=0.3, aspect=10
            cb3 = fig.colorbar(contour3, ax=axs[2])#, shrink=0.3, aspect=10

            fig.tight_layout()
            writer.grab_frame()
            cb1.remove()
            cb2.remove()
            cb3.remove()
            plt.cla()


def plot_sparsity_conditioning_tradeoff(cond_nums,d_state,w_inf_norm, figsize=(2.5,2), fontsize=10):
    plt.rcParams["text.usetex"] = False
    # print(cond_nums)
    fig_1 = plt.figure(figsize=figsize)
    plt.plot(np.arange(d_state) + 1, cond_nums, color="g")  # Plot x vs. y with a blue line
    plt.xlabel("Horizontal layers", fontsize = fontsize)  # Label for the x-axis
    plt.ylabel("Condition no.", fontsize = fontsize)  # Label for the y-axis
    #plt.ylabel("$C_{tr}$", fontsize = fontsize)  # Label for the y-axis
    ticks = [2, 4, 6, 8, 10]  # Adjust the ticks based on your data
    plt.xticks(ticks, fontsize = fontsize)
    plt.yticks(ticks, fontsize = fontsize)
    
    # plt.title('')  # Title for the plot
    plt.yscale("log")
    fig_1.tight_layout()
    # fig_1.savefig('sct_1.pdf', format='pdf')
    fig_1.savefig("sct_1.jpg", dpi=600)

    #plt.rcParams["text.usetex"] = True
    fig_2 = plt.figure(figsize=figsize)
    plt.plot(
        np.arange(d_state) + 1, w_inf_norm[:, 2], color="blue"
    )  # label=r'$||w||_{\infty}$', Plot x vs. y with a blue line
    # plt.plot(np.arange(d_state) + 1, w_two_norm[:,2], label=r'$||w||_{2}$', color='blue')  # Plot x vs. y with a blue line
    plt.xlabel("Horizontal layers", fontsize = fontsize)  # Label for the x-axis
    plt.ylabel(r"$||w||_{\infty}$", fontsize = fontsize)  # $||w||$ Label for the y-axis
    plt.xticks(ticks, fontsize = fontsize)
    plt.yticks(ticks, fontsize = fontsize)
    plt.yscale("log")
    # plt.legend()  # Show legend
    fig_2.tight_layout()
    # fig_2.savefig('sct_2.pdf', format='pdf')
    fig_2.savefig("sct_2.jpg", dpi=600)
    
    # Uncomment the following to plot two norm of weights
    """
    fig_3 = plt.figure(figsize=(2.5, 2))
    # plt.plot(cond_nums, w_inf_norm[:,2], color='red')  #  label=r'$||w||_{\infty}$', Plot x vs. y with a blue line
    plt.plot(
        cond_nums, w_two_norm[:, 2], label=r"$||w||_{2}$", color="blue"
    )  # Plot x vs. y with a blue line
    plt.xlabel("$C_{tr}$")  # Label for the x-axis
    plt.ylabel(r"$||w||_{2}$")  # Label for the y-axis
    plt.xscale("log")
    plt.yscale("log")
    # plt.legend()  # Show legend
    fig_3.tight_layout()
    # fig_3.savefig('sct_3.pdf', format='pdf', dpi=1200)
    fig_3.savefig("sct_3.jpg", dpi=600)
    """

def numerical_simulation_lsim(timesteps, u, ssm, interp=False, init_cond=None):
    # Simulate the transformed LTI system
    ssm_val = signal.lti(ssm.A, ssm.B, ssm.C, ssm.D)
    t_t, y_lsim, x_t = signal.lsim(ssm_val, u, timesteps, interp=interp, X0=init_cond)
    return y_lsim


def numerical_simulation_ivp_solve(
    state_space_model, inputs, timesteps, u_interp, outer_loop, rtol=1e-8, atol=1e-8, init_cond=None
):
    if outer_loop == "neurons":
        y_solve_ivp, nfe_solver = numerical_simulation_outer_loop_neurons(
            timesteps, inputs, state_space_model, u_interp, rtol, atol, init_cond
        )
    elif outer_loop == "timesteps":
        y_solve_ivp, nfe_solver = numerical_simulation_outer_loop_timesteps(
            timesteps, inputs, state_space_model, u_interp, rtol, atol, init_cond
        )
    else:
        raise ValueError("Check the outer_loop string!")
    return y_solve_ivp, nfe_solver


def dx_dt_outer_loop_timesteps(t, x, A, B, u_old, du_dt_old, u_interp, t_st):
    # Find the index of the time-step which has the value <= t
    # Set appropriate u(t)
    if u_interp == "piecewise_constant":
        u = u_old
    elif u_interp == "piecewise_linear":
        u = u_old + (t - t_st) * du_dt_old
    elif u_interp == "sample":
        t_new = np.reshape(np.array([t]), (1, 1))
        i_new = np.reshape(np.arange(np.shape(B)[1]), (1, np.shape(B)[1]))
        u = u_old(t_new, i_new).reshape(-1)
    else:
        raise ValueError("Check the input_interpolation method!")

    # dx_dt = np.reshape(A @ x + B @ u.T, (A.shape[0], 1))
    dx_dt = A @ x + B @ u.T
    return dx_dt


def numerical_simulation_outer_loop_timesteps(
    timesteps, inputs, ssm, u_interp="piecewise_constant", rtol=1e-8, atol=1e-8, init_cond=None
):
    # Get u, du_dt
    u = inputs[0]
    du_dt = inputs[1]
    nfe_solver = 0

    # Initialize the state and state-derivative set to zero
    if init_cond is None:
        x_gt = np.zeros((ssm.dim[0]))  # 1 state per timestep
    else:
        x_gt = init_cond
    
    y_solve_ivp = np.zeros(
        (len(timesteps), ssm.dim[2])
    )  # timesteps, neurons in the output layer
    y_solve_ivp[0, :] = ssm.C @ x_gt + ssm.D @ u[0].T 
    for t in range(1, len(timesteps)):
        # Set initial conditions of the ODE
        x_old = x_gt
        if u_interp != "sample":
            u_old = u[t - 1]
            if du_dt is not None:
                du_dt_old = du_dt[t - 1, :]
            else:
                du_dt_old = None
        else:
            u_old = u
            du_dt_old = du_dt
        sol = solve_ivp(
            dx_dt_outer_loop_timesteps,
            [timesteps[t - 1], timesteps[t]],
            x_old,
            args=(ssm.A, ssm.B, u_old, du_dt_old, u_interp, timesteps[t - 1]),
            t_eval=np.array([timesteps[t - 1], timesteps[t]]),
            method="DOP853",
            rtol=rtol,
            atol=atol,
        )  # , method='LSODA'
        nfe_solver += sol.nfev

        # Update the state (Required for computing the forcing term)
        x_gt = sol.y.T[-1]  # , -1

        if u_interp == "sample":
            i = np.arange(ssm.dim[1])  # Input dimension
            u_eval = u(
                np.reshape(timesteps[t], (1, 1)), np.reshape(i, (1, len(i)))
            ).reshape(-1)
            y_solve_ivp[t, :] = (ssm.C @ x_gt + ssm.D @ u_eval.T).T
        else:
            y_solve_ivp[t, :] = (ssm.C @ x_gt + ssm.D @ u[t].T).T

    return y_solve_ivp, nfe_solver


def numerical_simulation_outer_loop_neurons(
    timesteps, inputs, ssm, u_interp="piecewise_constant", rtol=1e-8, atol=1e-8, init_cond=None
):
    # Get u, du_dt
    u = inputs[0]
    du_dt = inputs[1]
    nfe_solver = 0
    # Initialize the state to zero
    if init_cond is None:
        x_gt = np.zeros((ssm.dim[0]))  # 1 state per timestep
    else:
        x_gt = init_cond
    y_solve_ivp = np.zeros(
        (len(timesteps), ssm.dim[2])
    )  # timesteps, neurons in the output layer
    y_solve_ivp[0, :] = ssm.C @ x_gt + ssm.D @ u[0].T 
    sol = solve_ivp(
        dx_dt_outer_loop_neurons,
        [timesteps[0], timesteps[-1]],
        x_gt,
        args=(ssm.A, ssm.B, u, du_dt, u_interp, timesteps),
        t_eval=timesteps,
        method="DOP853",
        rtol=rtol,
        atol=atol,
    )  # , method='LSODA'
    nfe_solver += sol.nfev
    # Update the state (Required for computing the forcing term)
    if u_interp == "sample":
        t = timesteps
        i = np.arange(ssm.dim[1])  # Input dimension
        u_eval = u(np.reshape(t, (len(t), 1)), np.reshape(i, (1, len(i))))
        y_solve_ivp = (np.dot(ssm.C, sol.y) + np.dot(ssm.D, u_eval.T)).T
    else:
        y_solve_ivp = (np.dot(ssm.C, sol.y) + np.dot(ssm.D, u.T)).T

    return y_solve_ivp, nfe_solver


def dx_dt_outer_loop_neurons(t, x, A, B, u_hat, u_hat_prime, u_interp, timesteps):
    # Find the index of the time-step which has the value <= t
    dt = timesteps[1] - timesteps[0]
    ind = int(t / dt)
    if t > 0 and (t / dt).is_integer():
        ind -= 1
    # Set appropriate u(t)
    if u_interp == "piecewise_constant":
        u = u_hat[ind, :]
    elif u_interp == "piecewise_linear":
        u = u_hat[ind, :] + u_hat_prime[ind, :] * (t - timesteps[ind])
    elif u_interp == "sample":
        t_new = np.reshape(np.array([t]), (1, 1))
        i_new = np.reshape(np.arange(np.shape(B)[1]), (1, np.shape(B)[1]))
        u = u_hat(t_new, i_new).reshape(-1)
    else:
        raise ValueError("Check the input_interpolation method!")

    # dx_dt = np.reshape(A @ x + B @ u.T, (A.shape[0], 1))
    dx_dt = A @ x + B @ u.T
    return dx_dt


def plot_solutions_single(
    plotting_variables,
    timesteps,
    outputs,
    title=None,
    savefig=False,
    figname="solutions.pdf",
):
    output_dim, num_plots, plots_per_fig = (
        plotting_variables[0],
        plotting_variables[1],
        plotting_variables[2],
    )
    y_dynn, y_lsim, y_solve_ivp = outputs[0], outputs[1], outputs[2]
    jump = int(output_dim / num_plots)
    # Plot every 5th output
    n_output = num_plots  # l

    # Compute Rows required
    n_cols_out = num_plots  # Total columns
    Rows = 1

    c = 0
    for k in range(0, output_dim, jump):
        fig = plt.figure(figsize=(3, 1))
        plt.plot(
            timesteps,
            eval(f"y_dynn[:, k]"),
            color="#0065bd",
            linestyle="-",
            linewidth=3,
            label=f"$y^{{DyNN}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        plt.yticks([])
        plt.xticks([])
        fig.tight_layout()
        eval(f' plt.savefig("intro_fig_{k + 1}.png")')
    if savefig:
        plt.savefig(figname)


def plot_solutions(
    plotting_variables,
    timesteps,
    outputs,
    title=None,
    savefig=False,
    figname="solutions.pdf",
    figsize=(7, 2.5),
):
    output_dim, num_plots, plots_per_fig = (
        plotting_variables[0],
        plotting_variables[1],
        plotting_variables[2],
    )
    y_dynn, y_lsim, y_solve_ivp = outputs[0], outputs[1], outputs[2]
    jump = int(output_dim / num_plots)
    # Plot every 5th output
    n_output = num_plots  # l

    # Compute Rows required
    n_cols_out = num_plots  # Total columns
    Rows = 1

    c = 0
    for k in range(0, output_dim, jump):
        if c % plots_per_fig == 0:
            fig = plt.figure(figsize=figsize)  # , sharex=True, sharey=True

        if k == 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1)"
            )
            eval(f"ax_{c+1}").set_ylabel("Output")
        elif k > 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1, sharey=ax_{1})"
            )  # ,sharey=ax_{k}
            exec(f"ax_{c+1}.yaxis.set_tick_params(labelleft=False)")  # ,sharey=ax_{k}

        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_dynn[:, k]"),
            color="#0065bd",
            linestyle="-",
            linewidth=3,
            label=f"$y^{{DyNN}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_lsim[:, k]"),
            color="#e37222",
            linestyle="-",
            dashes=(3, 3),
            linewidth=3,
            label=f"$y^{{solver}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").set_xlabel("Time")
        c += 1
        # plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol = 1)#loc='best'
        plt.legend()
        fig.tight_layout()

    if savefig:
        plt.savefig(figname)


def plot_solutions_siso(
    plotting_variables,
    timesteps,
    outputs,
    title=None,
    savefig=False,
    figname="solutions.pdf",
    figsize=(7, 2.5),
):
    output_dim, num_plots, plots_per_fig = (
        plotting_variables[0],
        plotting_variables[1],
        plotting_variables[2],
    )
    y_dynn, y_lsim, y_solve_ivp = outputs[0], outputs[1], outputs[2]
    jump = int(output_dim / num_plots)
    # Plot every 5th output
    n_output = num_plots  # l

    # Compute Rows required
    n_cols_out = num_plots  # Total columns
    Rows = 1

    c = 0
    for k in range(0, output_dim, jump):
        if c % plots_per_fig == 0:
            fig = plt.figure(figsize=figsize)  # , sharex=True, sharey=True

        if k == 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1)"
            )
            eval(f"ax_{c+1}").set_ylabel("Output")
        elif k > 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1, sharey=ax_{1})"
            )  # ,sharey=ax_{k}
            exec(f"ax_{c+1}.yaxis.set_tick_params(labelleft=False)")  # ,sharey=ax_{k}

        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_dynn"),
            color="#0065bd",
            linestyle="-",
            linewidth=3,
            label=f"$y^{{DyNN}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_lsim"),
            color="#e37222",
            linestyle="-",
            dashes=(3, 3),
            linewidth=3,
            label=f"$y^{{solver}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").set_xlabel("Time")
        c += 1
        # plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol = 1)#loc='best'
        plt.legend()
        fig.tight_layout()

    if savefig:
        plt.savefig(figname)


def plot_absolute_errors(
    plotting_variables,
    timesteps,
    outputs,
    title=None,
    savefig=False,
    figname="errors.pdf",
    figsize=(7, 2.5),
):
    output_dim, num_plots, plots_per_fig = (
        plotting_variables[0],
        plotting_variables[1],
        plotting_variables[2],
    )
    y_dynn, y_lsim, y_solve_ivp = outputs[0], outputs[1], outputs[2]
    jump = int(output_dim / num_plots)
    # Plot every 5th output
    n_output = num_plots  # l

    # Compute Rows required
    n_cols_out = num_plots  # Total columns
    Rows = 1
    c = 0
    for k in range(0, output_dim, jump):
        if c % plots_per_fig == 0:
            # fig, ax = plt.subplots(figsize=(8,3), sharey='row')
            fig = plt.figure(figsize=figsize)

        if k == 0:
            exec(f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out,(c % plots_per_fig) + 1)")
            eval(f"ax_{c+1}").set_ylabel("Error")
        elif k > 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out,(c % plots_per_fig) + 1, sharey=ax_{1})"
            )  # ,sharey=ax_{k}
            exec(f"ax_{c+1}.yaxis.set_tick_params(labelleft=False)")  # ,sharey=ax_{k}

        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]")),
            color="y",
            linewidth=3,
            label=f"$e^{{abs}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]"))
            / (abs(eval(f"y_lsim[:, k]")) + 1e-6),
            color="c",
            linewidth=3,
            label=f"$e^{{rel}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").set_xlabel("Time")
        c += 1
        # plt.legend(loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol = 1)#loc='best'
        plt.legend()
        plt.yscale("log")
        fig.tight_layout()
        if savefig:
            plt.savefig(figname)


def plot_absolute_relative_errors(plotting_variables, timesteps, outputs, figsize=(10, 8), title=None):
    output_dim, num_plots, plots_per_fig = (
        plotting_variables[0],
        plotting_variables[1],
        plotting_variables[2],
    )
    y_dynn, y_lsim, y_solve_ivp = outputs[0], outputs[1], outputs[2]
    jump = int(output_dim / num_plots)
    # Plot every 5th output
    n_output = num_plots  # l
    n_cols_out = int(np.sqrt(num_plots))  # Total columns

    # Compute Rows required
    Rows = n_output // n_cols_out
    if n_output % n_cols_out != 0:
        Rows += 1

    # Create a Position index
    Position = range(1, n_output + 1)

    ax_list = []
    c = 0
    for k in range(0, output_dim, jump):
        if c % plots_per_fig == 0:
            fig = plt.figure(figsize=figsize)
            if title is not None:
                fig.suptitle("Comparison of output for dt = " + title)
            else:
                fig.suptitle("Comparison of output")

        if k == 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1)"
            )
        elif k > 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out, (c % plots_per_fig) + 1)"
            )  # ,sharey=ax_{k}

        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_dynn[:, k]"),
            color="#D41159",
            linestyle="-",
            linewidth=3,
            label=f"$y^{{DyNN}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_lsim[:, k]"),
            color="#FFC20A",
            linestyle="-",
            dashes=(3, 3),
            linewidth=3,
            label=f"$y^{{lsim}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            eval(f"y_solve_ivp[:, k]"),
            color="#0C7BDC",
            linestyle="-",
            dashes=(5, 5),
            linewidth=3,
            label=f"$y^{{solve ivp}}_{{{k+1}}}(t)$",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").set_xlabel("t")
        c += 1
        plt.legend(loc="best")
        fig.tight_layout()
    # plt.savefig("comparison.png")
    plt.show()
    ax_list = []
    c = 0
    for k in range(0, output_dim, jump):
        if c % plots_per_fig == 0:
            fig = plt.figure(figsize=figsize)
            if title is not None:
                fig.suptitle("Error for dt = " + title)
            else:
                fig.suptitle("Error")

        if k == 0:
            exec(f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out,(c % plots_per_fig) + 1)")
        elif k > 0:
            exec(
                f"ax_{c+1} = fig.add_subplot(Rows,n_cols_out,(c % plots_per_fig) + 1)"
            )  # ,sharey=ax_{k}

        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_solve_ivp[:, k]") - eval(f"y_dynn[:, k]")),
            color="#0C7BDC",
            linestyle="-",
            linewidth=3,
            label=f"Abs: Solve IVP",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]")),
            "#FFC20A",
            dashes=(3, 3),
            linewidth=3,
            label=f"Abs: lsim",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_solve_ivp[:, k]") - eval(f"y_dynn[:, k]"))
            / (abs(eval(f"y_solve_ivp[:, k]")) + 1e-6),
            color="#E66100",
            linestyle="-",
            linewidth=3,
            label=f"Rel: Solve IVP",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").plot(
            timesteps,
            abs(eval(f"y_lsim[:, k]") - eval(f"y_dynn[:, k]"))
            / (abs(eval(f"y_lsim[:, k]")) + 1e-6),
            color="#5D3A9B",
            dashes=(3, 3),
            linewidth=3,
            label=f"Rel: lsim",
            markersize=12,
        )  # dashes=(4, 3),
        eval(f"ax_{c+1}").set_xlabel("t")
        c += 1
        plt.legend(loc="best")
        plt.yscale("log")
        fig.tight_layout()


def linear_input(timesteps, m, compute_du_dt="analytical"):
    u = np.zeros((len(timesteps), m))
    du_dt = np.zeros((len(timesteps), m))
    if compute_du_dt == "analytical":
        for i in range(m):
            u[:, i] = 1.0 / 3 * timesteps
            du_dt[:, i] = 1.0 / 3
    elif compute_du_dt == "numerical":
        for t in range(1, len(timesteps) - 1):
            if t == 0:
                du_dt[t, 0] = (u[t + 1, :] - u[t, :]) / (
                    timesteps[t + 1] - timesteps[t]
                )
            else:
                du_dt[t, :] = (u[t + 1, :] - u[t - 1, :]) / (
                    timesteps[t + 1] - timesteps[t - 1]
                )
    else:
        print("Wrong value of du_dt!")
        exit(0)

    return [u, du_dt]


def sinusoidal_inputs(timesteps, m, compute_du_dt="analytical"):
    u = np.zeros((len(timesteps), m))
    du_dt = np.zeros((len(timesteps), m))
    if compute_du_dt == "analytical":
        for i in range(m):
            u[:, i] = np.sin(i * timesteps / 4.0)
            du_dt[:, i] = i / 4.0 * np.cos(i * timesteps / 4.0)
    elif compute_du_dt == "numerical":
        for t in range(1, len(timesteps) - 1):
            if t == 0:
                du_dt[t, 0] = (u[t + 1, :] - u[t, :]) / (
                    timesteps[t + 1] - timesteps[t]
                )
            else:
                du_dt[t, :] = (u[t + 1, :] - u[t - 1, :]) / (
                    timesteps[t + 1] - timesteps[t - 1]
                )
    else:
        print("Wrong value of du_dt!")
        exit(0)

    return [u, du_dt]
