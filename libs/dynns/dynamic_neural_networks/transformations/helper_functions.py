import numpy as np
import scipy.linalg as LA
import copy


# Helper functions
def compute_vec_norms(x):
    n_1 = np.linalg.norm(x, ord=1)
    n_2 = np.linalg.norm(x, ord=2)
    n_inf = np.linalg.norm(x, ord='fro')
    
    
def real_schur_decomposition(A):
    return LA.schur(A, output='real') # No sorting, random order


def block_structure(A):
    # Strategy to regroup R suitable for the next block-diagonalization step
    # Should return the regrouped matrix R and the number of block rows 'n_block_rows' 
    # and block columns 'n_block_cols'
    pass


def dim_check_block_diagonalization(Q, R, block_dims, block_row_sizes, block_col_sizes):
    if block_dims[0] != len(block_row_sizes):
        raise Exception("Mismatch in no. of block-rows of R and length of the vector specifying the sizes of the block-rows")                        
    if block_dims[1] != len(block_col_sizes):
        raise Exception("Mismatch in no. of block-cols of R and length of the vector specifying the sizes of the block-cols")                        
    if np.shape(R)[0] != np.sum(block_row_sizes):
        raise Exception("Mismatch in no. of rows of A and sum of sizes of block-rows of A")
    if np.shape(R)[1] != np.sum(block_col_sizes):
        raise Exception("Mismatch in no. of cols of A and sum of sizes of block-cols of A")
    if np.shape(R) != np.shape(Q):
        raise Exception("Mismatch in the dimensions of R and Q")

    
def block_diagonalization(Q, R, block_sizes):
    # block_dims = [no_block_rows, no_block_columns]
    # block_row_sizes = []  # block_row_sizes(i) = No. of rows in the block row 'i'
    # block_col_sizes = []  # block_col_sizes(i) = No. of cols in the block col 'i'

    
    block_dims = block_sizes[0]
    block_row_sizes = block_sizes[1]
    block_col_sizes = block_sizes[2]
    
    block_rows = block_dims[0]
    block_columns = block_dims[1]
    cum_sum_columns = np.cumsum(block_col_sizes)
    # block_col_sizes = [1, 2]    
    # block_row_sizes = [1, 2]
    
    # Strategy to regroup R suitable for the next block-diagonalization step
    c_st = 0
    c_end = 0
    for c in range(1, block_columns):  # Fix the block column index (Excluding first column)
        # Set block dimensions of the current column
        c_st = cum_sum_columns[c-1]
        c_end = cum_sum_columns[c]
        
        # Initialize block dimensions of the 0'th row
        r_st = 0
        r_end = 0
        
        for r in range(c):   # Make the block matrices 0, 1, .., c - 1 in this column zero
            r_end += block_row_sizes[r]
            Z = LA.solve_sylvester(R[r_st:r_end, r_st:r_end], -R[c_st:c_end, c_st:c_end], -R[r_st:r_end, c_st:c_end])
        
            for s in range(c + 1, block_columns):
                s_st = cum_sum_columns[s-1] #np.cumsum(block_col_sizes[0:s-1])
                s_end = s_st + block_col_sizes[s]
                R[r_st:r_end, s_st:s_end] -= np.matmul(Z, R[c_st:c_end, s_st:s_end])
            
            p_st = 0
            p_end = 0
            for p in range(block_columns):
                p_end +=  block_row_sizes[p] # np.cumsum(block_row_sizes[p])
                Q[p_st:p_end, c_st:c_end] += np.matmul(Q[p_st:p_end, r_st:r_end], Z)
                p_st += block_row_sizes[p]
        
            r_st += block_row_sizes[r]
        #c_st += block_col_sizes[c]
    return Q 
            

def block_diag_temp(R, Q, eps):
    # Input: Q,R from real schur decomposition 
    # Output: Q := QY s.t (QY)^{-1} A (QY) = T (Diagonal matrix)
    if np.abs(R[0,0] - R[1,1]) > eps: # Only applicable for matrix having different evals
        z = -R[0,1] / (R[0,0] - R[1,1])
    # z = LA.solve_sylvester(R[0,0], -R[1,1], -R[0,1])
        Q[:, 1] += z * Q[:, 0]
    return Q 

def block_diag_Bartels_Stewart(R, Q):
    # Input: Q,R from real schur decomposition 
    # Output: Q := QY s.t (QY)^{-1} A (QY) = T (Diagonal matrix)
    '''
    if np.abs(R[0,0] - R[1,1]) > eps: # Only applicable for matrix having different evals
        z = -R[0,1] / (R[0,0] - R[1,1])
        Q[:, 1] += z * Q[:, 0]
    '''
    Z = LA.solve_sylvester(R[0:2,0:2], -R[2:4, 2:4], -R[0:2, 2:4])
    # Q[12] += Q[11] Z, Q[22] += Q[21] Z
    Q[0:2, 2:4] = Q[0:2, 2:4] + np.matmul(Q[0:2, 0:2], Z)
    Q[2:4, 2:4] = Q[2:4, 2:4] + np.matmul(Q[2:4, 0:2], Z)
    return Q 


def noise_11(A, p):
    A_p = copy.deepcopy(A)
    A_p[1][1] += A[1][1] * p/100.0 
    return A_p


def truncate_real(arr, threshold):
    a_trunc = np.zeros_like(arr)
    for i in range(len(arr)):
        if np.abs(arr[i].real) < threshold:
            a_trunc[i] = complex(0, arr[i].imag)
        else:
            a_trunc[i] = arr[i]
    return a_trunc


def truncate_complex(arr, threshold):
    a_trunc = np.zeros_like(arr)
    for i in range(len(arr)):
        if np.abs(arr[i].imag) < threshold:
            a_trunc[i] = complex(arr[i].real, 0)
        else:
            a_trunc[i] = arr[i]
        
    return a_trunc
# 1 function per child class: implementation of particular child strategy/function
# Define various transformations as classes


