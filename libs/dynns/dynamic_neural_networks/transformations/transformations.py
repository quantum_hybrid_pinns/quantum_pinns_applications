from abc import ABC, abstractmethod
import scipy.linalg as LA
import numpy as np

class transformations(ABC):
    @abstractmethod
    def apply_transformation():
        pass


class unitary_triangulation(transformations):
    def __init__(self, A) -> None:
        super().__init__()
        self.A = A
        
    def apply_transformation(self):
        return LA.schur(self.A, output='real')
        
        
class block_diagonalization_22(transformations):
    def __init__(self, Q, R, eps) -> None:
        super().__init__()
        self.Q = Q
        self.R = R
        self.eps = eps
    
    def apply_transformation(self):
        # Input: Q,R from real schur decomposition 
        # Output: Q := QY s.t (QY)^{-1} A (QY) = T (Diagonal matrix)
        if np.abs(self.R[0,0] - self.R[1,1]) > self.eps: # Only applicable for matrix having different evals
            z = -self.R[0,1] / (self.R[0,0] - self.R[1,1])
        # z = LA.solve_sylvester(R[0,0], -R[1,1], -R[0,1])
            self.Q[:, 1] += z * self.Q[:, 0]
        return self.Q 


class block_diagonalization_BS(transformations):
    def __init__(self, Q, R, eps) -> None:
        super().__init__()
        self.Q = Q
        self.R = R
        self.eps = eps
        
    def apply_transformation(self):
        # Input: Q,R from real schur decomposition 
        # Output: Q := QY s.t (QY)^{-1} A (QY) = T (Diagonal matrix)
        Z = LA.solve_sylvester(self.R[0:2,0:2], -self.R[2:4, 2:4], -self.R[0:2, 2:4])
        # Q[12] += Q[11] Z, Q[22] += Q[21] Z
        self.Q[0:2, 2:4] += self.Q[0:2, 2:4] + np.matmul(self.Q[0:2, 0:2], Z)
        self.Q[2:4, 2:4] += self.Q[2:4, 2:4] + np.matmul(self.Q[2:4, 0:2], Z)
        return self.Q 


class eigenvalue_decomposition(transformations):
    def __init__(self, A) -> None:
        super().__init__()
        self.A = A
        
    def apply_transformation(self):
        return LA.eig(self.A)


class inverse(transformations):
    def __init__(self, A: np.array) -> None:
        super().__init__()
        self.A = A
           
    def apply_transformation(self):
        return np.linalg.inv(self.A)
    
    
class transformed_state_matrix(transformations):
    def __init__(self, Z_inv, A, Z) -> None:
        super().__init__()
        self.Z_inv = Z_inv
        self.A = A
        self.Z = Z
        
    def apply_transformation(self):
        return np.matmul(np.matmul(self.Z_inv, self.A), self.Z)