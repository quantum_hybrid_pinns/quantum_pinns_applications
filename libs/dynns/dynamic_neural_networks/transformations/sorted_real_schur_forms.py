# Python code: taken from https://gist.github.com/fabian-paul/14679b43ed27aa25fdb8a2e8f021bad5

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from itertools import chain
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
import seaborn as sns
expensive_asserts = False

#TODO: Optimization: Use finding indices only once
#TODO: Compute the real parts of the array directly without iterating through the list
#TODO: Move the figure post-sorting
    
def sort_real_schur(Q, R, spectral_clustering, show_plots, verbose, b, inplace=False):
    r'''
    :param Q: np.array((N, N))
        orthogonal real Q such that AQ=QR
    :param R: np.array((N, N))
        quasi-triangular real R such that AQ=QR
    :param b: float
        determines the length of the ordering with respect to z to be produced
        * if b < 0 then -b blocks will be sorted,
        * if b > 0 then  b or b+1 eigenvalues will be sorted, depending on the sizes of the blocks,
        * if b = 0 then the whole Schur form will be sorted.
    :return: Q, R, ap
        * Q, R : orthogonal real Q and quasi-triangular real R such that AQ=QR with
          the diagonal blocks ordered with respect to the target z. The number of
          ordered blocks/eigenvalues is determined by the parameter b.
        * A vector ap warns for inaccuracy of the solution if an entry of ap exceeds one.
    '''
    eps = np.finfo(R.dtype).eps
    if not np.all(np.abs(np.tril(R, -2)) <= 100*eps):
        raise ValueError('R is not block-triangular')
    if not inplace:
        Q = Q.copy()
        R = R.copy()

    r = np.where(np.abs(np.diag(R, -1)) > 100*eps)[0]  # detect subdiagonal nonzero entries
    s = [i for i in range(R.shape[0] + 1) if not i in r + 1]  # construct from them a vector s with the-top left positions of each block
    if verbose > 1:
        print('Top left positions of each block :\n')
        print(s)
    p = [None]*(len(s) - 1)  # will hold the eigenvalues
    
    for k in range(1, len(s) - 1):  # debug
        assert R[s[k], s[k] - 1] <= 100*eps  # debug

    for k in range(len(s) - 1):  # ranging over all blocks
        sk = s[k]
        if s[k + 1] - sk == 2:  # if the block is 2x2
            Q, R = normalize(Q, R, slice(sk, s[k + 1]), inplace=True)  # normalize it
            p[k] = R[sk, sk] + np.lib.scimath.sqrt(R[sk + 1, sk]*R[sk, sk + 1])  # store the eigenvalues
        else:  # (the one with the positive imaginary part is sufficient)
            assert s[k + 1] - sk == 1  # debug
            p[k] = R[s[k], s[k]]  # if the block is 1x1, only store the eigenvalue

    ap = []

    # Cluster eigenvalues
    if spectral_clustering.n_clusters == len(p):
        clustered_labels = np.arange(len(p))
    else:
        clustered_labels = perform_clustering(p, spectral_clustering)
    
    # Sort the eigenvalues one cluster at a time
    sorted_evals, n_r, n_c = perform_sorting(clustered_labels, p, show_plots, c_order='max_real')

    # Compute block-sizes required for the Bartels-Stewart block diagonalization algorithm
    block_sizes = compute_block_sizes(n_r, n_c)
    
    for k in swaplist(p, sorted_evals, s, b):  # For k ranging over all neighbor-swaps
        assert k + 2 < len(s)  # debug
        v = list(range(s[k], s[k + 1]))  # collect the coordinates of the blocks
        w = list(range(s[k + 1], s[k + 2]))
        assert v[0]!=w[0]  # debug
        if len(v) == 2: assert v[0] < v[1]  # debug
        if len(w) == 2: assert w[0] < w[1]  # debug

        vw = v + w
        nrA = np.linalg.norm(R[vw, :][:, vw], ord=np.inf)  # compute norm of the matrix A from (6)
        Q, R = swap(Q, R, v, w, inplace=True)  # swap the blocks
        p[k], p[k + 1] = p[k + 1], p[k]  # debug
        s[k + 1] = s[k] + s[k + 2] - s[k + 1]  # update positions of blocks
        v = list(range(s[k], s[k + 1]))  # update block-coordinates
        w = list(range(s[k + 1], s[k + 2]))
        if len(v)==2:  # if the first block is 2 x 2
            Q, R = normalize(Q, R, v, inplace=True)  # normalize it
        if len(w)==2:  # if the second block is 2 x 2
            Q, R = normalize(Q, R, w, inplace=True)  # normalize it
        ap.append(np.linalg.norm(R[w, :][:, v], ord=np.inf) / (10*eps*nrA))  # measure size of bottom-left block (see p.6, Sect. 2.3)

    R = R - np.tril(R, -2)  # Zero the below-block entries
    for k in range(1, len(s)-1):  # to get a quasi-triangle again
        R[s[k], s[k]-1] = 0

    return Q, R, block_sizes, n_r, n_c, ap


def normalize(U, S, v, inplace=False):
    r'''Applies a Givens rotation such that the two-by-two diagonal block of S situated at diagonal positions v[0], v[1] is in standardized form.
    :param U:
    :param S:
    :param v:
    :return:
    '''
    Q = rot(S[v, :][:, v])  # Determine the Givens rotation needed for standardization -
    if not inplace:
        S = S.copy()
        U = U.copy()
    S[:, v] = np.dot(S[:, v], Q)  # and apply it left and right to S, and right to U.
    S[v, :] = np.dot(Q.T, S[v, :])  # Only rows and columns with indices in the vector v can be affected by this.
    U[:, v] = np.dot(U[:, v], Q)
    return U, S


def rot(X):
    r'''Computes a Givens rotation needed in `normalize`
    :param X:
    :return:
    '''
    c = 1.0  # Start with the identity transformation, and if needed, change it into ...
    s = 0.0
    if X[0, 0] != X[1, 1]:
        tau = (X[0, 1] + X[1, 0]) / (X[0, 0] - X[1, 1])
        off = (tau**2 + 1)**0.5
        v = [tau - off, tau + off]
        w = np.argmin(np.abs(v))
        c = 1.0/(1.0 + v[w]**2)**0.5  # ... the cosine and sine as given in Section 2.3.1
        s = v[w]*c
    Q = np.array([[c, -s], [s, c]], dtype=X.dtype)
    return Q


def perform_clustering(eigenvalues, spectral_clustering):
    eigenvalues = np.array(eigenvalues) 
    #TODO: Compute the real parts of the array directly without iterating through the list
    # TODO: Move the figure post-sorting
    data_df = pd.DataFrame({'re':eigenvalues.real, 'imag':eigenvalues.imag})
    data_df.head()
    clustered_labels = spectral_clustering.fit_predict(data_df)
    return clustered_labels # Returns cluster labels
    
    
def perform_sorting(clustered_labels, p, show_plots, c_order):
    p_orig = np.array(p)
    n_clusters = len(np.unique(clustered_labels))
    sorted_evals = []
    n_r = []
    n_c = []
    
    # Sort clusters (To get a deterministic order of eigenvalues)
    # Cluster containing the lowest eigenvalues are put first (-ve first then +ve)
    # Compute the minimum real values, min complex values in all clusters
    indices = []
    min_real_imag = np.empty([3, n_clusters]) # row 0: index, row 1 : min_real, row 2: min imag
    max_real_imag = np.empty([3, n_clusters]) # row 0: index, row 1 : min_real, row 2: min imag
    # min_imag = np.empty([n_clusters])
    cluster_order = np.empty([n_clusters])
    #TODO: Optimization: Use finding indices only once
    for i in range(n_clusters):
        indices = [idx for idx in range(len(clustered_labels)) if clustered_labels[idx] == i]
        evals_cluster_i = p_orig[indices]
        
        # Sort the evals in 
        min_real_imag[0][i] = i
        min_real_imag[1][i] = np.amin(evals_cluster_i.real)
        ind = np.argmin(evals_cluster_i.real)
        min_real_imag[2][i] = -evals_cluster_i[ind].imag
    
        max_real_imag[1][i] = np.amax(evals_cluster_i.real)
        ind_max = np.argmax(evals_cluster_i.real)
        max_real_imag[2][i] = -evals_cluster_i[ind_max].imag

    list_0 = list(zip(min_real_imag[0], min_real_imag[1], min_real_imag[2]))
    list_1 = list(zip(min_real_imag[0], max_real_imag[1], max_real_imag[2]))
    # Get the cluster order
    list_cluster_0 = sorted(list_0, \
        key=lambda item: (item[1], item[2]))
    cluster_order = [t[0] for t in list_cluster_0]
    
    # New stuff 
    list_cluster_1 = sorted(list_1, \
        key=lambda item: (-item[1], item[2]))
    cluster_order_1 = [t[0] for t in list_cluster_1]

    for i in cluster_order_1: #range(n_clusters):#spectral_clustering.n_clusters
        indices = [idx for idx in range(len(clustered_labels)) if clustered_labels[idx] == i]

        # Sorting algorithm: For clusteres with complex evals, place real evals first (if there are any)
        if any(number.imag > 0 for number in p_orig[indices]): # complex evals
            #sorted_evals_cluster = sorted(p_orig[indices], key=lambda x: np.abs(x.imag))
            sorted_evals_cluster = sorted(p_orig[indices], key=lambda x: np.abs(round(x,2).imag))
        else: # real evals
            #sorted_evals_cluster = sorted(p_orig[indices], key=lambda x: np.abs(x))
            sorted_evals_cluster = sorted(p_orig[indices], key=lambda x: -np.abs(round(x,2)))
        
        # real eigenvalues in the current cluster
        n_r.append(np.count_nonzero(np.isreal(np.array(sorted_evals_cluster))))
        n_c.append(np.count_nonzero(np.iscomplex(np.array(sorted_evals_cluster))))
              
        # Extend the list by adding the sorted eigenvalues one cluster at a time
        sorted_evals += sorted_evals_cluster
    

    re = np.array(sorted_evals).real 
    imag = np.array(sorted_evals).imag
    cluster_number = []
    for c in range(n_clusters):
        cluster_number.extend(c * np.ones(n_r[c] + n_c[c], dtype=int))
    
    cluster_number = np.array(cluster_number)
    df = pd.DataFrame(dict(Real=re, Imaginary=imag, Cluster = cluster_number))
  

    if show_plots:
        fig, axes = plt.subplots(1, 2, figsize=(7.,2.5))

        # Example data
        y_pos = np.arange(n_clusters)
        n_neurons = np.array(n_r) + np.array(n_c)
        
        my_cmap = plt.cm.get_cmap('jet')
        colors = my_cmap(y_pos)

        # Normalize values to 0-1
        norm = plt.Normalize(y_pos.min(), y_pos.max())

        # Apply the color map
        colors = my_cmap(norm(y_pos))
        bar_chart = axes[0].barh(y_pos, n_neurons, align='center', color=colors)
        axes[0].set_yticks(y_pos)#, labels=np.arange(n_clusters)
        axes[0].invert_yaxis()  # labels read top-to-bottom
        axes[0].set_ylabel('Horizontal layer')
        axes[0].set_xlabel('Depth (no. of neurons)')
        #axes[0].title.set_text('DyNN hidden layers')
        xticks = np.arange(1, np.amax(n_neurons) + 1, 1, dtype='int')
        axes[0].set_xticks(xticks)
        #sm = plt.cm.ScalarMappable(cmap=my_cmap, norm=plt.Normalize(0,max(y_pos)))
        #sm.set_array([])

        #plt.colorbar(sm)
        #fig.colorbar(ax=axes[0])
        
        # First subplot
        """
        fig, axes = plt.subplots(1, 2, figsize=(7.,2.5))
        axes[0].scatter(np.array(n_r) + np.array(n_c),np.arange(n_clusters),\
            c=np.arange(n_clusters), cmap='jet')
        axes[0].set_ylabel('Horizontal Layer')
        axes[0].set_xlabel('# Neurons')
        axes[0].title.set_text('DyNN architecture')
        """
        # Add solid lines for x and y axes
        sc = axes[1].scatter(df['Real'], df['Imaginary'],c=cluster_number, cmap='jet')
        axes[1].set_xlabel('Real')
        #axes[1].set_xscale('log')
        axes[1].set_ylabel('Imaginary')
        axes[1].title.set_text('Eigenvalues')
        
        #fig.colorbar(sc, ax=axes.ravel().tolist())
        fig.colorbar(sc, ax=axes[1])
        fig.tight_layout()
        plt.savefig('clustering.pdf')

        plt.show()
        
    return sorted_evals, n_r, n_c
        

def compute_block_sizes(n_r, n_c):
    n_clusters = len(n_r)
    block_sizes = []
    block_dims = [n_clusters, n_clusters]
    block_row_sizes = []
    block_col_sizes = []
    for i in range(n_clusters):
        block_row_sizes.append(n_r[i] + 2 * n_c[i])
        block_col_sizes.append(n_r[i] + 2 * n_c[i])

    # Creata a list with 3 lists required for the Bartels Stewart Block-Diagonalization algorithm
    block_sizes.append(block_dims)
    block_sizes.append(block_row_sizes)
    block_sizes.append(block_col_sizes)
    
    return block_sizes
    
def swaplist(p, sorted_evals, s, b):
    r'''Produces the list v of swaps of neighboring blocks needed to order the eigenvalues assembled in the vector v
     in clusters (with real eigenvalues in the top-left blocks), taking into account the parameter b.
    :param p: list
        list of eigenvalues (only one copy for each complex-conjugate pair)
    :param s: list
        list of the the-top left positions of each block
    :param b: float
        determines the length of the ordering with respect to z to be produced
        * if b < 0 then -b blocks will be sorted,
        * if b > 0 then  b or b+1 eigenvalues will be sorted, depending on the sizes of the blocks,
        * if b = 0 then the whole Schur form will be sorted.
    :return:
    '''
    p_orig = np.array(p) # debug
    n = len(p)
    p = list(p)
    k = 0
    v = []
    srtd = 0  # Number of sorted eigenvalues.
    q = list(np.diff(s))  # Compute block sizes.
    q_orig = list(q) # debug
    fini = False
    
    #################### End #########################################
    
    while not fini:
        _, j = sorted_evals[k], p[k:n].index(sorted_evals[k])
        #print("\nValue, index: ", _, j)
        
        # Sequentially select the eigenalues one-cluster at a time (Im = 0 first!)
        p_j = p[k + j] # debug
        p[k:n + 1] = [p[j + k]] + p[k:n]  # insert this block at position k,
        assert p[k]==p_j  # debug
        del p[j + k + 1]  # and remove it from where it was taken.
        if expensive_asserts and __debug__: assert np.all(sorted(p)==sorted(p_orig))  # debug
        q_j = q[k + j]  # debug
        q[k:n + 1] = [q[j + k]] + q[k:n]  # Similar for the block-sizes
        assert q[k] == q_j  # debug
        del q[j + k + 1]
        if expensive_asserts and __debug__: assert np.all(sorted(q) == sorted(q_orig)) # debug
        v = v + list(range(k, j + k))[::-1]  # Update the list of swaps for this block
        srtd = srtd + q[k]  # Update the number of sorted eigenvalues
        k += 1
        fini = (k >= n - 1 or k == -b or srtd == b or (srtd == b + 1 and b != 0))
    return v


def swap(U, S, v, w, inplace=False):
    r'''Swaps the two diagonal blocks at positions symbolized by the entries of v and w.
    :param U:
    :param S:
    :param v:
    :param w:
    :return:
    '''
    p, q = S[v, :][:, w].shape  # p and q are block sizes
    Ip = np.eye(p)
    Iq = np.eye(q)
    r = np.concatenate([S[v, w[j]] for j in range(q)])  # Vectorize right-hand side for Kronecker product formulation of the Sylvester equations (7).
    K = np.kron(Iq, S[v, :][:, v]) - np.kron(S[w, :][:, w].T, Ip)  # Kronecker product system matrix.
    L, H, P, Q = lu_complpiv(K, overwrite=True)  # LU-decomposition of this matrix.
    e = np.min(np.abs(np.diag(H)))  # Scaling factor to prevent overflow.
    sigp = np.arange(p*q)
    for k in range(p*q - 1):  # Implement permutation P of the LU-decomposition PAQ=LU ...
        sigp[[k, P[k]]] = sigp[[P[k], k]].copy()
    r = e*r[sigp]  # ... scale and permute the right-hand side.
    x = np.linalg.solve(H, np.linalg.solve(L, r))  # and solve the two triangular systems.
    sigq = np.arange(p*q)
    for k in range(p*q - 1):  # Implement permutation Q of the LU-decomposition PAQ=LU ...
        sigq[[k, Q[k]]] = sigq[[Q[k], k]].copy()
    x[sigq] = x.copy()  # ... and permute the solution.
    X = np.vstack([x[j*p:(j + 1)*p] for j in range(q)]).T  # De-vectorize the solution back to a block, or, quit Kronecker formulation.
    Q, R = np.linalg.qr(np.vstack((-X, e*Iq)), mode='complete')  # Householder QR-decomposition of X.
    vw = list(v) + list(w)
    if not inplace:
        S = S.copy()
        U = U.copy()
    S[:, vw] = np.dot(S[:, vw], Q)  # Perform the actual swap by left- and right-multiplication of S by Q,
    S[vw, :] = np.dot(Q.T, S[vw, :])
    U[:, vw] = np.dot(U[:, vw], Q)  # and, right-multiplication of U by Q
    return U, S


def lu_complpiv(A, overwrite=False):
    '''Computes the LU-decomposition of A with complete pivoting, i. e. PAQ=LU, with permutations symbolized by vectors.
    :param A:
    :return:
    '''
    if not overwrite or (__debug__ and expensive_asserts):
        A_inp = A  # debug
        A = A.copy()
    n = A.shape[0]
    P = np.zeros(n - 1, dtype=int)
    Q = np.zeros(n - 1, dtype=int)
    for k in range(n - 1):  # See Golub and Van Loan, p. 118 for comments on this LU-decomposition with complete pivoting.
        Ak = A[k:n, :][:, k:n]
        rw, cl = np.unravel_index(np.argmax(np.abs(Ak), axis=None), Ak.shape)
        rw += k
        cl += k
        A[[k, rw], :] = A[[rw, k], :].copy()
        A[:, [k, cl]] = A[:, [cl,  k]].copy()
        P[k] = rw
        Q[k] = cl
        if A[k, k] != 0:
            rs = slice(k + 1, n)
            A[rs, k] = A[rs, k] / A[k, k]
            A[rs, :][:, rs] = A[rs, :][:, rs] - A[rs, k][:, np.newaxis]*A[k, rs]
    U = np.tril(A.T).T
    L = np.tril(A, -1) + np.eye(n)
    if __debug__ and expensive_asserts:
        perm_p = np.arange(n)  # debug
        for k in range(n - 1):  # debug
            perm_p[[k, P[k]]] = perm_p[[P[k], k]].copy()  # debug
        perm_q = np.arange(n)  # debug
        for k in range(n - 1):  # debug
            perm_q[[k, Q[k]]] = perm_q[[Q[k], k]].copy()  # debug
        assert np.allclose(A_inp[perm_p, :][:, perm_q], np.dot(L, U))  # debug
    return L, U, P, Q


def data_frame_from_coordinates(coordinates): 
    """From coordinates to data frame."""
    xs = chain(*[c[0] for c in coordinates])
    ys = chain(*[c[1] for c in coordinates])

    return pd.DataFrame(data={'x': xs, 'y': ys})



if __name__=='__main__':
    import scipy
    import scipy.linalg
    expensive_asserts = True
    for m in range(1):  
        # radius of the circle
        s = 2.0
        
        # Set up a matrix A with real and complex eigenvalues ordered randomly on the diagonal
        c1_ev1_r = -1./np.sqrt(2) * s  
        c1_ev1_c = 1./np.sqrt(2) * s  
        
        c1_ev2_r = 1./np.sqrt(2) * s 
        c1_ev2_c = 1./np.sqrt(2) * s

        c1_ev3_r = 0. * s 
        c1_ev3_c = 1. * s

        c1_ev6_r = -0.2 * s 
        c1_ev6_c = 1. * s

        c1_ev4 = -1. * s
        c1_ev5 = 1. * s
        A = np.array([
            [c1_ev1_r,-c1_ev1_c, -0.8, -0.9, 0.5, 0.6,0.7, -0.3,0.7, -0.3],
            [c1_ev1_c, c1_ev1_r, -0.4, 0.3, -0.2, 0.1, 0.3, -0.24,0.7, -0.3],
            [0.   ,0.      , c1_ev4, 0.1, 0.2, -0.3, -0.2, -0.1,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_r,-c1_ev2_c, -0.5, -0.3, -0.9,0.7, -0.3],
            [0.   ,0.      , 0. , c1_ev2_c, c1_ev2_r, -0.9, -0.9, 0.9,0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     ,  c1_ev5, 0.7, 0.6,0.7, -0.3], 
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_r,-c1_ev3_c,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_c, c1_ev3_r,    0.7, -0.3],
            [0.   ,0.      , 0., 0.    ,0.     , 0.,    0.,   0.,   c1_ev6_r, -c1_ev6_c],
            [0.   ,0.      , 0., 0.    ,0.     , 0., 0.,       0.,   c1_ev6_c,  c1_ev6_r],
            ]); 

        """
        for i in range(len(s)): 
            # Complex eigenvalues
            exec(f'c{i+1}_ev1_r = -1./np.sqrt(2) * s[i]')
            exec(f'c{i+1}_ev1_c = 1./np.sqrt(2) * s[i]')
            
            exec(f'c{i+1}_ev2_r = 1./np.sqrt(2) * s[i]')
            exec(f'c{i+1}_ev2_c = 1./np.sqrt(2) * s[i]')
            
            exec(f'c{i+1}_ev3_r = 0.4 * s[i]')
            exec(f'c{i+1}_ev3_c = 1. * s[i]')
           
            # Real eigenvalues
            exec(f'c{i+1}_ev4 = -1. * s[i]')
            exec(f'c{i+1}_ev5 = 1. * s[i]')
            
        A_11 = np.array([
              [c1_ev1_r,-c1_ev1_c, -1., -2., 3., 4., 2.1, -3.5],
              [c1_ev1_c, c1_ev1_r, 4., 3., 2., 1., -3.1, 2.4],
              [0.   ,0.      , c2_ev4, 2., 4., 5., -1.2, -1.4],
              [0.   ,0.      , 0. , c2_ev2_r,-c2_ev2_c, -1., -2.3, 1.2],
              [0.   ,0.      , 0. , c2_ev2_c, c2_ev2_r, -3., -1.9, 2.1],
              [0.   ,0.      , 0., 0.    ,0.     ,  c1_ev5, -2.4, 1.2], 
              [0.   ,0.      , 0., 0.    ,0.     , 0., c2_ev3_r,-c2_ev3_c],
              [0.   ,0.      , 0., 0.    ,0.     , 0., c2_ev3_c, c2_ev3_r]
              ]); 
        A_22 = np.array([
              [c2_ev1_r,-c2_ev1_c, -1., -2., 3., 4., 2.1, -3.5],
              [c2_ev1_c, c2_ev1_r, 4., 3., 2., 1., -3.1, 2.4],
              [0.   ,0.      , c1_ev4, 2., 4., 5., -1.2, -1.4],
              [0.   ,0.      , 0. , c1_ev2_r,-c1_ev2_c, -1., -2.3, 1.2],
              [0.   ,0.      , 0. , c1_ev2_c, c1_ev2_r, -3., -1.9, 2.1],
              [0.   ,0.      , 0., 0.    ,0.     ,  c2_ev5, -2.4, 1.2], 
              [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_r,-c1_ev3_c],
              [0.   ,0.      , 0., 0.    ,0.     , 0., c1_ev3_c, c1_ev3_r]
              ]);
        A = np.block([
                        [A_11,               np.random.randn(8, 8)],
                        [np.zeros((8, 8)), A_22              ]
                    ])
        """
       
        R, Q = scipy.linalg.schur(A, output='real')
        print('Before sorting:  \n', R)
        T, Z = scipy.linalg.rsf2csf(R, Q)
        ev_orig = np.diag(T)
        print('Eigenvalues before sorting: \n ', ev_orig)
        re = []
        imag = []
        mag = []
        for i in range(len(ev_orig)):
            re.append(ev_orig[i].real)
            imag.append(ev_orig[i].imag)
            mag.append(80.0 * np.sqrt(ev_orig[i].real **2 + ev_orig[i].imag ** 2)) # For size of circle in the plot
          
          
        # Plot the eigenvalues of the state matrix
        """
        fig = plt.figure(1)
        fig.suptitle('Eigenvalues of the state matrix A')

        plt.scatter(re, imag , cmap='jet', s=mag, alpha=0.5)
        
        for i, txt in enumerate(np.arange(16)):
            plt.annotate(txt, (re[i], imag[i]))
        plt.xlabel('Re')
        plt.ylabel('Im')
        plt.colorbar()
        plt.show()
        """
        
        #################### Clustering Algorithm ######################
        data_df = pd.DataFrame({'re':re, 'imag':imag})
        data_df.head()
        spec_cl = SpectralClustering(
                    n_clusters=2,  
                    n_neighbors=5, 
                    affinity='rbf',
                    gamma=1.0)#nearest_neighbors
        cluster = spec_cl.fit_predict(data_df)
        
        print('\n\ncluster: ', cluster)
        cluster = ['k-means_c_' + str(c) for c in cluster]
        """
        fig, ax = plt.subplots()
        sns.scatterplot(x='re', y='imag', data=data_df.assign(cluster = cluster), hue='cluster', ax=ax)
        ax.set(title='Spectral Clustering')
        plt.show()
        """
        eps = np.finfo(R.dtype).eps
        assert np.allclose(np.dot(A, Q), np.dot(Q, R))
        r = np.count_nonzero(np.abs(np.diag(R, -1)) > 100*eps)
        Q, R, block_sizes, n_r, n_c, ap = sort_real_schur(Q, R, spec_cl, 
                           show_plots=show_plots, 
                           b=0, inplace=True)
        #Q, R, ap = sort_real_schur(Q, R, spec_cl, 0, inplace=(m%2==0))
        print('After sorting: \n ', R)

        assert np.allclose(np.dot(A, Q), np.dot(Q, R))  # check that still a decomposition of the original matrix

        # test that Q and R have the correct structure
        assert np.allclose(np.dot(Q, Q.T), np.eye(A.shape[0]))  # Q orthonormal
        assert np.all(np.tril(R, -2) == 0)  # R triangular
        assert r == np.count_nonzero(np.abs(np.diag(R, -1)) > 100 * eps)  # number of blocks in R is preserved

        # check that eigenvalues are sorted
        T, Z = scipy.linalg.rsf2csf(R, Q)
        ev = np.diag(T)
        
        print('Eigenvalues after sorting: \n ', ev)

        # check that eigenvalues were preserved
        for a, b in zip(np.sort(ev), np.sort(ev_orig)):
            print('\na: ', a)
            print('\nb: ', b)
            assert np.allclose(a, b, atol=1e-5, rtol=0) or np.allclose(a, np.conj(b), atol=1e-5, rtol=0)  # TODO: check that both b and conj(b) are present in list
        
        print('OK')



