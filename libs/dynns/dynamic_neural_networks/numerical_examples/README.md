# Numerical examples: Reproducibility information for results in the paper
- This directory contains Jupyter notebooks with the examples in the main paper and appendix: 
    - 1. __diffusion_equation_2d.ipynb__: Ex.3.1: Difusion equation (Unitarily diagonalizable state matrix)
    - 2. __sparsity_cond_tradeoff.ipynb__: Ex 3.2: The reason for horizontal layers
    - 3. __horizontal_layers_all_layers.ipynb__: Ex 3.3: State matrix with different kinds of clusters of eigenvalues
    - 4. __convection_diffusion_equation_2d.ipynb__ Ex SM2.2: Convection-Diffusion equation (example in the supplemental meterial)
    - 5.1. __continuous_time_system_idnetification.mlx__ (MATLAB live script to perform system identification)
    - 5.2. __system_idnetification.ipynb__ (DyNN to simulate the identified LTI system)
    - Example in Section SM2.1.3. The directory __adam_sys_id_comparison__ has all scripts for obtaining state-space matrices using (a) system identification methods and (b) gradient-based iterative optimization with ADAM.
        - __adam_training.ipynb__ :  Ex SM2.1.3: Script to learn state-space matrices from data using the Adam optimizer.
        - __adam_dynn_train.ipynb__: Ex SM2.1.3: Simulate the LTI system obtained with the Adam optimzer using a DyNN on train data. 
        - __adam_dynn_test.ipynb__: Ex SM2.1.3: Simulate the LTI system obtained with the Adam optimzer using a DyNN on test data. 
        - __sys_id.mlx__ : Ex SM2.1.3: Script to identify state-space matrices from data using the continuous-time system identification (CTSI) algorithm.
        - __sys_id_dynn_train.ipynb__: Ex SM2.1.3: Simulate the identified LTI system with DyNN on train data. 
        - __sys_id_dynn_test.ipynb__: Ex SM2.1.3: Simulate the identified LTI system with DyNN on test data. 

- __Run the examples__: 
    - You can run the notebook one cell a time or the whole notebook in a single step by clicking on the menu Cell -> Run All. It will generate the data used in the corresponding example and reproduce the results in the paper. 
